# -*- coding: utf-8 -*-

import ui
import appex

from views import ProgressBarView, Home, uiHelper


class Downloader(ui.View):
    def __init__(self):
        self.percent = 0

        self.main = Home
        self.helper = uiHelper(self.main)
        self.main.flex = 'WH'

        # Setup Progress Bars

        dlp_speed = self.helper.get_item_by_name('pb_dlspeed')
        dlp_num = self.helper.get_item_by_name('pb_dlnum')
        
        self.tabs = self.helper.get_item_by_name('tabs')
        self.tabs.action = self.tab_toggle

        x, y, w, h = dlp_speed.frame

        download_progress = ProgressBarView(w, h)
        download_progress.frame = (0, 0, w, h)
        

        #dlp_speed.present()
        download_number = ProgressBarView(w, h)
        

        dlp_speed.add_subview(download_progress)
        dlp_num.add_subview(download_number)

        download_progress.set_progress(16)

        # TODO: add subviews using uiHelper

        icons = {
            'github': 'iob:social_github_24',
            'video': 'iob:ios7_videocam_24',
            'photo': 'iob:ios7_photos_24',
            'zip': 'iob:bag_24',
            'other': 'iob:link_24',
        }
        
        

    def start(self):
        self.main.present()
        pass
        
    def tab_toggle(self, sender):
        print(sender.selected_index)
        
    


# Download
Downloader().start()

