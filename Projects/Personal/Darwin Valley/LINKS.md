# Helpful Links

### Evernote Client
* https://github.com/mikaelho/noter

### Composite (combine Button and Label)
https://github.com/mikaelho/pythonista-composite/blob/master/README.md

### ui animation franework
https://github.com/mikaelho

### Gestures

https://github.com/mikaelho/pythonista-gestures


### AsyncIO + ui

Class for combining asyncio and aiohttp with Pythonista UI classes

https://github.com/mikaelho/asyncui

### proplistener

Listen to ui components and take action when changed

ie - when textbox changed etc

https://gist.github.com/jsbain/87cf25db0d3f1b16c512


### pinch zoom (zoomabke window)

https://gist.github.com/jsbain/6e4e406b07f52a68d961

### tabbed view?? - meh

https://gist.github.com/jsbain/fcadaffff4be09c4ec78

### touchdispatcher

Intercept touch events before sending to subviews

https://gist.github.com/jsbain/1cf350e92bb5f59706ca

### progressview

https://gist.github.com/blmacbeth/60bacd65c89e5290f452


### Misc

https://github.com/mikaelho/extend/blob/master/README.md

Python EasyCluster

### loads of good atuff

https://github.com/jsbain/uicomponents?files=1
