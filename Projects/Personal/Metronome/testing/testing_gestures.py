#coding: utf-8
from ui import *
from gestures import *

v = View(background_color='white')
v.present('sheet')

b = Label(background_color='grey', text_color='white', alignment= ALIGN_CENTER, center=(v.center[0], v.bounds[3]/2-v.y))
v.add_subview(b)

current_rotation = 0
counter = 0


def rotation_handler(data):
  global current_rotation
  if data.state == Gestures.CHANGED:
    b.transform = Transform.rotation(current_rotation + (data.rotation * 1.5))    
  if data.state == Gestures.ENDED:
    current_rotation += data.rotation
    
def swipe_handler_up(data):
  global counter
  counter += 1
  b.text = str(counter)
  
def swipe_handler_right(data):
  global counter
  counter += 1
  b.text = str(counter)

def tempo_down(data):
  global counter
  counter += 1
  b.text = str(counter)
  
def pan_tempo(data):
  #print(data.velocity)
  #print(dir(data))
  velocity = data.velocity
  scale = data.scale
  state = data.state
  translation = data.translation
  recognizer = data.recognizer
  location = data.location
  
  print('Velocity: {}'.format(velocity))
  print('Scale: {}'.format(scale))
  print('State: {}'.format(state))
  print('Translation: {}'.format(translation))
  #print('Recognized: {}'.format(recognizer))
  print('Location: {}'.format(location))
  print()
  

g = Gestures()
g.add_rotation(v, rotation_handler)

g.add_pan(v, pan_tempo)
#g.add_swipe(v, swipe_handler_up, Gestures.UP)
#g.add_swipe(v, swipe_handler_up, Gestures.RIGHT)
#g.add_swipe(v, tempo_down, Gestures.LEFT)
#g.add_swipe(v, tempo_down, Gestures.DOWN)
# Etc. for down, left
