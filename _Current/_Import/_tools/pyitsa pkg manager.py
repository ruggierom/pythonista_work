# -*- coding: utf-8 -*-
import os
import requests
import ui
import console
import json
import six
from collections import OrderedDict
from pprint import pprint
from configparser import ConfigParser


#installed_pkgs = ''
class PythonistaPackageMover (object):
    
    
    def get_packages(self):
        pip_pkglist = os.path.expanduser('~/Documents/site-packages/.pypi_packages')
        config = ConfigParser()
        config.read(pip_pkglist, encoding='utf-8')
        
     
        packages = []
        for pkg_name in list(config.sections()):
            deps = str(config[pkg_name]['dependency']).split(',')
            #print(dict(config[pkg_name].items()))
            pypi = True if config[pkg_name]['url'] == 'pypi' else False
            p = {
                'name' : pkg_name,
                'version' : config[pkg_name]['version'],
                'dependencies' : deps,
                'pypi' : pypi
            }
            packages.append(p)
            
        return packages
    
    def package_pypi_info(self, package, version):
        
        url = 'http://pypi.python.org/pypi/{package}/{version}/json'.format(package=package,version=version)
        try:
            r = requests.get(url)
            if r.status_code == 200:
                data = r.json()
                cl = data['info']['classifiers']
                cl_dict = {}
                for i in cl:
                    x = i.split(' :: ')
                    if len(x) == 2:
                        k, v = x
                    else:
                        v = list(x).pop()
                        k = ' :: '.join(x)
                    cl_dict.update({ k : v })
            elif r.status_code == 404:
                # Package not found
                return False
            else:
                status_code = r.status_code
                message = r.content
                pass
        except Exception as e:
            six.reraise(BaseException,str(e))


#packages = get_packages()
#package_pypi_info()
#print(packages)
tdata = {'version': '1.2.0b3', 'name': 'SQLAlchemy', 'dependencies': [''], 'pypi': True}
# http://pypi.python.org/pypi/<package_name>/<version>/json
