# -*- coding: utf-8 -*-

import datetime
from peewee import *
import os
import keychain
import bz2
import base64
import textwrap


# Coffee Users

_db_file = 'nutbar.db'
database = SqliteDatabase(_db_file, threadlocals=True)


class RubberWalls (Model):
    
    class Meta:
        database = database

class Meds (RubberWalls):
    id = PrimaryKeyField()
    name = CharField()
    strength = CharField()
    active = BooleanField(default=True)
    

class Nuthouse (object):
    
        def __init__(self):
        super(Coffee, self).__init__()
        if os.path.exists(_db_file):
            try:
                os.unlink(_db_file)
            except Exception as e:
                raise e
        
        self._db_file = _db_file
    
    
    @staticmethod
    def create_tables():
        try:
            database.connect()
            database.create_tables([User, Restaurant, Purchase, Attendance])
        except OperationalError:
            pass
        finally:
            try:
                database.close()
            except:
                pass
        
    def _compress_db(self):
        try:
            with open(self._db_file, 'rb') as dbf:
                db_comp = base64.b64encode(
                    bz2.compress(dbf)
                ).decode('utf-8')
            
            keychain.set_password('nutbar','db', db_comp)
            os.unlink(self._db_file)
            return True
        except Exception as e:
            return False
    
    def _extract_db(self):
        try:
            data = keychain.get_password('nutbar','db')
            if data:
                with open(self._db_file, 'wb') as dbf:
                    dbf.write(
                        bz2.decompress(b64decode(data))
                    )
            else:
                self.create_tables()
                
            return True
        except Exception as e:
            return False
