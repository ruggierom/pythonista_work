# -*- coding: utf-8 -*-

from bottle import Bottle, static_file
from threading import Thread



class WebAPI (object):
  
  app = Bottle()
  
  def __init__(self, **kwargs):
    
    self.web = Thread(target=self.app.run, kwargs=kwargs)
    self.web.daemon = True
  
  @app.get('/static/<filename>')
  def server_static(filename):
    return static_file(
      filename, 
      root='web/static/'
    )
  
  @app.get('/')
  def home(self):
    return static_file('web/index.html')
    
  def start(self):
    self.web.start()
    pass

w = WebAPI(host='localhost', port=8000, debug=True)
#w.start()
