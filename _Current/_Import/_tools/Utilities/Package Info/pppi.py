#
import requests
import pureyaml
import json

def pypi_package_info(pkg):
    url = 'http://pypi.python.org/pypi/{p}/json'.format(p=pkg)
    r = requests.get(url)
    if r.status_code == 200:
        #print(r.json())
        with open('pkk.txt','w') as f:
            #t = type(r.json())
            t = pureyaml.dumps(r.json())
            #print(t)
            #f.write(t)
            #t = json.dumps(r.json(),indent=2)
            f.write(t)
        pass
    else:
        print(r.status_code)

pypi_package_info('paramiko')
"""
def rinfo(pkg):
...     url = 'http://pypi.python.org/pypi/{p}/json'.format(p=pkg)
...     r = requests.get(url)
...     if r.status_code == 200:
...         return r.json()
... 
"""
