# -*- coding: utf-8 -*-
"""
Installer Utility for Pythonista Apps/Modules

Workflow:
    
"""

import ui
import json
import console

class InstallerConfig (object):
    """
    InstallerConfig Object
    
    * Reads and Writes JSON config files
    * Allows developer to customize installer
    """
    _install = None
    
    
    def __init__(self):
        
        self.install=dict(
            target='.',
            other_option=1,
        )
        
        self.options=dict(
            background_color='#666666',
            titlebar=dict(
                hide_titlebar=False,
                title_bar_color='#fff',
                title_color='#333',
                hide_close_button=False,
                orientations=None,
                
            ),
            installer_view=dict(
                background_color='#ffffff'
            ),
            heading_font=('<System>', 36),
            font=('<System>', 16),
        )
        
        
        self.requirements=dict(
            python=[2.7, 3.4, 3.6],
            device=['iPhone'], # iPhone, iPad, iWatch
            python_modules=[
                ('requests','~', '2.9.1'),
                ('package', 'vers_mod', 'vers'),
            ], # Python Modules
        )
        
        
        self.pages=[
            dict(
                name='Welcome',
                view=ui.TextView,
                init_params=dict(
                    editable=False,
                    text="Sample Text",
                )
            ),
            dict(
                name='license',
                view='LicenseView',
                init_params=['self.MITLicense'], # Copy
            ),
            dict(
                name='Install',
                view=self.install,
            )
        ]

class WelcomeView (ui.View):
    """
    Standard View for Installer - Welcome View
    
    Shows the name of the App being installed and allows user
    to proceed or exit.
    
    Also allows the optional display of requirements and installed status.
    """
    def __init__(self):
        self.flex = 'WH'
        
        welcome = ''
    

class Installer (ui.View):
    """
    Main Installer View
    
    This generates the GUI, and performs the required installation steps.
    """
    
    def __init__(self, config=None, **kwargs):
        """
        Class Initializer
        """
        # Setup self
        
        # installer subview default options
        installer_view = dict(
            background_color = '#ffffff',
            corner_radius=8,
            frame=(5, 30, 310, 445),
            flex='WH',
            navigation_bar_hidden=True,
        )
        
        # main view options
        
        self.frame = (0, 25, 320, 480)
        self.flex = 'WH'
        self.background_color = '#666666'
        
        
        # Options
        self.titlebar_options = None
        for key, value in config.options.items():
            if key == 'titlebar':
                self.titlebar_options = value
            elif key == 'installer_view':
                for k, v in value.items():
                    installer_view[k] = v
            else:
                setattr(self, key, value)
        
        # Pages
        self.pages = config.pages
        self.page_views = []
        self.page_views.append(
            ui.View()
        )
        self.process_pages()
        
        # Setup the installer view
        
        self.content_view = ui.NavigationView(
            self.page_views[0],
        )
        
        for key, value in installer_view.items():
            setattr(self.content_view, key, value)
        
        self.add_subview(self.content_view)
    
    def draw(self):
        pass
        
    def process_pages(self):
        if not isinstance(self.pages, list):
            raise AttributeError('pages must be list')
        
        for page in self.pages:
            if not isinstance(page, dict):
                raise AttributeError('Invalid Page')
    
    
            
         
main = Installer(InstallerConfig())
main.present('sheet', hide_title_bar=True, title_bar_color='#333')
