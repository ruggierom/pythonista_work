# -*- coding: utf-8 -*-
"""

"""

import os
import sys
import errno

class Warn (object):
    def __init__(self, message):
        sys.stderr.write('{}\n'.format(message))
        

class Services (object):
    
    services = None
    
    def __init__(self):
        """
        Class Initializer -
        
        This will search python files in this folder, except
        the services listed in the disabled.txt file.
        """
        self._services = None
        self._start_path = os.getcwd()
        self._service_path = os.path.dirname(__file__)
        # Set Defaults
        self.services = []
        

    @property
    def services(self):
        return self._services
    
    @services.setter
    def services(self, value):
        self._services = value
        return self

    
    

s = Services()
print(s.services)
s.services = [1,2,3]
#print(s._find_services())
