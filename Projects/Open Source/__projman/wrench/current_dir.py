# -*- coding: utf-8 -*-
"""
Small Utility to allow you to find or set your python working path to the folder of the currently edited file.

I created this to help me quickly chdir into folders which are connected to Working Copy or Clone.
"""

import os.path
import console
import editor
import clipboard

def findpath():
    try:
        path = os.path.dirname(
            editor.get_path()
        )
        basepath = os.path.expanduser('~')
        os.chdir(basepath)
        relpath = '~/{}'.format(os.path.relpath(path))
        os.chdir(path)
        title='Copy Path to Clipboard?'
        message='Discovered:\n' \
                '{}\n' \
                'Select Action'.format(relpath)
        buttons = [
            'Copy path to clipboard',
            'Change to directory',
            'None'
        ]
        choice = console.alert(
            title,
            message,
            *buttons,
            hide_cancel_button=True
        )
        if choice == 1:
            clipboard.set(relpath)
            console.hud_alert(
                'Path copied to clipboard'
            )
        elif choice == 2:
            console.hud_alert(
                'Switched to {}'.format(relpath)
            )
            os.chdir(path)
        else:
            """
            console.hud_alert(
                "Clipboard not modified",
            )
            """
            return 
        
        
    except Exception as e:
        console.hud_alert('Unable to find path from current file.',icon='error')

findpath()
