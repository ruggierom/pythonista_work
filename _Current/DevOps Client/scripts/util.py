# -*- coding: utf-8 -*-
"""
Utility Functions to be used with ui
"""
from ui import *
from objc_util import *

UILayoutContainerView = ObjCClass('UILayoutContainerView')
UISwipeGestureRecognizer = ObjCClass('UISwipeGestureRecognizer')

def disable_swipe_to_close(view):
    """
    Disable the two-finger swipe gesture for Pythonista.
    See: https://github.com/mikaelho/pythonista-gestures/issues/4    
    """
    v = view.objc_instance
    while not v.isKindOfClass_(UILayoutContainerView.ptr):
        v = v.superview()
    for gr in v.gestureRecognizers():
        if gr.isKindOfClass_(UISwipeGestureRecognizer.ptr):
            gr.setEnabled(False)

__all__ = ['disable_swipe_to_close']

