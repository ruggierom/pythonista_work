# -*- coding: utf-8 -*-

from db import CoffeeTrip

def test_dbcreate():
    import os
    CoffeeTrip().create_tables()
    assert os.path.exists('coffee.db')


def test_user_create():
    import random
    import string

    username = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(10)])
    c = CoffeeTrip.new_user(username)
    assert c == 1
    for u in range(0,35):
        username = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(10)])
        CoffeeTrip.new_user(username)


def test_user_deactivate():
    assert CoffeeTrip.deactivate_user(1)


def test_user_activate():
    assert CoffeeTrip.activate_user(1)


def test_user_delete():
    assert CoffeeTrip.delete_user(1)


def test_new_restaurant():
    assert CoffeeTrip.new_restaurant('Starbuck Hortons') == 1


def test_restaurant_deactivate():
    assert CoffeeTrip.deactivate_restaurant(1)


def test_restaurant_activate():
    assert CoffeeTrip.activate_restaurant(1)


def test_purchase():
    import random


    coffee_goers = [4]
    for num_peeps in range(1,random.randint(2,15)):
        coffee_goers.append(random.randint(2,35))   # User #1 was deleted, remember?

    # Remove dupes
    coffee_goers = list(set(coffee_goers))

    assert CoffeeTrip.new_purchase(buyer_id=2, restaurant_id=1, attendees=coffee_goers) == 1


def test_delete_attendee():
    assert CoffeeTrip.delete_attendee(1,4)


def test_delete_purchase():
    assert CoffeeTrip.delete_purchase(1)


def test_all_users():
    users = CoffeeTrip.all_users()
    assert isinstance(users, list)
    assert isinstance(users[0],dict)


# Purge the old DB

def test_purge_test_db():
    import os
    os.unlink('coffee.db')
    assert not os.path.exists('coffee.db')