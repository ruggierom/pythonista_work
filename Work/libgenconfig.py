# -*- coding: utf-8 -*-
import pyaes
from six import text_type
import errno

class GCrypto (object):
  
  def __init__(self, key):
    assert isinstance(key, text_type)
    assert len(key) == 32, 'Key must be 32 byte length'
    self.key = str(key).encode('utf8')
    self.aes = pyaes.AESModeOfOperationCTR(key)
  
  def encrypt(self, string):
    """
    Encrypt the contents of a string with AES128 Counter Mode of Operation
    
    :param string: str: String to encrypt
    :return bytes: Encrypted Bytes representation of string
    """
    string = str(string).encode('utf8')
    return self.aes.encrypt(string)
  
  def decrypt(self, crypt):
    """
    Decode the contents of a string which was encrypted by the GCrypto().encrypt() function
    
    :param bytes: Encrypted Bytes representation of string
    :return str: The Decrypted string 
    """
    return self.aes.decrypt(crypt)



class FIMReport (object):
  """
  Read and Write FIM Reports 
  """
  
  def __init__(self, report_filepath):
    self.current = None
    self.report_filepath = report_filepath
    try:
      with open(report_filepath, 'r') as report:
        self.current = report.read()
    except IOError as e:
      if e.errno != errno.ENOENT:
        # If error is any other than does not exist
        raise e
    
  def compare(self, new_report):
    """
    Compare a current scan with a previously saved scan
    """
    return 
  
  def save(self, report_string):
    with open(self.report_filepath, 'w') as report:
      report.write(report_string)
        report.write(report_string)
  
  
import console
console.clear()
FIMReport('derf.txt')
