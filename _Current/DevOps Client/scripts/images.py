# -*- coding: utf-8 -*-
"""
images - A Pythonista module of custom images created from the ui.ImageContext module.

Current Images:
    * Checkbox
        * Unchecked
        * Checkmark
        * Plus
        * Minus
        * X
        
    
        
"""

import ui
import console

console.clear()


class CheckboxImage (object):
    
    
    def __init__(self, image, width=25, height=25, corner_radius=4, line_width=2.5, padding=1, **kwargs):
        
        CHECKMARK_UNCHECKED=self.add_checkbox_border,
        CHECKMARK_CHECK=self.add_checkmark_check,
        CHECKMARK_PLUS=self.add_checkmark_plus,
        CHECKMARK_DASH=self.add_checkmark_dash,
        CHECKMARK_CROSS=self.add_checkmark_cross,      
        
        # Setup Box width to be a little less than the image size        
        box_width = (width - (line_width / 2) - padding)
        box_height = (height - (line_width / 2) - padding)
        
        
        # Setup class attributes
        
        self.width = width
        self.height = height
        self.corner_radius = corner_radius
        self.box_width = box_width
        self.box_height = box_height
        self.padding = padding
        self.line_width = line_width
        
        # Customization / Keyword Arguments
        self.stroke_color= kwargs.pop('stroke_color','black')
        
        
        # Figure out how to make this work?
        
    @property
    def image_list(self):
        return dict(
            CHECKMARK_UNCHECKED='',
            CHECKMARK_CHECK='',
            CHECKMARK_PLUS='',
            CHECKMARK_DASH='',
            CHECKMARK_CROSS='',
        )            
    
    @property
    def image_reference(self):
        return dict(
            CHECKMARK_UNCHECKED=self.add_checkbox_border,
            CHECKMARK_CHECK=self.add_checkmark_check,
            CHECKMARK_PLUS=self.add_checkmark_plus,
            CHECKMARK_DASH=self.add_checkmark_dash,
            CHECKMARK_CROSS=self.add_checkmark_cross,              
        )
        
            
    def add_checkmark_check(self):
        with ui.GState():
            check = ui.Path(
                self.padding, 
                self.padding, 
                self.box_width, 
                self.box_height
            )
            
            start_x =  (self.box_width / 3)
            start_y = self.box_height / 2 + (self.box_height / 8)
            
            check.line_width = (self.line_width * 1.5)
            check.line_cap_style = ui.LINE_CAP_ROUND
            check.line_join_style = ui.LINE_JOIN_ROUND
            ui.set_color(self.stroke_color)
            
            check.move_to(start_x, start_y)
            check.line_to(
                (self.box_width / 2) + self.padding,
                self.box_height - (self.box_height / 6),
            )
            
            check.line_to(
                self.box_width - (self.box_width / 4),
                self.box_height / 4
            )
            
            check.stroke()
    
    def add_checkmark_plus(self):
        
        with ui.GState():
            dash = ui.Path(
                self.padding, 
                self.padding, 
                self.box_width, 
                self.box_height
            )
            
            start_point = ((self.padding * 3) + self.line_width)
            
            dash_height = ((self.box_height / 2) + self.padding)
            
            dash.line_width = (self.line_width * 1.5)
            dash.line_cap_style = ui.LINE_CAP_ROUND
            ui.set_color(self.stroke_color)
            
            dash.move_to(
                (start_point + self.padding),
                dash_height,
            )
            
            dash.line_to(
                (self.box_width - start_point + self.padding),
                dash_height
            )
            
            dash.move_to(
                dash_height,
                (start_point + self.padding),
            )
            
            dash.line_to(
                dash_height,
                (self.box_height - start_point + self.padding)
            )
            
            dash.stroke()
    
    def add_checkmark_dash(self):
        with ui.GState():
            dash = ui.Path(
                self.padding, 
                self.padding, 
                self.box_width, 
                self.box_height
            )
            
            start_point = ((self.padding * 3) + self.line_width)
            
            dash_height = ((self.box_height / 2) + self.padding)
            dash.move_to(
                (start_point + self.padding),
                dash_height,
            )
            dash.line_width = (self.line_width * 1.5)
            dash.line_cap_style = ui.LINE_CAP_ROUND
            ui.set_color(self.stroke_color)
            
            dash.line_to(
                (self.box_width - start_point + self.padding),
                dash_height
            )
            
            dash.stroke()
            
            return dash
            
        
    
    def add_checkmark_cross(self):
        # Draw the X
        
        start_point = ((self.padding * 3) + self.line_width)
        
        
        with ui.GState():
            cross = ui.Path(
                self.padding, 
                self.padding, 
                self.box_width, 
                self.box_height
            )
                    
            cross.line_width = (self.line_width * 1.5)
            cross.line_cap_style = ui.LINE_CAP_ROUND
            ui.set_color(self.stroke_color)
            
            
            cross.move_to(
                start_point, 
                start_point 
            )
            
            cross.line_to(
                self.box_width - (start_point /2) - self.padding, 
                self.box_height - (start_point /2) - self.padding,
            )
        
            cross.move_to(
                (start_point), 
                self.box_height - (start_point / 2) - self.padding
            )
            
            cross.line_to(
                self.box_width-(start_point / 2) - self.padding, 
                (start_point)
            )
            
            
            cross.stroke()
        
        return cross
    
    def add_checkbox_border(self):
        
        box_method = ui.Path.rect
        checkbox_args = [
            self.padding,
            self.padding,
            self.box_width,
            self.box_height,
        ]
        
        if self.corner_radius > 0:
            box_method = ui.Path.rounded_rect
            checkbox_args.append(self.corner_radius)
            
        checkbox_box = box_method(
            *checkbox_args,
        )
        
        checkbox_box.line_join_style=ui.LINE_JOIN_BEVEL
        checkbox_box.line_width=self.line_width
        ui.set_color('black')
        checkbox_box.stroke()
        
        return checkbox_box
    
    @property
    def image(self):
        with ui.ImageContext(self.width, self.height) as ctx:            
            checkbox_box = self.add_checkbox_border()
            #checkmark_cross = self.add_checkmark_check()            
            return ctx.get_image()
            
    

# Radio Buttons
class RadioButtonImage (object):
    
    def __init__(self, type="standard", checked=False, width=24, height=24, line_width=2.5, border_color='black', fill_color='#444444'):
        """
        Radio Button Class Initializer
            Arguments:
                :param str: type: Type of Button, "standard" or "heavy".
                :param bool: checked: If the button state is checked, or unchecked.
                :param (int|float): width: Width of the button
                :param (int|float): height: Height of the button
                :param (int|float): line_width: Width the drawn lines (border)
                :param (str|tuple): border_color: Color of the line (border)
                :param (str|tuple): fill_color: Fill Color (checked)
        
        Example:
            unchecked_image = RadioButtonImage().image() # Unchecked image
            checked_image = RadioButtonImage('standard', checked=True).image() # Checked Image
            checked_image = RadioButtonImage('heavy', checked=True).image() # Checked Image
        """
        self.width = width
        self.height = height
        self.line_width = line_width
        self.padding = self.line_width * 1.75
        self.border_color = border_color
        self.fill_color = fill_color        
        self.checked = checked
        try:
            #self.check_type = type
            assert type in ('standard','heavy',), 'type value may be "standard" or "heavy" only.'
            self._check = getattr(self, type)            
        except AttributeError as e:
            print(e)
            raise AttributeError('Error: type may be string value of "standard" or "heavy" only.')        
    
    def unchecked(self):
        ui.set_color(self.border_color)
        border = ui.Path.rounded_rect(self.padding / 2, self.padding / 2, (self.width - self.padding), (self.height - self.padding), self.width / 2)
        border.line_width = self.line_width
        border.stroke()
        return self
        
    def heavy(self):
        self.unchecked()
        ui.set_color(self.fill_color)
        border = ui.Path.rounded_rect(self.padding, self.padding, (self.width - (self.padding * 2)), (self.height - (self.padding * 2)), self.width / 2)
        border.line_width = self.line_width
        border.fill()              
        return self
    
    def standard(self):
        self.unchecked()
        ui.set_color(self.fill_color)        
        padding_mult = (self.width / 16)
        length_mult = (padding_mult * 2)
        border = ui.Path.rounded_rect( (self.padding * padding_mult), (self.padding * padding_mult), (self.width - (self.padding * length_mult)), (self.height - (self.padding * length_mult)), self.width)
        border.line_width = self.line_width
        border.fill()              
        return self
    
    
    def image(self):
        with ui.ImageContext(self.width, self.height) as ctx:
            self._check()            
            return ctx.get_image()
                                    
if __name__ == '__main__':
    cbi = CheckboxImage('')
    cbi.add_checkmark_cross()
    cbi.image.show()
    #cb_image = cbi.image
    #cb_image.show()
