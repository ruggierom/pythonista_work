import ui

v = ui.load_view()
login = ui.load_view('login')
login.background_color = '#eee'
login['image_logo'].image = ui.Image('../assets/logo.png')
login['image_logo'].content_mode = ui.CONTENT_SCALE_ASPECT_FIT
login['ssl_image'].image = ui.Image('../assets/shield.jpg')
login['ssl_image'].content_mode = ui.CONTENT_SCALE_ASPECT_FIT

# ui.CONTENT_SCALE_ASPECT_FIT

v = login
v.present('sheet', hide_title_bar=True)
