# -*- coding: utf-8 -*-

import ui

try:
    from .tabview import TabView
except ImportError:    
    from tabview import TabView
        
    

class Route53 (ui.View):
    
    def __init__(self):
        
        hz_image = ''
        
        self.frame = (0, 0, 1024, 768)
        # Load Views
        try:
            self.hosted_zone_view = ui.load_view('views/hosted_zones')
        except Exception as e:
            self.hosted_zone_view = ui.load_view('hosted_zones')
        self.hosted_zone_view.flex = 'WH'
        
        self.placeholder_view = ui.View()
        
        tabs = [
            dict(
                name='hosted_zones',
                title='Hosted Zones',
                image=ui.Image.named('typb:List'),
                view=self.hosted_zone_view
            ),
            dict(
                name='health_checks',
                title='Health Checks',
                image=ui.Image.named('typb:Line_Chart'),
                view=self.placeholder_view
            ),
            dict(
                name='traffic_policies',
                title='Traffic Policies',
                image=ui.Image.named('typb:Relocate'),
                view=self.placeholder_view,
            )
        ]
        #super(Route53, self).__init__(tabs=tabs, height=70)
        #self.flex = 'WH'
        #self.frame = (0, 0, 1024, 768)
        #self.tab_view.flex = 'WH'
        self.content_view = TabView(tabs=tabs, height=70)
        self.content_view.frame = (0, 0, 1024, 768)
        self.add_subview(self.content_view)
        

__all__ = ['Route53']


if __name__ == '__main__':
    r = Route53()
    x = dir(r.subviews[0].content_view)
    print(x)
    r.present('sheet')
