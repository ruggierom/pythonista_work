# -*- coding: utf-8 -*-

class MethodOverrideForbiddenError(RuntimeError):
    pass

class NoMainMethodError(NotImplementedError):
    pass
