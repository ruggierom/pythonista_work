# -*- coding: utf-8 -*-

class DuplicateKeyError (AttributeError):
    
    def __init__(self, *args, **kwargs):
        super(DuplicateKeyError, self).__init__(*args, **kwargs)
