# -*- coding: utf-8 -*-
"""
forms - Custom Form Elements for Pythonista ui.

Included in this package are some of the ui elements that you might typically see in Desktop or Web based applications.

Included:
    * Drop Down
    
Planned:
    * Combo Box (Drop Down + Free Form)
    * Auto Suggest (drop down)
    * Auto Suggest (over keyboard)
    * Radio Buttons
    * Form Group Box (bordered ui.View with title)
"""

import ui



class TextFieldAutoSuggest (ui.View):
    """
    Associate Auto Suggest with a TextField.
    
    """
    pass

class GroupView (ui.View):
    
    content_view = ui.View()
    
    def __init__(self,frame=(0, 0, 100, 100), flex='', background_color=None, name=None, **kwargs):
        self.frame = frame
        self.flex = flex
        self.background_color = background_color
        self.name = name
        pass
    
    # def __setattr__(self, var, val):
    #     super(GroupView, self).__setattr__(var, val)
    # 
    #     if var !='content_view':
    #         setattr(self.content_view, var, val)
    # 
        


            

if __name__ == '__main__':
    gv = GroupView()
    print(gv.flex)
    print('---')
    gv.flex = 'WH'
    print(gv.flex)
