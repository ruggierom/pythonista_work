# -*- coding: utf-8 -*-
"""
Python Manager - inspired by PyEnv for posix, and py launcher

Uses:
    sh - execute applications, shell arg eacape, etc
"""

import os
import sh
from click import get_app_dir
import configparser

class Config (object):
    """
    ini style config manager
    """
    def __init__(self):
        self.config_file = os.path.join(
            get_app_dir('winpyenv', roaming=True, force_posix=False),
            'winpyenv_config.ini',
        )
        self.config = configparser.ConfigParser()
        
    

class PythonManager (object):
    
    def __init__(self):
        # 
        pass


if __name__ == '__main__':
    # main_with_ini.py
    """
    import configparser
    
    config = configparser.ConfigParser()
    config.read('config.ini')
    
    secret_key = config['DEFAULT']['SECRET_KEY'] # 'secret-key-of-myapp'
    ci_hook_url = config['CI']['HOOK_URL'] # 'web-hooking-url-from-ci-service'
    
    # main_with_json.py
    import json
    
    with open('config.json', 'r') as f:
        config = json.load(f)
    
    secret_key = config['DEFAULT']['SECRET_KEY'] # 'secret-key-of-myapp'
    ci_hook_url = config['CI']['HOOK_URL'] # 'web-hooking-url-from-ci-service'
    view raw
    """
