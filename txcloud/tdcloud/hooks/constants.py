# -*- coding: utf-8 -*-

from enum import Enum


class HookType(Enum):
    RUN_BEFORE_EXECUTE=0
    RUN_AFTER_EXECUTE=1
    RUN_BEFORE_TEST=2
    RUN_AFTER_TEST=3
    

__all__ = ['HookType']

## testing

# Data Types

from typing import Union, AnyStr, NewType

class Hook(object):
    pass

CLIHook = NewType('CLIHook', Hook)

z = CLIHook(Hook())
print(type(z))
print(z)
print(isinstance(z, Hook))
