#!python2
# -*- coding: utf-8 -*-

from diffdict import Gendiff, GendiffReport
import click


@click.group()
def cli():
  pass

@cli.command(help='Checkfor differences')
def diff():
  pass

@cli.command(help='Merge')
def merge():
  pass

if __name__ == '__main__':
  import console
  console.clear()
  cli()
