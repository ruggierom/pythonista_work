# -*- coding: utf-8 -*-

import ui
import os.path
import appex
from tabview import TabView
from picker import AssetCollectionView


class PhotoView (ui.View):
    
    def __init__(self):
        
        self.frame = (0, 0, 320, 480)
        self.flex = 'WH'
        
        self.av = AssetCollectionView(
            frame=(0, 35, 320, 480), 
            flex='WH'
        )
        
        self.check_button = ui.Button(
            title='Check',
            action=self.seldo,
            frame=(10, 0, 75, 32)
        )
        self.av.name = 'All Photos'
        self.add_subview(self.check_button)
        self.add_subview(self.av)
    
    def selected(self):
        if hasattr(self.av.data_source, 'selected_asset_index'):
            asset = self.av.data_source.assets[self.av.data_source.selected_asset_index]
            return asset
        else:
            return None
    
    def seldo(self, sender=None):
        print(self.selected())

class ZipTool (ui.View):
    def __init__(self):
        self.docs = os.path.join(
            os.path.dirname(__file__),
            'Documents'
        )
        
        # Setup Self
        self.frame = (0, 0, 320, 480)
        self.flex = 'WH'
        self.background_color = '#666'
        
        # Setup Tabs
        self.pv = PhotoView()
        
        temp_view = ui.View()
        tabs = [
            dict(
                name='files',
                title='Files',
                image=ui.Image.named('typb:List'),
                view=temp_view,
            ),
            dict(
                name='photos',
                title='Photos',
                image=ui.Image.named('typb:Camera'),
                view=self.pv,
            ),
            dict(
                name='audio',
                title='Audio',
                image=ui.Image.named('typb:Music'),
                view=temp_view,
            ),
            dict(
                name='import',
                title='Import',
                image=ui.Image.named('typb:Archive'),
                view=temp_view
            )
        ]
        
        self.tabview = TabView(
            tabs, 
            height=65,
        )
    
        
        rounded_top = ui.View(
            frame=(6, 25, 308, 420),
            flex='WH',
            corner_radius=12,
            background_color='white',
        )
        
        
        self.add_subview(rounded_top)
        # Opt 1 
        self.tabview.frame = (6, 35, 308, 440)
        self.add_subview(self.tabview)
        
        
    def photo_assets(self, sender=None):
        x = self.pv.selected()
        print(x)



if __name__ == '__main__':
    present_options = dict(
        style='fullscreen', 
        title_bar_color='black', 
        hide_title_bar=True
    )
    ZipTool().present(**present_options)
    
    """
'bs_indexPathByAddingPrefix_', 'bs_indexPathByRemovingFirstIndex', 'bs_nearestCommonAncestorWithIndexPath_', 'bs_subpathFromPosition_', 'bs_subpathWithRange_', 'compare_', 'copy', 'copyWithZone_', 'dd_stringValue', 'dealloc', 'description', 'encodeWithCoder_', 'getIndexes_', 'getIndexes_range_', 'hash', 'indexAtPosition_', 'indexPathByAddingIndex_', 'indexPathByRemovingLastIndex', 'init', 'initWithCoder_', 'initWithIndex_', 'initWithIndexes_length_', 'isEqual_', 'item', 'kind', 'length', 'mutableCopy', 'performSelectorInBackground_withObject_', 'performSelectorOnMainThread_withObject_waitUntilDone_', 'performSelector_withObject_afterDelay_', 'pu_alteredIndexPathAfterDeletingItemAtIndexPath_', 'pu_alteredIndexPathAfterDeletingItemsAtIndexPaths_', 'pu_alteredIndexPathAfterInsertingItemAtIndexPath_', 'pu_alteredIndexPathAfterInsertingItemsAtIndexPaths_', 'pu_alteredIndexPathAfterMovingItemAtIndexPath_toIndexPath_', 'pu_indexPathAfterDeletingItemAtIndexPath_', 'pu_indexPathAfterInsertingItemAtIndexPath_', 'pu_indexPathAfterMovingItemFromIndexPath_toIndexPath_', 'pu_indexPathAfterReloadingItemAtIndexPath_', 'pu_indexPathByAppendingIndexPath_', 'pu_indexPathByChangingIndexAtPosition_toIndex_', 'pu_isParentOfIndexPath_', 'pu_isValid', 'pu_shortDescription', 'px_alteredIndexPathAfterDeletingItemAtIndexPath_', 'px_alteredIndexPathAfterDeletingItemsAtIndexPaths_', 'px_alteredIndexPathAfterInsertingItemAtIndexPath_', 'px_alteredIndexPathAfterInsertingItemsAtIndexPaths_', 'px_alteredIndexPathAfterMovingItemAtIndexPath_toIndexPath_', 'px_indexPathAfterDeletingItemAtIndexPath_', 'px_indexPathAfterInsertingItemAtIndexPath_', 'px_indexPathByChangingIndexAtPosition_toIndex_', 'row', 'section']    
    
    """
