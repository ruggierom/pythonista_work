# -*- coding: utf-8 -*-
import re
from six import text_type
from collections import OrderedDict, deque, namedtuple


try:
    from typing import Union, List, Dict, Tuple, NamedTuple
    
    dict_type = Union[Dict, OrderedDict]
    list_type = Union[List, Tuple, deque]
    
except ImportError:
    dict_type = (dict, OrderedDict)
    list_type = (list, tuple, deque)


def set_if_in(obj, attr, value = None):

    if isinstance(attr, text_type):
        if hasattr(obj,attr):
            setattr(obj, attr, value)
            
def namedtuple_with_defaults(typename, field_names, default_values=(), verbose=False, rename=False, module=None):
    """
    Create a namedtuple object with customizable defaults.
    Returns a new tuple subclass named typename

    :param typename: Name of the tuple subclass created
    :param field_names: (str): Space separated list of class attributes
    :param default_values: (dict,list,tuple):  Setup default values
    :param verbose: (bool): Print class definition after build. Outdated, use _source instead.
    :param rename: Automatically rename invalid fieldnames (i.e. 'def') to positional arguments rather than fail.
    :param module: If module is defined, the __module__ attribute of the named tuple is set to that value.
    :return: typename(tuple)

    Note: If you wish to see the source, call print(Friend._source) after examples below from python interpreter.
    """
    opts = {'verbose': verbose, 'rename': rename, 'module': module}
    # Fix for Python < 3.6, < 3.1
    py_version = int(''.join(map(str, sys.version_info[:2])))  # Python 2.7 becomes 27, 3.5 = 35, etc.
    if py_version < 31:
        # Verbose option was added in 3.1
        opts = {'verbose': verbose, 'rename': rename}

    elif py_version < 36:
        # Module was added in 3.6
        del opts['module']

    T = collections.namedtuple(typename, field_names, **opts)
    T.__new__.__defaults__ = (None,) * len(T._fields)
    if isinstance(default_values, collections.Mapping):
        prototype = T(**default_values)
    else:
        prototype = T(*default_values)
    T.__new__.__defaults__ = tuple(prototype)
    return T

first_cap_re = re.compile('(.)([A-Z][a-z]+)')
all_cap_re = re.compile('([a-z0-9])([A-Z])')

def snake_case(name):
    # https://stackoverflow.com/questions/1175208/elegant-python-function-to-convert-camelcase-to-snake-case
    
    s1 = first_cap_re.sub(r'\1_\2', name)
    return all_cap_re.sub(r'\1_\2', s1).lower()


__all__ = ['dict_type', 'list_type', 'set_if_in',
           'snake_case']
