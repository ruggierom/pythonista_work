import ui

v = ui.load_view()
meds = ui.load_view('meds')
meds.background_color = '#ccc'
medlist = meds['medications']
medlist.background_color='#fff'
meds_list = [
    dict(
        image=ui.Image.named('iob:medkit_24'),
        title='Zoloft 50mg\n8am',
        accessory_type='checkmark',
    ),
    dict(
        image=ui.Image.named('iob:medkit_24'),
        title='Vyvanse 50mg\n8am',
        accessory_type='checkmark',
    ),
    dict(
        image=ui.Image.named('iob:medkit_24'),
        title='Vyvanse 20mg\n11am',
        accessory_type='None',
    ),
]

medications = ui.ListDataSource(meds_list)
medications.number_of_lines = 3
medications.highlight_color = None
medlist.data_source = medications

nav = ui.NavigationView(meds)
nav.frame = (0,0, v.width, v['tabs'].y )
nav.flex = 'WH'

v.add_subview(nav)

v.present('sheet')
