import ui
import json

nitem = [{
  'type' : 'text',
  'key' : 'Courier',
  'value' : '',
  },{
  'type' : 'text',
  'key' : 'API URL',
  'value' : '',
  }, {
  'type' : 'switch',
  'key' : 'Done',
}]




class APIStatus (object):
  
  def __init__(self):
    with open('api_status.json') as f:
      status = json.load(f)

    self.add_fields = [
      {'type' : 'switch',
        'key' : 'Needs Reg',
      },{
      'type' : 'switch',
        'key' : 'Reg Done',
      },
    ]
    

  
  def save(self):
    with open('apistatus.json', 'w') as f:
      self.status = json.dump(self.status, f, indent=2)
  

APIStatus()
