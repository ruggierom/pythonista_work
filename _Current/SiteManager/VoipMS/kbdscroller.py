#!python2
import ui

#Get current screen size, taking orientation into account.
def screen_size():
    size = ui.get_screen_size()
    if int(ui.WebView().eval_js('window.orientation')) % 180 != 0:
        return (size[1], size[0])
    return size

class DynamicScrollViewTest (ui.View):
    def __init__(self):
        self.frame = (0, 0, 320, 504)
        sv = self.scroll_view = ui.ScrollView()
        self.add_subview(sv)
        sv.frame = self.bounds
        print "self.frame: " + str(self.frame)
        print "scroll_view.frame: " + str(sv.frame)
        sv.content_size = (320, 504)
        sv.flex = "wh"
        sv.background_color = "silver"
        self.fields = []
        for i in range(13):
            field = ui.TextField(frame=(6, 10 + 42 * i, 308, 32))
            field.placeholder = "Field " + str(i + 1)
            field.flex = "w"
            sv.add_subview(field)
            self.fields.append(field)
        
    def keyboard_frame_will_change(self, frame):
        print("Keyboard frame: " + str(frame))
        print("Window frame: " + str(main_form.frame))
        screen = screen_size()
        print("Screen size: {} x {}".format(screen[0], screen[1]))
        y_overlap = max([main_form.frame[1] + main_form.frame[3] + frame[1] - screen[1], 0])
        print("Y Overlap: " + str(y_overlap))
        self.scroll_view.content_inset = (0, 0, y_overlap, 0)

main_form = DynamicScrollViewTest()
main_form.present("sheet")
