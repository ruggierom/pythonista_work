#!python2
# -*- coding: utf-8 -*-

from colors import Colors
from collections import OrderedDict

terpy = {}

for name, rgb in Colors:
    r, g, b = rgb
    total = (r+b+g)
    terpy.update({name: total})


color_dict = OrderedDict(
    sorted(
        terpy.items(), 
        key=lambda t: t[1]
    )
)

for k, v in color_dict.items():
    print('{}: {}'.format(k, v))
