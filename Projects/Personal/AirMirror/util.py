# -*- coding: utf-8 -*-
import sys

# TODO: Finish this!

disable_override_warning = False


# Taken from: https://stackoverflow.com/questions/3948873/prevent-function-overriding-in-python

def protect(*protected):
    """Returns a metaclass that protects all attributes given as strings"""
    class Protect(type):
        has_base = False
        def __new__(meta, name, bases, attrs):
            if meta.has_base:
                for attribute in attrs:
                    if attribute in protected:
                        raise AttributeError('Overriding of attribute "%s" not allowed.'%attribute)
            meta.has_base = True
            klass = super().__new__(meta, name, bases, attrs)
            return klass
    return Protect

def override_warning(*protected):
    """Returns a metaclass that warns on overriding all attributes given as strings"""
    class Protect(type):
        has_base = False
        def __new__(meta, name, bases, attrs):
            if meta.has_base:
                for attribute in attrs:
                    if attribute in protected:
                        if not disable_override_warning:
                          sys.stderr.write('WARNING: Overriding this method is not recommended. See Documentation')
            meta.has_base = True
            klass = super().__new__(meta, name, bases, attrs)
            return klass
    return Protect  
    
class Access(type):

    __SENTINEL = object()

    def __new__(mcs, name, bases, class_dict):
        private = {key
                   for base in bases
                   for key, value in vars(base).items()
                   if callable(value) and mcs.__is_final(value)}
        if any(key in private for key in class_dict):
            raise RuntimeError('certain methods may not be overridden')
        return super().__new__(mcs, name, bases, class_dict)

    @classmethod
    def __is_final(mcs, method):
        try:
            return method.__final is mcs.__SENTINEL
        except AttributeError:
            return False

    @classmethod
    def final(mcs, method):
        method.__final = mcs.__SENTINEL
        return method


class Parent(metaclass=Access):

    @Access.final
    def do_something(self):
        """This is where some seriously important stuff goes on."""
        pass


try:
    class Child(Parent):

        def do_something(self):
            """This should not be allowed."""
            pass
except RuntimeError:
    print('Child cannot be created.')


class AnotherParent:

    def __do_something(self):
        print('Some seriously important stuff is going on.')

    def do_parent_thing(self):
        self.__do_something()


class AnotherChild(AnotherParent):

    def __do_something(self):
        print('This is allowed.')

    def do_child_thing(self):
        self.__do_something()


example = AnotherChild()
example.do_parent_thing()
example.do_child_thing()
