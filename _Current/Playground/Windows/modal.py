# -*- coding: utf-8 -*-

import ui


class WindowView (ui.View):
    
    def __init__(self, **kwargs):
        
        self.frame = (0,0, 320, 240)
        self.name = ''
        
        
        self.name = kwargs.pop('title', '')
        self.title_color = kwargs.pop('title_color','#fff')
        
        self.close_button_image = kwargs.pop(
            'close_button_image',
            ui.Image.named(
                'iow:close_circled_24'
            ).with_rendering_mode(
                ui.RENDERING_MODE_TEMPLATE
            )
        )
        
        self.title_font = kwargs.pop(
            'title_font',
            ('<System>', 14)
        )
        
        for key, value in kwargs.items():
            try:
                setattr(self, key, value)
            except:
                pass
        
        
        
        titlebar = ui.View(
            frame = (0, 0, self.width, 40),
            background_color='blue',
        )
        
        close_button = ui.Button(
            image=self.close_button_image,
            frame=(
                (titlebar.width - 25), 
                (titlebar.height - 24) / 2,
                24,
                24,
            ),
            action=self.close_window,
            flex='LTB',
            tint_color='#ccc'
            
        )
        
        title = ui.Label(
            text=self.name,
            text_color=self.title_color,
            frame=(
                5,
                0, 
                self.width - 7, 
                titlebar.height
            ),
            font=self.title_font,
            flex='WTB'
        )
        
        titlebar.add_subview(close_button)
        titlebar.add_subview(title)
        
        self.add_subview(titlebar)
    
    def close_window(self, sender=None):
        pass


if __name__ == '__main__':
    v = ui.View(
        frame=(0, 0, 320, 480),
        flex='WH',
        background_color='#fff',
    )
    test = WindowView(
        name='Terpy',
        frame=(100, 100, 100, 220)
    )
    v.add_subview(test)
    v.present('sheet')
