# -*- coding: utf-8 -*-

import boto3
import keychain

class Client (object):
    
    def __init__(self, region='us-east-1'):
        self.aws_key = keychain.get_password('devops_pro','aws_key')
        self.aws_secret = keychain.get_password('devops_pro', 'aws_secret')
        self.region = region
    
    def route53(self):
        return boto3.client('route53', aws_access_key_id=self.aws_key, aws_secret_access_key=self.aws_secret, region_name=self.region)
        
