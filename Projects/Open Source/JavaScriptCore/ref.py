# -*- coding: utf-8 -*-

"""
Classes

class JSContext

  A JSContext object represents a JavaScript execution environment. You create and use JavaScript contexts to evaluate JavaScript scripts from Objective-C or Swift code, to access values defined in or calculated in JavaScript, and to make native objects, methods, or functions accessible to JavaScript.

class JSManagedValue
  A JSManagedValue object wraps a 
  JSValue object, adding “conditional retain” behavior to provide automatic memory management of values. The primary use case for a managed value is to store a JavaScript value in an Objective-C or Swift object that is itself exported to JavaScript.
 
class JSValue
  A JSValue instance is a reference to a JavaScript value. You use the JSValue class to convert basic values (such as numbers and strings) between JavaScript and Objective-C or Swift representations in order to pass data between native code and JavaScript code. You can also use this class to create JavaScript objects that wrap native objects of custom classes or JavaScript functions whose implementations are provided by native methods or blocks.

class JSVirtualMachine

  A JSVirtualMachine instance represents a self-contained environment for JavaScript execution. You use this class for two main purposes: to support concurrent JavaScript execution, and to manage memory for objects bridged between JavaScript and Objective-C or Swift.
"""
