# -*- coding: utf-8 -*-
"""
Reticulated Download Manager

Features:
    * Download virtually any size file off the internet
    * iOS App Extensions:
      * Open URL (download file)
      * Media - Copy to configured destination folder

Planned Features:
  * Supports HTTP Basic Authentication
  * Run in background on iDevice
  * Download on Wifi, Mobile ir Both!
    * TODO: Check if wifi is enabled
  * Set bandwidth usage limits (app only)
    * Auto reset by date
    * Manual reset
    * One-time override
  * Select Download Location
  * Auto-save files in supported cloud drives
  * Custom URL Schema
  * Add support for:
    * Google Drive
    * OneDrive - Including wget
    * iCloud Drive
    * Dropbox
    * Box.net
    * AWS S3
    * Backblaze B2
    * Digital Ocean Whatevz
  
* Capability to create plugins for:
* Cloud Providers
  * Customize Interface
  * other?


Advanced:
        * Custom Headers

Open TrueCrypt Volumes:
        * https://github.com/4144414D/pytruecrypt?files=1
"""

from .views import *

