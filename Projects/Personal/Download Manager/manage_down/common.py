# -*- coding: utf-8 -*-
"""
Common Use Items
"""

from os.path import (
    exists,
    join as path_join,
    splitext as split_filename,
    basename,
    normpath,
    split as os_split,
)

import tempfile
import itertools as IT

from enum import Enum

import console
console.clear()



class File (object):
    """
    Planning on expanding this later.
    """
    
    # Duplicate File Action
    
    Exists = Enum(
        'Exists',
        (
            'APPEND',
            'AUTONAME',
            'FAIL',
            'OPEN_FILE',
            'OVERWRITE',
        )
    )
    
    # Defaults 
    
    prepend = ' ('
    append = ')'
    has_handle = False
    
    def __init__(self, 
                 filename = None,
                 duplicate_action=Exists.AUTONAME,
                 binary=False,
                 **kwargs):
                     
        self.file_mode = 'r'
        self.binary = binary
        
        
        if filename:
            self.name = filename
            
            if exists(self.name):
                if duplicate_action == self.Exists.AUTONAME:
                    self.name = self.name_unique(
                        prepend=self.prepend,
                        append=self.append
                    )
                    self.file_mode = 'w'
                
                elif duplicate_action == self.Exists.APPEND:
                    self.file_mode = 'a'
                
                elif duplicate_action == self.Exists.OVERWRITE:
                    self.file_mode = 'w'
                
                else:
                    raise IOError('File Exists')
            
    # TODO: Setup enter/exit methods
    # Enter/Exit methods:
    #     with File('filename.txt') as f:
    #         print(f.exists)
    #         print(f.name)
    # Obv. Doesn't work at the moment 
                
                
    @property
    def mode(self):
        return '{mode}{binary}'.format(
            mode=self.file_mode,
            binary = 'b' if self.binary else ''
        )
        
    def name_unique(self, name=None, 
                    prepend = '', append=''):
                        
        assert self.name
        
        path = self.name if not name else name
        def name_sequence():
            count = IT.count()
            yield ''
            while True:
                yield '{p}{n:d}{a}'.format(
                    p = prepend, 
                    n = next(count),
                    a = append,
                )
                
        orig = tempfile._name_sequence 
        with tempfile._once_lock:
            tempfile._name_sequence = name_sequence()
            path = normpath(path)
            dirname, basename = os_split(path)
            filename, ext = split_filename(basename)
            #
            
            fd, filename = tempfile.mkstemp(dir = dirname, prefix = filename, suffix = ext)
            tempfile._name_sequence = orig
            
            
        return filename

class HTTP (object):
    
    def DownloadFile(self, url):
        #local_filename = url.split('/')[-1]
        local_filename = path_join(
            '.', 
            'Documents', 
            basename(url)
        )
        
        r = requests.get(url)
        f = open(local_filename, 'wb')
        for chunk in r.iter_content(chunk_size=512 * 1024): 
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)
        f.close()
        return 
        
if __name__ == '__main__':
    import inspect
    import functools
    
    def finder():
        stack = inspect.stack()
        the_class = stack[1][0].f_locals["self"].__class__
        the_method = stack[1][0].f_code.co_name
        
        return the_class
    
    class B (object):
        name = 'boo'
        
    class A (object):
        name = 'Eh?'
        
        def b(self):
            print(finder())
            
    
    def trace(*args):
        print('running')
        print((args))
        def _trace(func):
            def wrapper(*args, **kwargs):
                print(enter_string)
                func(*args, **kwargs)
                print(exit_string)
            return wrapper
        if len(args) == 1 and callable(args[0]):
            # No arguments, this is the decorator
            # Set default values for the arguments
            enter_string = 'entering'
            exit_string = 'exiting'
            return _trace(args[0])
        else:
            # This is just returning the decorator
            enter_string, exit_string = args
            return _trace
            
    
    # A().b()
    class d(object):
        
        msg = None
        
        def __init__(self,msg='my default message'):
            self.msg = msg
            
        def __call__(self,fn=None):
            
            if self and not fn:
                #print(functools.partial(self))
                fn = functools.partial(self)
                print(fn.func)
                #print('bitch')
                msg = 'default msg'
                #print(msg)
                print(type(fn))
                print(self)
                
                stack = inspect.stack()
                #the_class = stack[1][0].f_locals["self"].__class__
                
                the_method = stack[1][0].f_code.co_name
                from pprint import pprint
                for i in stack:
                    #print(dir(i.frame.f_code))
                    for x in [ z for z in dir(i.frame.f_code) if not z.startswith('_') and 'filename' not in z ]:
                        print('{}:'.format(x))
                        pprint(getattr(i.frame.f_code, x))
                        print()
        
                print(the_method)
                #print(the_class)
                
            elif self and fn:
                msg = self.msg
            else:
                raise AttributeError('You aint doin it right')
                    
            def newfn(*args):
                print(msg)
                return fn(*args)
                
            return newfn
            
    def baked(method=None, temperature=None, duration=None):
        # If called without method, we've been called with optional arguments.
        # We return a decorator with the optional arguments filled in.
        # Next time round we'll be decorating method.
        if method is None:
            return functools.partial(baked, temperature=temperature, duration=duration)
        @functools.wraps(method)
        def f(*args, **kwargs):
            thing_to_be_baked = method(*args, **kwargs)
            return (thing_to_be_baked)
        return f
    

    @baked
    def hello():
        print('hello world !')
    

    #@baked(duration='newp')
    @d
    def too_bad():
        print('does not work')
        
    
    
    hello()
    
    
    too_bad()
        
    
    pass
