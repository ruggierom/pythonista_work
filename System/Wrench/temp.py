import argparse

# ...
parser = argparse.ArgumentParser(
    prog="uiPacker",
    epilog="Keep open source open."
)
parser.add_argument(
    "-a",
    "--auto",
    action="store_true",
    help="Auto-pack all pyui files"
)
parser.add_argument(
    "-i",
    "--ignore",
    nargs="+",
    help="Ignore pyui files - used with --auto"
)

args = parser.parse_args()

