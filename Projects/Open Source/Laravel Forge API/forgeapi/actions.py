# -*- coding: utf-8 -*-
"""
Laravel Forge API Actions

This file provides the actions to be performed
"""

class Certificates (object):
  pass
  
class Credentials (object):
  pass

class Daemons (object):
  pass
  
class FirewallRules (object):
  pass


class Jobs (object):
  pass


class MySQLDatabases (object):
  pass

class MySQLUsers (object):
  pass

class Recipes (object):
  pass

class SSHKeys (object):
  pass

class Servers (object):
  pass

class Sites (object):
  pass

class Workers (object):
  pass
  
