# -*- coding: utf-8 -*-


from bottle import Bottle, request, static_file
from util import RequiresPython

class ConnectRPDB (object):
  
  def __init__(self):
    pass
  
  def command(self, command, *args):
    """
    Send a command to rpdb remote shell
    """
    # send command
    # return output
    return
  

class WebDebuggerAPI (object):
  """
  WebDebugger API
  """
  api = Bottle()
  pdb = None
  
  def __init__(self):
    self.pdb = ConnectRPDB()
    self.breakpoints = []
  
  @api.get('/')
  def index(self):
    pass
  
  @api.get('/where')
  def where(self):
    """
    Print a stack trace, with the most recent frame at the bottom. An arrow indicates the current frame, which determines the context of most commands.
    """
    output = self.pdb.command('w')
    # parse ouput
    return
  
  @api.put('/down')
  def down(self):
    """
    Move the current frame one level down in the stack trace (to a newer frame).
    """
    output = self.pdb.command('d')
    return
  
  @api.put('/up')
  def up(self):
    """
    Move the current frame one level up in the stack trace (to an older frame).
    """
    output = self.pdb.command('u')
    return
  
  @api.put('/break')
  def b(self, filename=None, lineno=None, function=None, condition=None):
    """
    b(reak) [[filename:]lineno | function[, condition]]
    With a lineno argument, set a break there in the current file. With a function argument, set a break at the first executable statement within that function. The line number may be prefixed with a filename and a colon, to specify a breakpoint in another file (probably one that hasn’t been loaded yet). The file is searched on sys.path. Note that each breakpoint is assigned a number to which all the other breakpoint commands refer.

    If a second argument is present, it is an expression which must evaluate to true before the breakpoint is honored.
    
    Without argument, list all breaks, including for each breakpoint, the number of times that breakpoint has been hit, the current ignore count, and the associated condition if any.
    """
    output = self.pdb.command('b')
    return
    
  @api.put('/tbreak')
  def tbreak(self,filename=None, lineno=None, 
            function=None, condition=None):
    """
    Temporary breakpoint, which is removed automatically when it is first hit. The arguments are the same as break.
    """
    output = self.pdb.command('tbreak')
    return
  
  @api.put('/clear')
  def clear(self):
    """
    With a filename:lineno argument, clear all the breakpoints at this line. With a space separated list of breakpoint numbers, clear those breakpoints. Without argument, clear all breaks (but first ask confirmation).
    """
    output = self.pdb.command('cl')
    return
  
  @api.put('/disable')
  def disable(self):
    """
    Disables the breakpoints given as a space separated list of breakpoint numbers. Disabling a breakpoint means it cannot cause the program to stop execution, but unlike clearing a breakpoint, it remains in the list of breakpoints and can be (re-)enabled.
    """
    output = self.pdb.command('disble')
    return
  
  @api.put('/enable')
  def enable(self):
    """
    Enables the breakpoints specified.
    """
    output = self.pdb.command('enable')
    return
  
  @api.put('/ignore')
  def ignore(self):
    """
    Sets the ignore count for the given breakpoint number. If count is omitted, the ignore count is set to 0. A breakpoint becomes active when the ignore count is zero. When non-zero, the count is decremented each time the breakpoint is reached and the breakpoint is not disabled and any associated condition evaluates to true.
    """
    output = self.pdb.command('ignore')
    return
  
  @api.put('/condition')
  def condition(self):
    """
    Condition is an expression which must evaluate to true before the breakpoint is honored. If condition is absent, any existing condition is removed; i.e., the breakpoint is made unconditional.
    """
    output = self.pdb.command('condition')
    return
  
  @RequiresPython('2.5')
  @api.put('/commands/<bpnumber:int>')
  def commands(self, bpnumber):
    """
    Specify a list of commands for breakpoint number bpnumber. The commands themselves appear on the following lines. Type a line containing just ‘end’ to terminate the commands. An example:
      
      ```    
      (Pdb) commands 1
      (com) print some_variable
      (com) end
      (Pdb)
      ```
      
    To remove all commands from a breakpoint, type commands and follow it immediately with end; that is, give no commands.
    
    With no bpnumber argument, commands refers to the last breakpoint set.
    
    You can use breakpoint commands to start your program up again. Simply use the continue command, or step, or any other command that resumes execution.
    
    Specifying any command resuming execution (currently continue, step, next, return, jump, quit and their abbreviations) terminates the command list (as if that command was immediately followed by end). This is because any time you resume execution (even with a simple next or step), you may encounter another breakpoint—which could have its own command list, leading to ambiguities about which list to execute.
    
    If you use the ‘silent’ command in the command list, the usual message about stopping at a breakpoint is not printed. This may be desirable for breakpoints that are to print a specific message and then continue. If none of the other commands print anything, you see no sign that the breakpoint was reached.
    """
    output = self.pdb.command('condition')
    return
  
  @api.put('/step')
  def step(self):
    """
    Execute the current line, stop at the first possible occasion (either in a function that is called or on the next line in the current function).
    """
    output = self.pdb.command('s')
    return
  
  def next(self):
    """
    Continue execution until the next line in the current function is reached or it returns. (The difference between next and step is that step stops inside a called function, while next executes called functions at (nearly) full speed, only stopping at the next line in the current function.)
    """
    output = self.pdb.command('n')
    return
  
  @RequiresPython('2.6')
  @api.put('/until')
  def until(self):
    """
    Continue execution until the line with the line number greater than the current one is reached or when returning from current frame.
    """
    output = self.pdb.command('unt')
    return
  
  @api.put('/return')
  def r(self):
    """
    Continue execution until the current function returns.
    """
    output = self.pdb.command('r')
    return
  
  @api.put('continue')
  def cont(self):
    """
    Continue execution, only stop when a breakpoint is encountered.
    """
    output = self.pdb.command('c')
    return
  
  @api.put('/jump/<lineno:int>')
  def jump(self):
    """
    Set the next line that will be executed. Only available in the bottom-most frame. This lets you jump back and execute code again, or jump forward to skip code that you don’t want to run.

    It should be noted that not all jumps are allowed — for instance it is not possible to jump into the middle of a for loop or out of a finally clause.
    """
    output = self.pdb.command('c')
    return
    
  @api.get('/list/<first:int>/<last:int>')
  def list(self):
    """
    List source code for the current file. Without arguments, list 11 lines around the current line or continue the previous listing. With one argument, list 11 lines around at that line. With two arguments, list the given range; if the second argument is less than the first, it is interpreted as a count.
    """
    output = self.pdb.command('l')
    return
  
  @api.get('/args')
  def args(self):
    """
    Print the argument list of the current function.
    """
    output = self.pdb.command('a')
    return
  
  @api.get('/print')
  def p(self):
    """
    Evaluate the expression in the current context and print its value.
    
    Note print can also be used, but is not a debugger command — this executes the Python print statement.
    """
    output = self.pdb.command('p')
    return
  
  @api.get('/print/pretty')
  def pp(self):
    """
    Like the p command, except the value of the expression is pretty-printed using the pprint module.
    """
    output = self.pdb.command('p')
    return
  
  @api.post('/alias')
  def alias(self):
    """
    Creates an alias called name that executes command. The command must not be enclosed in quotes. Replaceable parameters can be indicated by %1, %2, and so on, while %* is replaced by all the parameters. If no command is given, the current alias for name is shown. If no arguments are given, all aliases are listed.

    Aliases may be nested and can contain anything that can be legally typed at the pdb prompt. Note that internal pdb commands can be overridden by aliases. Such a command is then hidden until the alias is removed. Aliasing is recursively applied to the first word of the command line; all other words in the line are left alone.

    As an example, here are two useful aliases (especially when placed in the .pdbrc file):
      ```
      #Print instance variables (usage "pi classInst")
      alias pi for k in %1.__dict__.keys(): print "%1.",k,"=",%1.__dict__[k]
      #Print instance variables in self
      alias ps pi self
      ```
    """
    output = self.pdb.command('alias')
    return
  
  @api.delete('/alias')
  def unalias(self):
    """
    Deletes the specified alias.
    """
    output = self.pdb.command('unalias')
    return
  
  @api.put('/statement/<command>')
  def statement(self, command):
    """
    Execute the (one-line) statement in the context of the current stack frame. The exclamation point can be omitted unless the first word of the statement resembles a debugger command. To set a global variable, you can prefix the assignment command with a global command on the same line, e.g.:
      (Pdb) global list_options; list_options = ['-l']
      (Pdb)
    """
    # TODO: Figure out how to do this
    args = request.POST
    output = self.pdb.command(command,**args)
    return
  
  @RequiresPython('2.6')
  @api.put('/run')
  def run(self):
    """
    Restart the debugged Python program. If an argument is supplied, it is split with “shlex” and the result is used as the new sys.argv. History, breakpoints, actions and debugger options are preserved. “restart” is an alias for “run”.
    """
    pass
  
  

class WebDebugger (object):
  """
  WebDebugger Class
  """
  app = Bottle()
  
  def __init__(self):
    pass
  
  @app.get('/')
  def index(self):
    pass
  
  
    
