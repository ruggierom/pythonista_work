# -*- coding: utf-8 -*-

import threading
import os
from bottle import Bottle, BottleException, run, request, response
from six import StringIO

import plistlib

airplay = Bottle()


class AirServerResponse (object):
  
  request = None
  response = None
  
  def __init__(self):
    pass

class AirServerException (BaseException):
  pass


class AirServer (object):
  
  app = Bottle()
  
  def __init__(self):
    super(AirServer,self).__init__()
    
    check_headers = [
      'X-Apple-Session-ID',
      'Accept-Language',      # English
    ]
    
    self.content_type_plist = 'text/x-apple-plist+xml'
    self.delegate = None
  
  
  def _header(self, key, default=None, request_object=request):
    try:
      return request_object.get(key, default)
    except AttributeError:
      return default
    except Exception as e:
      raise e
          
  @app.hook('config')
  def on_config_change(self, key, value):
    pass
  
  @airplay.get('/slideshow-features')
  def get_slideshow_features():
    """
    Return aet of transition features for slideshow
    Example Response:
      <?xml version="1.0" encoding="UTF-8"?>
      <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN"
       "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
      <plist version="1.0">
       <dict>
       <key>themes</key>
       <array>
       <dict>
       <key>key</key>
       <string>Reflections</string>
       <key>name</key>
       <string>Reflections</string>
       </dict>
       ...
       </array>
       </dict>
      </plist>
    """
    response.content_type = self.content_type_plist
    features=dict(
      themes=[]
    )
    
    try:
      f = delegate.slideshow_features
      if isinstance(f,list):
        features = f      
    except Exception:
      pass
  
    plist = plistlib.dumps(features).decode('utf8')
    return str(plist)
  
  @app.put('/photo')
  def put_photo(self):
    """
    Headers:
      X-Apple-AssetKey UUID for the picture
      X-Apple-Transition transition that should be used to show the picture
      X-Apple-AssetAction specify a caching operation
    """
    headers = request.headers
    picture_uuid = headers.pop('X-Apple-AssetKey', None)
    transition = headers.pop('X-Apple-Transition', None)
    caching = headers.pop('X-Apple-AssetAction', None)
    session = headers.pop('X-Apple-Session-ID', None)
    
    # get body data
    photo = request.body.read()
    
  

class MyAirServer (AirServer):
  
  def __init__(self):
    super(MyAirServer, self).__init__()
    self.delegate = self
    
    self.slideshow_features = [
      'Reflections',
      'Dissolve',
    ]
    
  def slideshow_photo(self, photo, transition, **kwargs):
    """
    Get slideshow photo
    """
    pass
