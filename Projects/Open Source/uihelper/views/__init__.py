# -*- coding: utf-8 -*-
"""
Pythonista uihelper views module.

Custom views and/or controls provided:

Items collected from the Pythonista Forum:  
  * Code Editor View by omz
  * Camera View (embed camera display) by gbains

Developed by Michael Ruggiero -- all in development
  * DropdownListView
  * ComboBoxView - DropdownList with Open TextField
  * AutoSuggestTextField
  * AutoSuggestTextView - Pops up from bottom of screen, can be used for snippets, emojis etc
  * Checkbox
  * RadioGoup - A modified TableView control to create a group of radio buttons
  * CheckboxGroup - Modified TableView with Checkbox Images identical functionality to RadioGroup, different look.
  * StatusbarView - Small statusbar at the bottom (Similar to Windows/Mac/Linux)
  * ToolbarView - Toolbar of Icons
  * ToolStatusbarView - Combination of Toolbar/Status Bar. Even useful?
  * ToolbarTabs - Toolbar style "tabs", a non-objc mimic of Apple's Tab Control
  
  Layout Helpers:
  
  * BoxSizer - Create equal sized compartments either horizontal or vertical. Similar to Stack View
  * Spacer - A transparent control used to take up a specified amount of space
  * TouchOverlay - An overlay which will take over the entire UI screen for touch events, except those specifically moved in FRONT of it.
  
--> Provides custom controls, and helper methods targeting Pythonista's UI module.
"""


