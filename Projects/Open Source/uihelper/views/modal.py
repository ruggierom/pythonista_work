# -*- coding: utf-8 -*-

import ui
import dialogs
from dialogs import basestring, collections

class BaseModal (ui.View):
  
  def __init__(self):
    self.corner_radius = 6
    self.ww = ui.get_screen_size()[0]
    pass
    
    
  def cover(self):
    self.flex = 'wH'
    x, y, w, h = self.frame
    
    ww = w * .95
    spacer = (w - ww) / 2
    wh = h - (2 * spacer)
    
    self.spacer = spacer
    self.ww = ww
    self.wh = wh
    
    #self.set_needs_display()
    return self
    
  def with_titlebar(self):
    pass
  
  def draw(self):
    x, y, w, h = self.superview.frame
    
    ww = w * .95
    spacer = (w - ww) / 2
    wh = h - (2 * spacer)
    self.flex = 'WH'
  
    self.frame = (spacer, spacer, 
                  ww, wh)

class BaseDialogModal (BaseModal):
  
  def __init__(self, **kwargs):
    super(BaseDialogModal, self).__init__()
    
    done_button = kwargs.pop('done_button', None)
    # Image/text etc
    
    self.close_button = ui.Button(
      image=ui.Image.named('iob:close_round_24'),
      action=self.cancel
    )
    
    self.done_button = ui.Button(
      title='Done',
      action=self.done,
    )
    
    self.toolbar = ui.View()
    self.main = ui.View()
    
  def cancel(self, sender):
    self.c.was_canceled = True
    pass
  
  def done(self, sender):
    pass
    
  

class EditListDialog (BaseDialogModal):
  def __init__(self, title='', items=None, move=True,
               delete=True, done_button_title='Done',
               delegate=None):
                 
    super(EditListDialog, self).__init__()
                 
    if not items:
      items = []
    if not isinstance(title, basestring):
      raise TypeError('title must be a string')
    if not isinstance(items, collections.Sequence):
      raise TypeError('items must be a sequence')
      
    c = dialogs._EditListDialogController(title, items, move, delete, done_button_title=done_button_title)
    c.view.flex = 'WH'
    c.view.frame = self.frame
    self.add_subview(c.view)
    c.view.wait_modal()
    self.c = c
  
  def did_action(sender):
    c = self.c
    if delegate:
      if callable(delegate):
        if c.was_canceled:
          delegate(None)
        elif None:
          delegate(list(c.view.data_source.items))
        
  
    
if __name__ == '__main__':
  v = ui.View()
  v.name = 'Test'
  v.background_color = '#ccc'
  v.flex = 'WH'
  
  tlist = ['one', 'two', 'three','four','five']
  e = EditListDialog('beep', tlist).cover()
  v.add_subview(e)
  v.present()
  

