Features
--------

- Documentation: https://pureyaml.readthedocs.org
- Open Source: https://github.com/bionikspoon/pureyaml
- MIT license
