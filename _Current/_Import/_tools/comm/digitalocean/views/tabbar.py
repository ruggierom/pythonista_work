# -*- coding: utf-8 -*-
try:
    # Note: This prevents errors when I am cleaning up code in PyCharm.
    import ui
except ImportError:
    pass


class uiTabbar (ui.View):
    """
    Helper class to create the root view, and add all subsequent needed views.
    """
    
    def __init__(self, *args, **kwargs):
        
        name = kwargs.get('name', None)
        style = kwargs.get('style', None)
        TBC = kwargs.get('tabbar', TabbarView)
        
        if isinstance(style, dict):
            for k, v in style.items():
                setattr(self, k , v)
        
        self.flex = 'WH'
        self.name = name
        self.delegate = self
        
    def add_view(self, view):
        pass
        
    def did_load(self):
        # This will be called when a view has been fully loaded from a UI file.
        pass

    def will_close(self):
        # This will be called when a presented view is about to be dismissed.
        # You might want to save data here.
        pass

    def draw(self):
        # This will be called whenever the view's content needs to be drawn.
        # You can use any of the ui module's drawing functions here to render
        # content into the view's visible rectangle.
        # Do not call this method directly, instead, if you need your view
        # to redraw its content, call set_needs_display().
        # Example:
        pass

    def layout(self):
        # This will be called when a view is resized. You should typically set the
        # frames of the view's subviews here, if your layout requirements cannot
        # be fulfilled with the standard auto-resizing (flex) attribute.
        pass

    def touch_began(self, touch):
        # Called when a touch begins.
        pass

    def touch_moved(self, touch):
        # Called when a touch moves.
        pass

    def touch_ended(self, touch):
        # Called when a touch ends.
        pass

    def keyboard_frame_will_change(self, frame):
        # Called when the on-screen keyboard appears/disappears
        # Note: The frame is in screen coordinates.
        pass

    def keyboard_frame_did_change(self, frame):
        # Called when the on-screen keyboard appears/disappears
        # Note: The frame is in screen coordinates.
        pass

# Raw class
class TabbarView (ui.View):
    """
    A Pythonista UI Class to simulate an iOS tab control.   
    """
    p = None
    neighbor_maxheight = 0
    selected = 0
    items = None
    
    def __init__(self, *items, **kwargs):
        self.items = []
        w, h = ui.get_window_size()
        
        titlebars = kwargs.pop('titlebars',1)
        bar_height = kwargs.pop('height', 50)
        bar_bgcolor = kwargs.pop('background_color','lightgrey')
        selected_bgcolor = kwargs.pop('selected_bgcolor', '#eee')
        
        # Selected item
        s = kwargs.pop('selected',None)
        if s: # Check if we are validated the selected item
            self.selected = s
        
        # Advanced use - set your own attribs
        advanced = kwargs.pop('advanced', None)
        if advanced:
            assert isinstance(advanced, dict), 'advanced attributes are set with a dict'
            # Iterate dictionary and blindly set them
            for k,v in advanced.items():
                try:
                    setattr(self,k,v)
                except:
                    pass
        
        # Advanced attribs for current item
        advanced_hl = kwargs.pop('advanced_hl', None)
        
        h = h - (65 * titlebars) # title bar
        pos_y = (h - (bar_height))

        # Leave 1px between the bar and its neighbor
        self.neighbor_maxheight = (pos_y - 1)
     
        self.background_color = bar_bgcolor
        self.frame = (0, pos_y, w, bar_height)
        self.flex = 'W'
            
        # add the items
        items_w = (w / len(items))
        self.container_vew = ''
        
        for key, view in enumerate(items):
            # add to subview
            view.flex = 'W'
            view.frame = (key*items_w, 0, items_w, bar_height)

            if key == self.selected:
                view.background_color = selected_bgcolor
                
            self.add_subview(view)
            
    def add_item(self, item):
        self.items.append(item)
        pass
    
    def add_button(self, image=None, name=None,
                   tint_color=None, title=None, 
                   action=None, **kwargs):
        btn = ui.Button()
        btn.name = name
        btn.image = image
        btn.tint_color = tint_color
        btn.title = title
        btn.action = action
        
        if kwargs:
            for k, v in kwargs.items():
                setattr(btn, k, v)
        
        self.items.append(btn)



if __name__ == '__main__':
    pass
    
