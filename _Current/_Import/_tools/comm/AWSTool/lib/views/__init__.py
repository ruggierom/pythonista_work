from .home import HomeView
from .main import AWSMain

__all__ = ['HomeView', 'AWSMain']
