import ui
from voipms import VoipMs

client = VoipMs('username', 'passwerd')


v = ui.load_view()

class SMSView (ui.View):
    def __init__(self):
        self.content_view = v
        self.content_view.flex = 'WH'
        self.content_view.frame = self.frame
        self.background_color = '#fff'
        self.flex = 'WH'
        self.touch_enabled = True
        self.content_view['view1']['textview1'].delegate = self
        self.tv = self.content_view['view1']['textview1']
        self.add_subview(self.content_view)
        self.content_view.delegate = self
        
    def touch_began(self, touch):
        # Called when a touch begins.
        print('began')
        pass

    def touch_moved(self, touch):
        # Called when a touch moves.
        pass

    def touch_ended(self, touch):
        # Called when a touch ends.
        #print('touch')
        
        pass

    def keyboard_frame_will_change(self, frame):
        # Called when the on-screen keyboard appears/disappears
        # Note: The frame is in screen coordinates.
        pass

    def keyboard_frame_did_change(self, frame):
        # Called when the on-screen keyboard appears/disappears
        # Note: The frame is in screen coordinates.
        
        # if opening, increase the height of the main view
        # remember to set scrolling position
        #print(frame)
        print('keyb changed')
        pass
    
    def close_keyboard(self):
        pass
        
    def on_keyboard_open(self):
        pass


s = SMSView()
s.present()

if __name__ == '__main__':
    """
    def download_file(url):
        local_filename = url.split('/')[-1]
        # NOTE the stream=True parameter
        r = requests.get(url, stream=True)
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024): 
                if chunk: # filter out keep-alive new chunks
                    f.write(chunk)
                    #f.flush() commented by recommendation from J.F.Sebastian
        return local_filename
    """
    

