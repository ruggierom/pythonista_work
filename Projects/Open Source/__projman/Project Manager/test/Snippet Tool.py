import ui
        

class SnippetTool (ui.View):
    """
    Snippet Tool Main Class
    
    This class is being used to manage all of the different sets of views required.
    """
    def __init__(self, **kwargs):
        """
        Class Initializer
        """
        self.flex = 'WH'
        self.name = 'Snippet Tool'
        # Embed the main content view
        self.content_view = ui.load_view()
        self.content_view.bounces = False
        self.content_view.delegate = self
        #self.content_view.flex = 'WH'
        
        self.search_position_set =  False
        self.scroll_tracker = 60
        self.is_animating = False
        
        # Setup the title bar buttons
        self.right_button_items = [
            ui.ButtonItem(
                #image=ui.Image.named('iob:search_32'),
                title='Search',
                action=self.toggle_search,
            )
        ]
        
        snippets = ui.load_view('snippets')
        snippets.name = 'Chew Chew Bitch'
        snippets.flex = 'WH'
        
        self.snippet_nav = ui.NavigationView(snippets)
        self.snippet_nav.frame = (0, 60, 320,480)
        self.snippet_nav.flex = 'WH'
        self.snippet_nav.name = ''
        self.content_view.add_subview(self.snippet_nav)
        # Manage Options
        # When I have options
        
        self.add_subview(self.content_view)
    
        
    # Built-In ui.View Methods
    def layout(self):
        _, _, w, h = self.frame
     
        #print(self.content_view.frame)
        self.content_view.frame = (0, 0, w, h+60)
        # Adjust height - not sure why bah
        self.snippet_nav.height -= 30 
        # Hide the search bar view
        if not self.search_position_set:
            #print('check')
            self.content_view.content_size = (w, h + 120)
            self.content_view.content_offset = (0, 60)
            self.search_position_set = True
    
    # ui.ScrollView Delegates
    
    def scrollview_did_scroll(self, scrollview):
        """
        Not Used ATM
        """
        pass
            
    # Title Bar Action Methods
    
    def toggle_search(self, sender=None, target=None):
        """
        Animate the scroll position change to shown the
        search field
        """
        def open():
            self.content_view.content_offset = (0, 0)
        
        def close():
            self.content_view.content_offset = (0, 60)
        
        if target:
            if target > 0:
                ui.animate(open, 0.5)
            else:
                ui.animate(close, 0.5)
            
        else:
            if self.content_view.content_offset[1] > 0:
                ui.animate(open, 0.5)
            else:
                ui.animate(close, 0.5)
            
            
        #self.content_view.content_offset = (0, 60)
        
    

v = SnippetTool()
#v = ui.load_view()
v.present('sheet')
