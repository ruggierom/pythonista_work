# -*- coding: utf-8 -*-
#!python2
"""
VRView - Create Apps/Games which are compatible with Virtual Reality head sets.

This is intended for use with VR Headsets which use the iPhone/iPod and lenses to render a VR experience.

How it works:
    ui module:
        * Takes the subviews 
"""
import ui
from abc import ABCMeta
from objc_util import *

# Load the Required Frameworks
load_framework('AssetsLibrary')
load_framework('AVFoundation')
load_framework('CoreGraphics')
load_framework('CoreMedia')
load_framework('CoreVideo')
load_framework('QuartzCore')

class AVWriter (object):
    
    current_screen = None
    framerate = None
    delegate = None
    
    def __init__(self):
        self.protocols = ['']
    
# - (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
def mailComposeController_didFinishWithResult_error_(_self, _cmd, controller, result, error):
    print('Mail composer finished')
    # Wrap the controller parameter in an `ObjCInstance`, so we can send messages:
    mail_vc = ObjCInstance(controller)
    # Set delegate to nil, and release its memory:
    mail_vc.setMailComposeDelegate_(None)
    ObjCInstance(_self).release()
    # Dismiss the sheet:
    mail_vc.dismissViewControllerAnimated_completion_(True, None)

methods = [mailComposeController_didFinishWithResult_error_]
protocols = ['MFMailComposeViewControllerDelegate']
MyMailComposeDelegate = create_objc_class('MyMailComposeDelegate', NSObject, methods=methods, protocols=protocols)

@on_main_thread
def show_mail_sheet():
    MFMailComposeViewController = ObjCClass('MFMailComposeViewController')
    mail_composer = MFMailComposeViewController.alloc().init().autorelease()
    # Use our new delegate class:
    delegate = MyMailComposeDelegate.alloc().init()
    mail_composer.setMailComposeDelegate_(delegate)
    # Present the mail sheet:
    root_vc = UIApplication.sharedApplication().keyWindow().rootViewController()
    root_vc.presentViewController_animated_completion_(mail_composer, True, None)

if __name__ == '__main__':
    show_mail_sheet()
