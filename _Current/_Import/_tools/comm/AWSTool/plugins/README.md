# AWSTool Plugins

AWSTool makes it easy to add functionality from third party developers without needing to modify the core.

It uses [PluginBase](http://pluginbase.pocoo.org) by Pocoo (The makers of the amazing Flask framework)

## Todo

- Setup example plugin
- Setup ways to do integrate, etc
- Setup a plugin installer that only works from us
