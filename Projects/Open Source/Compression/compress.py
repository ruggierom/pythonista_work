# -*- coding: utf-8 -*-
#!python2
"""
Appex Extension script to compress file(s)

Hopeful Features:
  * Appex file list to make zip/tgz file
  * GUI to select files to add
  * GUI to look at zip filenames
  * Passwords?
"""

import ui
import appex
import zipfile
import gzip
import bz2
import sys

_MODE=0 # 0 - Pythonista/Open, 1 - App Store Storage Only

class Main (object):
  
  def __init__(self):
    
    pass
    
#print(sys.version_info)
