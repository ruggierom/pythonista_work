# -*- coding: utf-8 -*-
"""
Custom Pythonista ui view types:
  * "Drop Down" Style list box
  * Combo box
  * Auto Suggest
  * Checkbox** (find this!)
  * Radio Group / 
  
  * Status Bar View 
  * Toolbar / Status Bar View
  * Tabbar View

View Helpers
  * BoxSizer (Horiz/Vert)
  * Spacer
  
"""
import ui
from six import text_type

# TODO: Setup Simple (no bg button) style
# TODO: Figure out how to set styles
# TODO:
"""
  * Make it able to drop "up" if the button is below 50%
  
  
"""

class TouchPanelView (ui.View):
  """
  Hack attempt at creating an overlay strictly for touch events
  
  Important: Remember to move views which you want to interact with in FRONT of this one!
  """
  
  def __init__(self, touch_action=None):
    self.background_color = None
    w, h = ui.get_screen_size()
    self.frame = (0,0, w, h)
    self.alpha = .1
    
    self.touch_action = touch_action
    self.touch_args = []
    self.touch_kwargs = {}
  
  def touch_began(self, touch):
    # Called when a touch begins.
    if not isinstance(self.touch_args, list):
      self.touch_args = [self.touch_args]
    
    if self.touch_action is not None:
      self.touch_action(
        *self.touch_args, 
        **self.touch_kwargs,
      )
      

class DropdownListData (ui.ListDataSource):
  
  def __init__(self, items):
    super(DropdownListData, self).__init__(items)
    self.move_enabled = False
    self.delete_enabled = False
    self.accessory_action = None
  
  def mkitem(self, item):
    self.items.append(item)
    self.reload()
  
  def rmitem(self, item):
    if item in self.items:
      return bool(self.items.pop(item, False))

class DropdownListView (ui.View):
  
  arrow_image = None

  def __init__(self, items, **kwargs):

    self.items = items
    self.name='dropdown'
    self.width = 200
    self.height = 32
    self.button_overlay = 4
    #self.background_color = '#fff'
    self.dropdown_presented = False
    
    self.touch_panel = TouchPanelView(self.hide_dropdown)
    
    
    
    # Setup keyword args
    
    self.value = kwargs.pop('default', None)
    self.arrow_style = kwargs.pop('arrow_style', None)
    self.dropdown_style = kwargs.pop('dropdown_style', None)
    
    
    # defaults
    
    # Default dropdown arrow image
    self.dropdown_arrow(
      'iob:arrow_down_b_24',
      dict(
        
      )
    )    
      
    # Create Button
    self.dropdown_button = ui.Button()
    self.dropdown_button_style = dict(
      name='{0}_button'.format(self.name),
      width=32,
      height=32,
      background_color='#666',
      tint_color='#f7f7f7',
      #border_width=0.5,
      corner_radius=2,
      border_color='#d3d3d3',
      action=self.toggle_dropdown,
      #flex='LB',
    )
    
  
    # Create TextField
    
    self.dropdown = ui.TextField()
    
    self.dropdown_tf_style = dict(
      name='{0}_field'.format(self.name),
      width=self.width,
      flex='WH',
      height=32,
      x=0,
      y=0,
      spellchecking_type=False,
      autocapitalization_type=False,
      enabled=False,
    )
    
    # Allow to override anything
    for key, value in kwargs.items():
      setattr(self, key, value)
    
    # Setup TableView
    self.items_delegate = DropdownListData(self.items)
    self.items_delegate.action = self.select_item
    
    tv = ui.TableView()
    tv.allows_multiple_selection = False
    tv.row_height = 32
    tv.separator_color = self.dropdown.background_color
    tv.background_color = self.dropdown.background_color
    tv.delegate = self.items_delegate
    tv.data_source = self.items_delegate
    tv.corner_radius = 4
    
    self.list_view = tv 
    
    self.add_subview(self.dropdown)
    self.add_subview(self.dropdown_button)
    
  def toggle_dropdown(self, sender):
    """
    Toggle the dropdown. Steps happening here:
      
      
    """
    if self.dropdown_presented:
      self.superview.remove_subview(self.touch_panel)
      self.superview.remove_subview(self.list_view)
      self.dropdown_presented = False
      self.update_interval = 0.0
    else:
      self.list_view.flex = 'T'
      self.list_view.border_width = 1
      self.list_view.border_color = '#eee'
      # TODO: Move styling
      self.dropdown_presented = True
      self.update_interval = 0.3
      self.superview.add_subview(self.touch_panel)
      self.superview.add_subview(self.list_view)
      
    pass
  
  def hide_dropdown(self, sender = None):
    if self.dropdown_presented:
      self.toggle_dropdown(sender)
    pass
    
  
  @staticmethod
  def _set_style(view, style):
    assert isinstance(style, dict)
    
    try:
      for key, value in style.items():
        setattr(view, key, value)
    except AttributeError:
      pass
    
  def select_item(self, sender):
    val = self.items_delegate.items[sender.selected_row]
    self.dropdown.text = val
    self.toggle_dropdown(sender)
    pass
    
    
  def draw(self):
    self._set_style(self.dropdown, self.dropdown_tf_style)
    self._set_style(
      self.dropdown_button,  
      self.dropdown_button_style
    )
    self.dropdown_button.x = (
      self.width - self.dropdown_button.width
    )
    self.dropdown_button.image = self.arrow_image
    self.dropdown.width = (
      self.width - self.dropdown_button.width + self.button_overlay
    )
    
    
  
  def update(self):
    """
    Since I can't redraw based on layout change / rotation since it won't be the superview, I am checking on half second intervals, IF the dropdown is presented - it will refresh the layout
    
    The check here is just because I am too lazy to check performance. Setting the interval to 
    """
    
    if self.dropdown_presented:
      self.set_needs_display()

  
  def layout(self):
    # set the frame for the dropdown
    
    # calculate width
    w, h = ui.get_screen_size()
    
    # Max width, leave 10px from end of screen
    max_item_length = max(
      (len(i) for i in self.items_delegate.items)
    )
    
    max_width = min(
      ((w - self.x - 10), max_item_length)
    )
    
    max_height = min(
      self.list_view.row_height * len(items),
      (h - self.y - self.height - 75),
    )
   
    # set frame
    self.list_view.frame = (
      self.x, 
      (self.y+self.height),
      max((self.width, max_width)),
      max_height
    )
    
    
  """
  def touch_began(self, touch):
    # Hide dropdown if the user clicks the box
    self.hide_dropdown(None)
    pass
    
  """

  def dropdown_arrow(self, arrow, styles=None):
    if isinstance(arrow, ui.Image):
      self.arrow_image = arrow
    elif isinstance(arrow, text_type):
      self.arrow_image = ui.Image.named(arrow)
      
    if isinstance(styles, dict):
      for key, value in styles.items():
        setattr(self.arrow_image, key, value)
      
    return self
  

class TextFieldSuggest (ui.View):

  def __init__(self, **kwargs):
    for key, value in kwargs.items():
      setattr(self, key, value)


# for reference
class MyView (ui.View):
  def __init__(self):
    # This will also be called without arguments when the view is loaded from a UI file.
    # You don't have to call super. Note that this is called *before* the attributes
    # defined in the UI file are set. Implement `did_load` to customize a view after
    # it's been fully loaded from a UI file.
    pass

  def did_load(self):
    # This will be called when a view has been fully loaded from a UI file.
    pass

  def will_close(self):
    # This will be called when a presented view is about to be dismissed.
    # You might want to save data here.
    pass

  def draw(self):
    # This will be called whenever the view's content needs to be drawn.
    # You can use any of the ui module's drawing functions here to render
    # content into the view's visible rectangle.
    # Do not call this method directly, instead, if you need your view
    # to redraw its content, call set_needs_display().
    # Example:
    pass

  def layout(self):
    # This will be called when a view is resized. You should typically set the
    # frames of the view's subviews here, if your layout requirements cannot
    # be fulfilled with the standard auto-resizing (flex) attribute.
    pass

  def touch_began(self, touch):
    # Called when a touch begins.
    pass

  def touch_moved(self, touch):
    # Called when a touch moves.
    pass

  def touch_ended(self, touch):
    # Called when a touch ends.
    pass

  def keyboard_frame_will_change(self, frame):
    # Called when the on-screen keyboard appears/disappears
    # Note: The frame is in screen coordinates.
    pass

  def keyboard_frame_did_change(self, frame):
    # Called when the on-screen keyboard appears/disappears
    # Note: The frame is in screen coordinates.
    pass

# For reference
class TextFieldDelegate (object):

  def textfield_should_begin_editing(self, textfield):
    return True
  def textfield_did_begin_editing(self, textfield):
    pass
  def textfield_did_end_editing(self, textfield):
    pass
  def textfield_should_return(self, textfield):
    textfield.end_editing()
    return True
  def textfield_should_change(self, textfield, range, replacement):
    return True
  def textfield_did_change(self, textfield):
    pass


if __name__ == '__main__':
  v = ui.View()
  v.name = 'pecker'
  v.flex = 'WH'
  v.background_color = '#fff'
  items = ['one', 'two', 'three', 'four', 'five','six',
  'seven','eight','none','ten']
  items = items + items
  dd = DropdownListView(items)
  dd.frame = (15, 10, 220, 32)

  v.add_subview(dd)
  v.present()
