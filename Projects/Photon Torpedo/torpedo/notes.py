# -*- coding: utf-8 -*-

from objc_util import *
import os
import ui
import time
import threading
from six import text_type
try:
  import logs
except ImportError:
  from . import logs

C = ObjCClass

logs.Logger.basicConfig(logs.ERROR)

class Headless (ui.View):
  
  @on_main_thread
  def __init__(self, category='uncategorized', warmup=0.2):
    #self.info('Initializing Camera')
    self.photos_dir = os.path.join('photos', category)
    self.category = category
    self.session = C('AVCaptureSession').new().autorelease()
    self.session.sessionPreset = 'AVCaptureSessionPresetPhoto'
    self.device = C('AVCaptureDevice').defaultDeviceWithMediaType_('vide')
    self.device_input = C('AVCaptureDeviceInput').deviceInputWithDevice_error_(self.device, None)
    self.session.addInput_(self.device_input)
    self.image_output = C('AVCaptureStillImageOutput').new().autorelease()
    self.session.addOutput_(self.image_output)
    self.session.startRunning()
    #self.debug('Camera session initialized')
    
    # NOTE: You may need to adjust this to wait for the camera to be ready (use a higher number if you see black photos):
    #time.sleep(warmup)

  @on_main_thread
  def snap(self,filename='photo.jpg'):
    
      def handler_func(_block, _buffer, _err):
          buffer = ObjCInstance(_buffer)
          img_data = C('AVCaptureStillImageOutput').jpegStillImageNSDataRepresentation_(buffer)
          img_data.writeToFile_atomically_(filename, True)
          e.set()
          
      video_connection = None
      for connection in self.image_output.connections():
          for port in connection.inputPorts():
              if str(port.mediaType()) == 'vide':
                  video_connection = connection
                  break
          if video_connection:
              break
              
      e = threading.Event()
      handler = ObjCBlock(handler_func, restype=None, argtypes=[c_void_p, c_void_p, c_void_p])
      retain_global(handler)
      #self.image_output.captureStillImageAsynchronouslyFromConnection_completionHandler_(video_connection, handler)
      e.wait()
  
  @on_main_thread
  def snap_next(self, file_prefix='photo'):
    base = os.path.join(
      os.path.abspath('.'),
      self.photos_dir,
    )
    #self.debug('Set photo directory to: {0}'.format(self.photos_dir))
    
    try:
      os.makedirs(self.photos_dir)
    except OSError as e:
      import errno
      if e.errno == errno.EEXIST:
        #self.debug('Directory already exists')
        pass
      else:
        self.error(e)
        raise e
        
    #self.debug('Scanning for existing photos...')
    
    files = os.listdir(self.photos_dir)
    item_number = [0]
    
    for item in files:
      item_parts = str(item).lower().split('.')
      if item_parts[-1] in ('jpg','jpeg',):
        #self.debug('Found photo: {0}'.format(item))
        item_noext = '.'.join(item_parts[:-1])

        for d in item_noext:
          try:
            item_number.append(int(d))
          except ValueError:
            item_number = [0]
        
    next_item_number = max(item_number) + 1
    """
    self.debug(
      'Using next item number: {}'.format(next_item_number)
    )
    """
    #print('next item')
    filename = '{prefix}{num:04d}.jpg'.format(
      prefix=file_prefix,
      num=next_item_number
    )
    
    file_path = os.path.join(self.photos_dir,filename)
    self.snap(file_path)
    self.debug('Snapped')
    return file_path
  
  def debug(self, d):
    print(d)
  
  @on_main_thread
  def snap_next_with_note(self, note, file_prefix='photo'):
    img = self.snap_next(file_prefix)
    note_filename = img + '_note.txt', 
    pass
  

selection = None

class NotesDelegate (object):
    def textview_should_begin_editing(self, textview):
        return True
    def textview_did_begin_editing(self, textview):
        pass
    def textview_did_end_editing(self, textview):
        pass
    def textview_should_change(self, textview, range, replacement):
        return True
    def textview_did_change(self, textview):
        pass
    def textview_did_change_selection(self, textview):
        global selection
        print(dir(textview))
        pass

class WorkNotes (object):
  def __init__(self):
    import os
    view_file = os.path.join(
      os.path.dirname(
        os.path.abspath(__file__)
      ), 'views','notes'
    )
    self.main = ui.load_view(view_file)
    self.main.background_color = '#999'
    self.main.right_button_items = [
      ui.ButtonItem(
        title='Save',
        action=self.save_note
      ),
    ]
    
    self.text_editor = self.main['text_editor']
    
  
  def save_note(self, sender):
    pass
  
  def clipboard(self, sender):
    s, f = self.text_editor.selected_range
    note = self.text_editor.text[s:f]
    self.headless.snap_next_with_note(note)
    pass
  
  def do_snap(self, sender):
    #t = threading.Thread(target=self.headless.snap_next)
    #t.run()
    
    ui.in_background(Headless().snap_next())
  
  def run(self):
    self.main.present('fullscreen')


def main():
  WorkNotes().run()

t = threading.Thread(target=main)
t.daemon = True
t.run()
while True:
  pass

