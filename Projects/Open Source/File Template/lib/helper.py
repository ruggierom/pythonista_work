# -*- coding: utf-8 -*-
try:
  # Note: This prevents errors when I am cleaning up code in PyCharm.
  import ui
  from objc_util import *
except ImportError:
  pass

import sys


class uiHelper (object):
  """
  uiHelper is a set of functions to help a developer while working with Pythonista ui

  Methods:
      get_item_by_attr(key, value) - Returns ui objects with the specified attribute.
      get_item_by_name(name) - Returns ui objects with the specified name

  """

  def __init__(self, root_view):
    """
    Class Initializer
    :param root_view: The root view in your project.
    """
    self.root = root_view

  def get_item_with_attr(self, key, cll=False):
    """
    Find a ui object ifnit has a specific attr
    :param key: The attribute key (i.e. name, action)
    :param value: The value you wish to look for
    :return: list: List of discovered objects (if any). Empty list if None.
    """
    output = []

    def find_recursive(key, view):

      try:
        
        if hasattr(view, key):
          if cll:
            if callable(getattr(view, key)):
              output.append(view)
          else:
            output.append(view)
      except Exception as e:
        pass # In case we're looking for an attribute which isn't common to all
      finally:
        subviews = view.subviews if hasattr(view,'subviews') else []

        for v in subviews:
          find_recursive(key, v)

    # Recursively search the root view and all subviews
    find_recursive(key,self.root)

    return output

  def get_item_by_attr(self, key, value):
    """
    Find a ui object by one of it's attributes
    :param key: The attribute key (i.e. name, action)
    :param value: The value you wish to look for
    :return: list: List of discovered objects (if any). Empty list if None.
    """
    output = []

    def find_recursive(key, value, view):
      try:
        if getattr(view, key) == value:
          output.append(view)
      except:
        pass # In case we're looking for an attribute which isn't common to all
      finally:
        subviews = view.subviews if hasattr(view,'subviews') else []

        for v in subviews:
          find_recursive(key, value, v)

    # Recursively search the root view and all subviews
    find_recursive(key, value,self.root)

    return output

  def get_item_by_name(self, name):
    """
    Find an attribute by name
    :param name: The name you wish to search for
    :return: list: List of discovered objects with matching name, if any. Empty list if None.
    """
    return self.get_item_by_attr('name', name)

  def get_button_item_location(self, btn):
    """
    Get the x coordinate of a button item (y is irrelevant in ButtonItems since the view does
    not have access to the title bar.
    :param btn: object: The button object you wish to track
    :return: (int|float|bool): x coordinate of ButtonItem. False if not in view

    Full Disclosure:
        I developed this when I was extremely tired - but had the idea. The math could likely
        be simplified, which I will look at later, it was trial and error since I could not
        find any real info on measuring the buttons.

        I plan on loading a screenshot up into a graphics program and measure the actual sizes.
        But that's later.
    """
    view = self.root
    assert hasattr(view, 'right_button_items'), 'Invalid view provided'


    # Attempt to measure the width of the button, by text or image.
    w = 0
    if btn.title:
      tw = ui.measure_string(btn.title)
      w+= tw
    if btn.image is not None:
      w+= 32

    # 32 is an approximation I found in my tests.  If they differ let me know.

    wi = view.width

    if btn in view.right_button_items:
      i = view.right_button_items.index(btn)
      i += 1 # since lists start at 0
      p = w
      o = 10
      if i > 1:
        p = ((i+(i-1))* w)
      if i > 2:
        o = 5 # Extra offset

      x = (wi - (p) - o)

    elif btn in view.left_button_items:
      i = view.left_button_items.index(btn)
      i += 1 # since lists start at 0
      p = (w*2)
      if i > 1:
        p = ((i+2)* w) - 8

      x = (0 + (p) + 10)

    else:
      return False

    return x

  def dismiss_keyboard(self):
    txt_views = self.get_item_with_attr('end_editing')
    for view in txt_views:
      #print(view)
      ui.animate(view.end_editing, 0.5)
    
    pass

  def isview(self, obj):
    import inspect  # Only needed if you call this method.

    assert not isinstance(obj, type), 'Please initialize the view before adding.'
    cls = obj.__class__
    if ui.View in inspect.getmro(cls):
      return True

    return False
  
  @staticmethod
  def font_family_list():
    """
    List all font families
    """

    UIFont = ObjCClass('UIFont')
    all_fonts = []
    families = UIFont.familyNames()
    for family in families:
      names = UIFont.fontNamesForFamilyName_(family)
      all_fonts += names

    out= []
    for f in all_fonts:
      out.append(str(f))
    return out

if __name__ == '__main__':
  """
  Used for testing
  """
