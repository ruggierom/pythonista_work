import csv
import os, glob
import sys
import xlsxwriter


## csv lines 1-5 are useless

def convert_to_xlsx(inpath='.' , outpath='./'):
    #listOfFiles = os.listdir(directory)           #  list of all files in the directory
    listOfFiles = glob.glob("*.csv")                       
    for index, fileInList in enumerate(listOfFiles):     
        fileName  = fileInList[0:fileInList.find(inpath)]     
        excelFile = xlsxwriter.Workbook(fileName + '.xlsx')
        worksheet = excelFile.add_worksheet() 
        with open(fileInList, 'rb') as f:   
            content = csv.reader(f)
            for index_row, data_in_row in enumerate(content):
                for index_col, data_in_cell in enumerate(data_in_row):
                    worksheet.write(index_row, index_col, data_in_cell)
    
    excelFile.close()
    print(" === Conversion is done ===")

if __name__ == '__main__':
    price_index = 'https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/index.{ext}'
    
    import requests
    import json

    r = requests.get(
        price_index.format(ext='json')
    )
    injson = r.json()

    with open('data/categories.json','w') as f:
        json.dump(injson,f,indent=2)
    
