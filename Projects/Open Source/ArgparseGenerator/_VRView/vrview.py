# -*- coding: utf-8 -*-
#!python2
"""
VRView - Create Apps/Games which are compatible with Virtual Reality head sets.

This is intended for use with VR Headsets which use the iPhone/iPod and lenses to render a VR experience.

How it works:
    ui module:
        * Takes the subviews 
"""
import ui
from abc import ABCMeta
from objc_util import *

# Load the Required Frameworks
load_framework('AssetsLibrary')
load_framework('AVFoundation')
load_framework('CoreGraphics')
load_framework('CoreMedia')
load_framework('CoreVideo')
load_framework('QuartzCore')


