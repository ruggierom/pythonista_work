import os, shutil, tempfile, console

source1 = os.path.expanduser('site-packages/')
source2 = os.path.expanduser('site-packages-2/')
source3 = os.path.expanduser('site-packages-3/')

doc_path = os.path.expanduser('~/Documents')
os.chdir(doc_path)
backup_path = os.path.join(doc_path, 'Backup.zip')
if os.path.exists(backup_path):
    os.remove(backup_path)
print('Creating backup archive...')
shutil.make_archive(os.path.join(tempfile.gettempdir(), 'Backup'), 'zip')
shutil.move(os.path.join(tempfile.gettempdir(), 'Backup.zip'), backup_path)
os.chdir(os.path.expanduser('~/Documents'))
dest = os.path.expanduser('~/Documents/sp.tar.gz')



all_src = [source1,source2,source3]

#make_tarfile(dest,source1)
