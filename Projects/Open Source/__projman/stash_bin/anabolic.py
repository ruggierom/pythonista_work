#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Anabolic Stash Library.

This is a shared library, but also the anabolic config utility.
"""

## Shared Library
import json
import six
import sys
import os
import errno

##########################################################
######                config module                 ######
##########################################################

# Constants

SAVE_RETURN_VOID=0
SAVE_RETURN_BOOL=1
SAVE_RETURN_CONFIG=2
SAVE_RETURN_SELF=3

SET_RETURN_VOID=0
SET_RETURN_BOOL=1
SET_RETURN_CONFIG=2
SET_RETURN_SELF=3


class JSONConfig (object):

    # Empty, shown for IDE Hinting
    config = {}
    config_file = None
    default_config = {}


    # Defaults
    autosave = True
    
    def __init__(self, config_file = 'app_config.json',autosave = True, **kwargs):
        self.config_file = config_file

        # Misc Options
        if kwargs:
            self.default_config = kwargs.pop('default_config',{})

            # Quick switch to enable cascading
            cascading = kwargs.pop('cascading',False)
            if cascading:
                self.save_return_type = SAVE_RETURN_SELF
                self.set_return_type = SET_RETURN_SELF
            else:
                self.save_return_type = SAVE_RETURN_VOID
                self.set_return_type = SET_RETURN_VOID


            # Overriding individual return types will still work

            self.save_return_type = kwargs.pop('save_return_type',SET_RETURN_SELF)
            self.set_return_type = kwargs.pop('set_return_type', SET_RETURN_SELF)

            cascade_set = kwargs.pop('cascade_set', True)

            if cascade_set:
                self.set_return_type = SET_RETURN_SELF


        if not autosave:
            self.autosave = False
        
        try:
            with open(config_file, 'r') as f:
                self.config = json.load(f)
        except IOError:
            try:
                assert isinstance(self.default_config, dict)
                
                with open(config_file, 'w') as f:
                    json.dump(self.default_config, f, indent=2)
                
                self.config = self.default_config
            except Exception as e:
                six.reraise(BaseException,e)
        except Exception as e:
            six.reraise(BaseException,e)
        pass


    def cascade(self, cascade=None):
        if cascade is not None:
            if cascade == True:
                self.save_return_type = SAVE_RETURN_SELF
                self.set_return_type = SET_RETURN_SELF
                return
            else:
                self.save_return_type = SAVE_RETURN_VOID
                self.set_return_type = SET_RETURN_VOID
                return
        else:
            if self.save_return_type == SAVE_RETURN_SELF \
                    and self.set_return_type == SET_RETURN_SELF:
                return True

            return False
    
    def set(self, option, value):
        self.config.update({option: value})

        if self.autosave == True:
            _ = self.save()

        if self.set_return_type == 0:
            return
        elif self.set_return_type == 1:
            return True
        elif self.set_return_type == 2:
            return self.config
        elif self.set_return_type == 3:
            return self
        else:
            # If it's wrong.. return nothing
            return

    
    def get(self, option):
        if option in self.config:
            return self.config[option]
        elif option in self.default_config:
            return self.default_config[option]
        else:
            raise KeyError('{0} not found in config'.format(option))
    
    def save(self):
        try:
            with open(self.config_file, 'w') as f:
                    json.dump(self.config,f, indent=2)

            if self.save_return_type == 0:
                return
            elif self.save_return_type == 1:
                return True
            elif self.save_return_type == 2:
                return self.config
            elif self.save_return_type == 3:
                return self
            else:
                # If it's wrong.. return nothing
                return
        except Exception as e:
            six.reraise(BaseException,e)
            # Just in case
            return False

class ConfigAnabolic (object):
    """
    Implementation of JSONConfig(), using env vars which will
    be set within .stashrc file (optionally)
    """
    
    def _init__(self):
        config_file = self._envpath(
            'ANABOLIC_CONFIG_FILE',
            '~/.anabolic/config.json'
        )
        self.config = JSONConfig(config_file)
        
    
    def _env(self, var, default=None):
        return os.environ.get(var.upper(), default)
    
    def _envpath(self, var, default=None):
        path = self._env(var, default)
        if path:
            try:
                os.makedirs(
                    os.path.dirname(
                        os.path.expanduser(path)
                    )
                )
                return path
            except IOError as e:
                if e.errno != errno.EEXIST:
                    # Sound the alarm, this aint right
                    raise e
                    
        return None
    
    def get(self, var, default=None, path=False):
        value = self._envpath(var, default) if path else self._env(var, default)
        
        if not value:
            try:
                value = self.config.get(var)
            except KeyError:
                value = None
            
        return value
    
    def set(self, var, value):
        return self.config.set(var, value)


class HistoryFile (object):
    """
    Setup directory History
    """
    def __init__(self, file_path, max_lines):
        """
        Class Initializer - 
        """
        pass
            

##########################################################
######            config implementation             ######
##########################################################


config = ConfigAnabolic()

__all__ = ['JSONConfig', 'SAVE_RETURN_CONFIG',
           'SAVE_RETURNE_BOOL','SAVE_RETURN_NONE',
           'SET_RETURN_CONFIG', 'SET_RETURNE_BOOL', 
           'SET_RETURN_NONE', 'config']

## Main
if __name__ == '__main__':
    import dialogs
    
    
    # this is where we do stuffs
