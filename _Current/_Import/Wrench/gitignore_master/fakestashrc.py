# -*- coding: utf-8 -*-
"""
The prompt is customizable with the PROMPT environment variable.
\w - current working directory with HOME folder abbreviated as ~
\W - last path component of current working directory
All other strings are displayed literally
The default setting is PROMPT='[\W]$ '
"""

## Aliases

# http://checkip.dyndns.com/
# http://wtfismyip.com/text
# Get your IP address

alias myip="curl http://checkip.amazonaws.com/"
# Backup - the below one goes down a lot
alias myipbk="curl http://icanhazip.com"

# Helper Aliases
alias ccd='PREVDIR=`pwd`; cd "$1"'

def testingstrawberries():
    print('troopytooooop')
