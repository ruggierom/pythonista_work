# -*- coding: utf-8 -*-
from typing import List

def greeting(name: str) -> str:
    return 'Hello ' + name
