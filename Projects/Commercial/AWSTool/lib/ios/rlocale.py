# -*- coding: utf-8 -*-

import sys
import encodings
import encodings.aliases
import re
import collections
from builtins import str as _builtin_str
import functools

# bring in some useful tools
try:
    from _locale import strcoll, strxfrm
except ImportError:
    try:
        from locale import _strcoll, _strxfrm
    except ImportError:
        def _strcoll(a,b):
            """ strcoll(string,string) -> int.
                Compares two strings according to the locale.
            """
            return (a > b) - (a < b)
        
        def _strxfrm(s):
            """ strxfrm(string) -> string.
                Returns a string that behaves for cmp locale-aware.
            """
            return s
            
        # These may or may not exist in _locale, so be sure to set them.
        if 'strxfrm' not in globals():
            strxfrm = _strxfrm
        if 'strcoll' not in globals():
            strcoll = _strcoll

try:
    from _locale import LC_ALL, LC_COLLATE, LC_CTYPE, LC_MESSAGES, LC_MONETARY
    from  _locale import LC_NUMERIC, LC_TIME
    Error = ValueError
except:
  pass

# Bring in pythonista libs
try:
    from objc_util import *
    PYTHONISTA=True
except ImportError:
    from locale import *

# if PYTHONISTA:
# tab these when done
__all__ = ["getlocale", "getdefaultlocale", "getpreferredencoding", "Error",
           "setlocale", "resetlocale", "localeconv", "strcoll", "strxfrm",
           "str", "atof", "atoi", "format", "format_string", "currency",
           "normalize", "LC_CTYPE", "LC_COLLATE", "LC_TIME", "LC_MONETARY",
           "LC_NUMERIC", "LC_ALL", "CHAR_MAX"]



def localeconv():
    """ localeconv() -> dict.
        Returns numeric and monetary locale-specific parameters.
    """
    # 'C' locale default values
    return {'grouping': [127],
            'currency_symbol': '',
            'n_sign_posn': 127,
            'p_cs_precedes': 127,
            'n_cs_precedes': 127,
            'mon_grouping': [],
            'n_sep_by_space': 127,
            'decimal_point': '.',
            'negative_sign': '',
            'positive_sign': '',
            'p_sep_by_space': 127,
            'int_curr_symbol': '',
            'p_sign_posn': 127,
            'thousands_sep': '',
            'mon_thousands_sep': '',
            'frac_digits': 127,
            'mon_decimal_point': '',
            'int_frac_digits': 127}


"""
['ABDAY_1', 'ABDAY_2', 'ABDAY_3', 'ABDAY_4', 'ABDAY_5', 'ABDAY_6', 'ABDAY_7', 'ABMON_1', 'ABMON_10', 'ABMON_11', 'ABMON_12', 'ABMON_2', 'ABMON_3', 'ABMON_4', 'ABMON_5', 'ABMON_6', 'ABMON_7', 'ABMON_8', 'ABMON_9', 'ALT_DIGITS', 'AM_STR', 'CHAR_MAX', 'CODESET', 'CRNCYSTR', 'DAY_1', 'DAY_2', 'DAY_3', 'DAY_4', 'DAY_5', 'DAY_6', 'DAY_7', 'D_FMT', 'D_T_FMT', 'ERA', 'ERA_D_FMT', 'ERA_D_T_FMT', 'ERA_T_FMT', 'Error', 'LC_ALL', 'LC_COLLATE', 'LC_CTYPE', 'LC_MESSAGES', 'LC_MONETARY', 'LC_NUMERIC', 'LC_TIME', 'MON_1', 'MON_10', 'MON_11', 'MON_12', 'MON_2', 'MON_3', 'MON_4', 'MON_5', 'MON_6', 'MON_7', 'MON_8', 'MON_9', 'NOEXPR', 'PM_STR', 'RADIXCHAR', 'THOUSEP', 'T_FMT', 'T_FMT_AMPM', 'YESEXPR', '__all__', '__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', '_append_modifier', '_build_localename', '_builtin_str', '_format', '_group', '_grouping_intervals', '_localeconv', '_override_localeconv', '_parse_localename', '_percent_re', '_print_locale', '_replace_encoding', '_setlocale', '_strcoll', '_strip_padding', '_strxfrm', '_test', 'atof', 'atoi', 'collections', 'currency', 'delocalize', 'encodings', 'format', 'format_string', 'functools', 'getdefaultlocale', 'getlocale', 'getpreferredencoding', 'k', 'locale_alias', 'locale_encoding_alias', 'localeconv', 'nl_langinfo', 'normalize', 're', 'resetlocale', 'setlocale', 'str', 'strcoll', 'strxfrm', 'sys', 'v', 'windows_locale']


dir(_locale)
['ABDAY_1', 'ABDAY_2', 'ABDAY_3', 'ABDAY_4', 'ABDAY_5', 'ABDAY_6', 'ABDAY_7', 'ABMON_1', 'ABMON_10', 'ABMON_11', 'ABMON_12', 'ABMON_2', 'ABMON_3', 'ABMON_4', 'ABMON_5', 'ABMON_6', 'ABMON_7', 'ABMON_8', 'ABMON_9', 'ALT_DIGITS', 'AM_STR', 'CHAR_MAX', 'CODESET', 'CRNCYSTR', 'DAY_1', 'DAY_2', 'DAY_3', 'DAY_4', 'DAY_5', 'DAY_6', 'DAY_7', 'D_FMT', 'D_T_FMT', 'ERA', 'ERA_D_FMT', 'ERA_D_T_FMT', 'ERA_T_FMT', 'Error', 'LC_ALL', 'LC_COLLATE', 'LC_CTYPE', 'LC_MESSAGES', 'LC_MONETARY', 'LC_NUMERIC', 'LC_TIME', 'MON_1', 'MON_10', 'MON_11', 'MON_12', 'MON_2', 'MON_3', 'MON_4', 'MON_5', 'MON_6', 'MON_7', 'MON_8', 'MON_9', 'NOEXPR', 'PM_STR', 'RADIXCHAR', 'THOUSEP', 'T_FMT', 'T_FMT_AMPM', 'YESEXPR', '__doc__', '__loader__', '__name__', '__package__', '__spec__', 'localeconv', 'nl_langinfo', 'setlocale', 'strcoll', 'strxfrm']


Help on module locale:

NAME
    locale - Locale support.

DESCRIPTION
    The module provides low-level access to the C lib's locale APIs
    and adds high level number formatting APIs as well as a locale
    aliasing engine to complement these.
    
    The aliasing engine includes support for many commonly used locale
    names and maps them to values suitable for passing to the C lib's
    setlocale() function. It also includes default encodings for all
    supported locale names.

CLASSES
    builtins.Exception(builtins.BaseException)
        Error
    
    class Error(builtins.Exception)
     |  Common base class for all non-exit exceptions.
     |  
     |  Method resolution order:
     |      Error
     |      builtins.Exception
     |      builtins.BaseException
     |      builtins.object
     |  
     |  Data descriptors defined here:
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from builtins.Exception:
     |  
     |  __init__(self, /, *args, **kwargs)
     |      Initialize self.  See help(type(self)) for accurate signature.
     |  
     |  __new__(*args, **kwargs) from builtins.type
     |      Create and return a new object.  See help(type) for accurate signature.
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from builtins.BaseException:
     |  
     |  __delattr__(self, name, /)
     |      Implement delattr(self, name).
     |  
     |  __getattribute__(self, name, /)
     |      Return getattr(self, name).
     |  
     |  __reduce__(...)
     |      helper for pickle
     |  
     |  __repr__(self, /)
     |      Return repr(self).
     |  
     |  __setattr__(self, name, value, /)
     |      Implement setattr(self, name, value).
     |  
     |  __setstate__(...)
     |  
     |  __str__(self, /)
     |      Return str(self).
     |  
     |  with_traceback(...)
     |      Exception.with_traceback(tb) --
     |      set self.__traceback__ to tb and return self.
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from builtins.BaseException:
     |  
     |  __cause__
     |      exception cause
     |  
     |  __context__
     |      exception context
     |  
     |  __dict__
     |  
     |  __suppress_context__
     |  
     |  __traceback__
     |  
     |  args

FUNCTIONS
    atof(string, func=<class 'float'>)
        Parses a string as a float according to the locale settings.
    
    atoi(string)
        Converts a string to an integer according to the locale settings.
    
    currency(val, symbol=True, grouping=False, international=False)
        Formats val according to the currency settings
        in the current locale.
    
    format(percent, value, grouping=False, monetary=False, *additional)
        Returns the locale-aware substitution of a %? specifier
        (percent).
        
        additional is for format strings which contain one or more
        '*' modifiers.
    
    format_string(f, val, grouping=False)
        Formats a string in the same way that the % formatting would use,
        but takes the current locale into account.
        Grouping is applied if the third parameter is true.
    
    getdefaultlocale(envvars=('LC_ALL', 'LC_CTYPE', 'LANG', 'LANGUAGE'))
        Tries to determine the default locale settings and returns
        them as tuple (language code, encoding).
        
        According to POSIX, a program which has not called
        setlocale(LC_ALL, "") runs using the portable 'C' locale.
        Calling setlocale(LC_ALL, "") lets it use the default locale as
        defined by the LANG variable. Since we don't want to interfere
        with the current locale setting we thus emulate the behavior
        in the way described above.
        
        To maintain compatibility with other platforms, not only the
        LANG variable is tested, but a list of variables given as
        envvars parameter. The first found to be defined will be
        used. envvars defaults to the search path used in GNU gettext;
        it must always contain the variable name 'LANG'.
        
        Except for the code 'C', the language code corresponds to RFC
        1766.  code and encoding can be None in case the values cannot
        be determined.
    
    getlocale(category=2)
        Returns the current setting for the given locale category as
        tuple (language code, encoding).
        
        category may be one of the LC_* value except LC_ALL. It
        defaults to LC_CTYPE.
        
        Except for the code 'C', the language code corresponds to RFC
        1766.  code and encoding can be None in case the values cannot
        be determined.
    
    getpreferredencoding(do_setlocale=True)
        Return the charset that the user is likely using,
        according to the system configuration.
    
    localeconv(...)
        () -> dict. Returns numeric and monetary locale-specific parameters.
    
    normalize(localename)
        Returns a normalized locale code for the given locale
        name.
        
        The returned locale code is formatted for use with
        setlocale().
        
        If normalization fails, the original name is returned
        unchanged.
        
        If the given encoding is not known, the function defaults to
        the default encoding for the locale code just like setlocale()
        does.
    
    resetlocale(category=0)
        Sets the locale for category to the default setting.
        
        The default setting is determined by calling
        getdefaultlocale(). category defaults to LC_ALL.
    
    setlocale(category, locale=None)
        Set the locale for the given category.  The locale can be
        a string, an iterable of two strings (language code and encoding),
        or None.
        
        Iterables are converted to strings using the locale aliasing
        engine.  Locale strings are passed directly to the C lib.
        
        category may be given as one of the LC_* values.
    
    str(val)
        Convert float to integer, taking the locale into account.
    
    strcoll(...)
        string,string -> int. Compares two strings according to the locale.
    
    strxfrm(...)
        strxfrm(string) -> string.
        
        Return a string that can be used as a key for locale-aware comparisons.

DATA
    CHAR_MAX = 127
    LC_ALL = 0
    LC_COLLATE = 1
    LC_CTYPE = 2
    LC_MESSAGES = 6
    LC_MONETARY = 3
    LC_NUMERIC = 4
    LC_TIME = 5
    __all__ = ['getlocale', 'getdefaultlocale', 'getpreferredencoding', 'E...

FILE
    /var/containers/Bundle/Application/5C902451-6EA2-4916-AD43-8308C078FB6C/Pythonista3.app/Frameworks/Py3Kit.framework/pylib/locale.py


"""
