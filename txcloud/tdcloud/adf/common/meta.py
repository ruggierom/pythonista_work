# -*- coding: utf-8 -*-
"""
Meta Class Definitions
"""

from abc import (
    ABCMeta, 
    abstractclassmethod, 
    abstractmethod,
    abstractproperty, 
    abstractstaticmethod
)

from tdcloud.exc import MethodOverrideForbiddenError
from six import add_metaclass

class Access(type):
    """
    Metaclass to restrict access to method overrides in child classes.
    
    This is non-pythonic, however this is implemented to ensure a quality package. 
    """

    __SENTINEL = object()

    def __new__(mcs, name, bases, class_dict):
        private = {
            key
            for base in bases for key, value in vars(base).items()
            if callable(value) and mcs.__is_final(value)
        }
        if any(key in private for key in class_dict):
            raise MethodOverrideForbiddenError('Method Override Forbidden: This method may notmbe overrdden.')
        return super().__new__(mcs, name, bases, class_dict)

    @classmethod
    def __is_final(mcs, method):
        try:
            return method.__final is mcs.__SENTINEL
        except AttributeError:
            return False

    @classmethod
    def final(mcs, method):
        method.__final = mcs.__SENTINEL
        return method



__all__ = [
    'Access',
    'abstractstaticmethod',
    'abstractclassmethod',
    'abstractmethod',
    'abstractproperty',
    'add_metaclass',
]
