# -*- coding: utf-8 -*-

import ui
import console

console.clear()


class CheckboxImage (object):
    
    def __init__(self, width=25, height=25, corner_radius=4, line_width=2.5, padding=1):
        
        box_width = (width - (line_width / 2) - padding)
        box_height = (height - (line_width / 2) - padding)
        
        
        ## setup for object
        
        self.corner_radius = corner_radius
        self.box_width = box_width
        self.box_height = box_height
        self.padding = padding
        self.line_width = line_width
        
        self.stroke_color='black'
        
        
        with ui.ImageContext(width, height) as ctx:
            
            checkbox_box = self.add_checkbox_border()
            checkmark_cross = self.add_checkmark_plus()
            
            self.image = ctx.get_image()
            self.image.show()
    
    def add_checkmark_check(self):
        with ui.GState():
            check = ui.Path(
                self.padding, 
                self.padding, 
                self.box_width, 
                self.box_height
            )
            pass
    
    def add_checkmark_plus(self):
        
        with ui.GState():
            dash = ui.Path(
                self.padding, 
                self.padding, 
                self.box_width, 
                self.box_height
            )
            
            start_point = ((self.padding * 3) + self.line_width)
            
            dash_height = ((self.box_height / 2) + self.padding)
            
            dash.line_width = (self.line_width * 1.5)
            dash.line_cap_style = ui.LINE_CAP_ROUND
            ui.set_color(self.stroke_color)
            
            dash.move_to(
                (start_point + self.padding),
                dash_height,
            )
            
            dash.line_to(
                (self.box_width - start_point + self.padding),
                dash_height
            )
            
            dash.move_to(
                dash_height,
                (start_point + self.padding),
            )
            
            dash.line_to(
                dash_height,
                (self.box_height - start_point + self.padding)
            )
            
            dash.stroke()
    
    def add_checkmark_dash(self):
        with ui.GState():
            dash = ui.Path(
                self.padding, 
                self.padding, 
                self.box_width, 
                self.box_height
            )
            
            start_point = ((self.padding * 3) + self.line_width)
            
            dash_height = ((self.box_height / 2) + self.padding)
            dash.move_to(
                (start_point + self.padding),
                dash_height,
            )
            dash.line_width = (self.line_width * 1.5)
            dash.line_cap_style = ui.LINE_CAP_ROUND
            ui.set_color(self.stroke_color)
            
            dash.line_to(
                (self.box_width - start_point + self.padding),
                dash_height
            )
            
            dash.stroke()
            
            return dash
            
        
    
    def add_checkmark_cross(self):
        # Draw the X
        
        start_point = ((self.padding * 3) + self.line_width)
        
        
        with ui.GState():
            cross = ui.Path(
                self.padding, 
                self.padding, 
                self.box_width, 
                self.box_height
            )
            
            
        
            cross.line_width = (self.line_width * 1.5)
            cross.line_cap_style = ui.LINE_CAP_ROUND
            ui.set_color(self.stroke_color)
            
            
            cross.move_to(
                start_point, 
                start_point 
            )
            
            
            
            cross.line_to(
                self.box_width - (start_point /2) - self.padding, 
                self.box_height - (start_point /2) - self.padding,
            )
            
            
            cross.move_to(
                (start_point), 
                self.box_height - (start_point / 2) - self.padding
            )
            
            cross.line_to(
                self.box_width-(start_point / 2) - self.padding, 
                (start_point)
            )
            
            
            cross.stroke()
        
        return cross
    
    def add_checkbox_border(self):
        
        box_method = ui.Path.rect
        checkbox_args = [
            self.padding,
            self.padding,
            self.box_width,
            self.box_height,
        ]
        
        if self.corner_radius > 0:
            box_method = ui.Path.rounded_rect
            checkbox_args.append(self.corner_radius)
            
        checkbox_box = box_method(
            *checkbox_args,
        )
        
        checkbox_box.line_join_style=ui.LINE_JOIN_BEVEL
        checkbox_box.line_width=self.line_width
        ui.set_color('black')
        checkbox_box.stroke()
        
        return checkbox_box
    
            

cbi = CheckboxImage()

