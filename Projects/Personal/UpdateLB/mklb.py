import nginx

c = nginx.Conf()
u = nginx.Upstream('wiki',
                   nginx.Comment('Setup least_conn for this partikular tool')
                   nginx.Key('least_conn',''),
                   nginx.Comment('Server 1')
                   nginx.Key('server','10.1.10.1'),
                   nginx.Comment('Server 2')
                   nginx.Key('server','10.1.10.2'),
                   nginx.Comment('Server 3')
                   nginx.Key('server','10.1.10.3'),
                   )
c.add(u)
s = nginx.Server()
s.add(
    nginx.Key('listen','80'),
    nginx.Location('/',nginx.Key('proxy_pass','http://wiki'))
)
c.add(s)

print(nginx.dumps(c))
