from pytube import YouTube
import ui

"""Register a download progress callback function post initialization.
:param callable func:
    A callback function that takes ``stream``, ``chunk``,
    ``file_handle``, ``bytes_remaining`` as parameters.
:rtype: None
"""

class ProgressBar(ui.View):
    
    def __init__(self, position=0, total=100, frame=(0,0, 250, 30)):
        self._position = position
        self.total = total
        self.frame = frame
        self.background_color = 'white'
        self.border_color = 'black'
        self.border_width = 0.5
        self._label = None
        
        self._progressbar = ui.View(
            frame=self.frame,
            background_color = 'blue',
            width = 0,
            border_color = '#CCC',
            border_width = 1
        )
        
        self.label_properties = dict(
            flex = 'WH',
            alignment=ui.ALIGN_CENTER,
            frame = self.frame
        )
        self.label = '0 %'
        
        self.add_subview(self._progressbar)
        self.add_subview(self._label)
        
    @property
    def label(self):
        return self._label
    
    @label.setter
    def label(self, value):
        assert isinstance(value, str), 'Use string value only'
        
        self.label_properties['text'] = value
        self._label = ui.Label(**self.label_properties)
    
    @property
    def position(self):
        return self._position
    
    @position.setter
    def position(self, value):
        if value > self.total:
            value = self.total
            
        self._position = value
        self._progressbar.width = (
            self.percent_of(
                self.percent_is(value, self.total),
                self.width
            )
        )
        self._label.text = '{} %'.format(
            int(self.percent_is(value, self.total))
        )
        
    
    @staticmethod
    def percent_is(part, whole):
        return 100.0 * float(part)/float(whole)
    
    @staticmethod
    def percent_of(percent, whole):
        return (percent * whole) / 100.0
    
    def set_percent(self, percent):
        self.position = self.percent_of(percent, self.total)
        
    def next(self):
        self.position += 1
    
    def draw(self):
        pass


if __name__ == '__main__':
    """
    v = ui.View()
    v.background_color = 'white'
    x = ProgressBar()
    x.position = 90
    x.flex = ''
    x.frame = (80, 100, 250,30)
    v.add_subview(x)
    v.present()
    import time
    for i in range(11):
        ui.delay(x.next, i)
    
    """
    
    
    v = ui.load_view()
    v.present('sheet')
