# -*- coding: utf-8 -*-

import ui
from photopicker2 import AssetCollectionView

def pick_asset():
    av = AssetCollectionView(frame=(0, 0, 540, 576))
    av.name = 'All Photos'
    
    terp = ui.View(
        frame=(0,0,320,480),
        flex='WH',
        background_color='#fff'
    )
    
    av.present('sheet')
    #terp.add_subview(av)
    #terp.present('sheet')
    #terp.wait_modal()
    if hasattr(av.data_source, 'selected_asset_index'):
        asset = av.data_source.assets[av.data_source.selected_asset_index]
        return asset
    else:
        return None

pick_asset()
