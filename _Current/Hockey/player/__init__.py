# -*- coding: utf-8 -*-
from abc import (
    ABCMeta, 
    abstractclassmethod, 
    abstractmethod, 
    abstractproperty,
)

from six import add_metaclass

class PlayerMeta (ABCMeta):
    pass

class PlayerBase (object):
    pass


@add_metaclass(PlayerMeta)
class Player (PlayerBase):
    pass


class hasblad (object):
    
    def __init__(self):
        pass
    
    def __getattr__(s,b,d,x):
        pass


"""
Setup Base Class
Setup Meta Class
Main Class Implements Base Class
    Implent custom import, via __new__() 
        - Call Entrypoint?
        Df = DeploymentFactory()
        Df.scaleset.deploy.Deploy().run()
        
"""



if __name__ == '__main__':
  """
  Execute if run directly
  """
  
