# -*- coding: utf-8 -*-

import ui
import console

class Main ():
	
	def __init__(self):
		self.template_list = ui.TableView()
		self.main = ui.NavigationView(self.template_list)
		self.main.name = 'Templates'
		
		_buttons = [
			self.make_title_button(self.add_template, 'iob:plus_round_32'),
			self.make_title_button(self.configure, 'iob:gear_a_32'),
		]
		
		self.main.right_button_items = _buttons
		
	
	def make_title_button(self, action, image):
		return ui.ButtonItem(
			action=action,
			image=ui.Image.named(image),
		)
		
	def configure(self, sender):
		pass
	
	def add_template(self, sender):
		pass
	
	def select_template(self, sender):
		pass
	
	def edit_template(self, sender):
		pass
	
	def preview_template(self, sender):
		pass
	
	
	def present(self, style=''):
		self.main.present()
	

if __name__ == '__main__':
	Main().present()
