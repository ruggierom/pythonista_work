# https://forum.omz-software.com/topic/4328/modules-of-pythonista-displayed-with-help

# View help text for all the importable modules
# using StringIO
# Edited based on suggestions by @shtek & @Phuket2
# coding: utf-8

import sys, ui, pkgutil

from io import StringIO
## c mod
import inspect, os
from importlib.machinery import ExtensionFileLoader, EXTENSION_SUFFIXES
from types import ModuleType

def is_c_extension(module: ModuleType) -> bool:
    '''
    `True` only if the passed module is a C extension implemented as a
    dynamically linked shared library specific to the current platform.

    Parameters
    ----------
    module : ModuleType
        Previously imported module object to be tested.

    Returns
    ----------
    bool
        `True` only if this module is a C extension.
    '''
    #assert isinstance(module, ModuleType), '"{}" not a module.'.format(module)
    if not isinstance(module, ModuleType):
        return False

    # If this module was loaded by a PEP 302-compliant CPython-specific loader
    # loading only C extensions, this module is a C extension.
    if isinstance(getattr(module, '__loader__', None), ExtensionFileLoader):
        return True

    # Else, fallback to filetype matching heuristics.
    #
    # Absolute path of the file defining this module.
    module_filename = inspect.getfile(module)

    # "."-prefixed filetype of this path if any or the empty string otherwise.
    module_filetype = os.path.splitext(module_filename)[1]

    # This module is only a C extension if this path's filetype is that of a
    # C extension specific to the current platform.
    return module_filetype in EXTENSION_SUFFIXES

## c mod

w, h = ui.get_screen_size()
fontsize = 15
if w > 767:
    fontsize = 24
if w > 1500:
    fontsize = 36

import importlib
cpackages = []
def check_for_c_module(mod):
    # https://stackoverflow.com/questions/20339053/in-python-how-can-one-tell-if-a-module-comes-from-a-c-extension
    return
    try:
        m = importlib.import_module(mod)
        if is_c_extension(m):
            cpackages.append(mod)
    except:
        pass
    pass

modulelist = []

for pkg in pkgutil.iter_modules():
    modulelist.append(pkg[1])
    check_for_c_module(pkg[1])



def load_action(sender):
    ttval = (ttableview1.data_source.items[sender.selected_row])
    # redirecting help output to string
    revertstatus = sys.stdout
    my_stdout = sys.stdout = StringIO()
    # help() output redirected to my_stdout
    help(ttval)
    sys.stdout = revertstatus  # default

    # loading txtview
    helptext = my_stdout.getvalue()
    if ttval in cpackages:
        helptext = 'cmod\n\n' + helptext
    ttextview1.text = helptext
    ttextview1.editable = False
    my_stdout.close()


ttextview1 = ui.TextView(name='ttextview1', frame=(w * .3, 0, w * .7, h * .9), flex='WH', text='Click Any Module On Left', border_width=1, border_color=0, font=('<system>', fontsize), bg_color = 'white', text_color = 'red')

ttableview1 = ui.TableView(name='ttableview1', frame=(0, 0, w * .3, h * .9), flex='HR', border_width=1, border_color=0, row_height=h / 20, seperator_color = 'red', alpha = .8)

list_source = ui.ListDataSource(sorted(modulelist))
list_source.font = ('Avenir Next Condensed', fontsize)
list_source.text_color = 'red'
list_source.highlight_color = '#eee'
ttableview1.data_source = ttableview1.delegate = list_source
ttableview1.data_source.action = load_action

vname = str(len(modulelist)) + ' Modules'
view = ui.View(name=vname, bg_color = 'lightgrey', frame=(0, 0, w, h * .9))

view.add_subview(ttableview1)
view.add_subview(ttextview1)
#print(cpackages)
with open('all_c_mods.txt','w') as f:
    f.write(str(cpackages))
view.present(title_bar_color = '#ccc')
