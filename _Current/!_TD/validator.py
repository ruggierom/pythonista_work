# -*- coding: utf-8 -*-

import math
from deepdiff import 


class Comparisons:

    @staticmethod
    def counter_cosine_similarity(c1, c2):
        terms = set(c1).union(c2)
        dotprod = sum(c1.get(k, 0) * c2.get(k, 0) for k in terms)
        magA = math.sqrt(sum(c1.get(k, 0)**2 for k in terms))
        magB = math.sqrt(sum(c2.get(k, 0)**2 for k in terms))
        return dotprod / (magA * magB)
    
    @staticmethod
    def length_similarity(c1, c2):
        lenc1 = sum(c1.itervalues())
        lenc2 = sum(c2.itervalues())
        return min(lenc1, lenc2) / float(max(lenc1, lenc2))
    
    @staticmethod
    def similarity_score(l1, l2):
        c1, c2 = Counter(l1), Counter(l2)
        return length_similarity(c1, c2) * counter_cosine_similarity(c1, c2)  


class Specfile:
    """
    Specfile Actions
    """
    
    def __init__(self):
        
        pass
        
    def compare(self):
        pass
    
    def validate(self):
        pass
        
    def import(self, repo_url):
        pass
