from github3 import authorize, login, GitHubError
from getpass import getuser, getpass
from datetime import datetime, timedelta
import os, sys, json, platform, appex

try:
    import dialogs, keychain, ui, console, clipboard,requests
    pythonista = True
except ImportError:
    pythonista = False   
    
APPNAME = 'GistManager'

def make_button_item(action, image_name):
    return ui.ButtonItem(action=action, image=ui.Image.named(image_name))

def make_button_text(action, btn_text):
    return ui.ButtonItem(action=action,
    title=btn_text)

def creation_date(path_to_file):
    """
    Taken from: https://stackoverflow.com/questions/237079
    
    Try to get the date that a file was created, falling back to when it was
    last modified if that isn't possible.
    See http://stackoverflow.com/a/39501288/1709587 for explanation.
    """
    if platform.system() == 'Windows':
        return os.path.getctime(path_to_file)
    else:
        stat = os.stat(path_to_file)
        try:
            return stat.st_birthtime
        except AttributeError:
            # We're probably on Linux. No easy way to get creation dates here,
            # so we'll settle for when its content was last modified.
            return stat.st_mtime

def cache_result(func):
    def func_wrapper(name):
        file_name='gists_cache.json'
        try:
            cache = json.load(open(file_name, 'r'))
        except (IOError, ValueError):
            cache = []
        
        if len(cache) < 1:
            cache = func(name)
            json.dump(cache, open(file_name, 'w'))
        else:
            dt = creation_date(file_name)
            #print(dt)
        return cache
    return func_wrapper


class GistView():
    
    content = None
    
    def get_webview(self, filename, code):
        from pygments import highlight
        from pygments.lexers import PythonLexer
        from pygments.formatters import HtmlFormatter
        
        webcode = highlight(code, PythonLexer(),
                         HtmlFormatter(full=True, prestyles='line-height: 125%; white-space: pre-wrap;'))
        web = ui.WebView()
        web.load_html(webcode)
        web.name = filename
        web.scales_page_to_fit = False
        web.right_button_items = [make_button_text(self.sendToClipboard, 'Copy')]
        self.content = code
        c1 = web.right_button_items
        #print(dir(web))
        
        return web
        
    def get_listview(self):
        indicator = ui.ActivityIndicator()
        indicator.start()
        root = ui.TableView(hide_title_bar=False)
        root.name='Gists'
        root.data_source = GistTableViewDataSource()
        root.delegate = GistTableViewDelegate()
        nav_view = ui.NavigationView(root, hide_title_bar=True)
        #nav_view.push_view(root)
        nav_view.name = 'Gist Manager'
        nav_view.present()
    
    def sendToClipboard(self, sender):
        clipboard.set(self.content)
        GistManager().notify('Copied')

class GistTableViewDelegate (object):
    
    def tableview_did_select(self, tableview, section, row):
        # Called when a row was selected.
        gist = GistManager().gists_get_single(row)
        #dialogs.text_dialog('test', str(gist))
        
        out = []
        files = gist['files']
        for d,f in files.items():
            c = requests.get(f['raw_url']).content
            x = {
                'filename' : f['filename'],
                'language' : f['language'],
                'content' : c
            }
            out.append(x)
        
        #print(out)
        t = out[0]['filename']
        c = out[0]['content']
        v = GistView().get_webview(t,c)
        v.present()
        pass

    def tableview_did_deselect(self, tableview, section, row):
        # Called when a row was de-selected (in multiple selection mode).
        pass

    def tableview_title_for_delete_button(self, tableview, section, row):
        # Return the title for the 'swipe-to-***' button.
        return 'Delete'

class GistTableViewDataSource (object):
    def tableview_number_of_sections(self, tableview):
        # Return the number of sections (defaults to 1)
        return 1    

    def tableview_number_of_rows(self, tableview, section):
        # Return the number of rows in the section
        #return 0
        #GistManager().github_login()
        return GistManager().gists_count()

    def tableview_cell_for_row(self, tableview, section, row):
        # Create and return a cell for the given section/row
        gists = GistManager().gists_get_dict()
        gist = gists[row]
        del gists
        #files = gist['files']
        #print(gist)
        description = gist['description']
        cell = ui.TableViewCell('subtitle')
        cell.text_label.text = description
        cell.text_label.size_to_fit = True
        cell.text_label.number_of_lines = 3
        cell.text_label.line_break_mode = ui.LB_WORD_WRAP
        cell.accessory_type = 'detail_disclosure_button'
        
        return cell

    def tableview_title_for_header(self, tableview, section):
        # Return a title for the given section.
        # If this is not implemented, no section headers will be shown.
        return ''

    def tableview_can_delete(self, tableview, section, row):
        # Return True if the user should be able to delete the given row.
        return True

    def tableview_can_move(self, tableview, section, row):
        # Return True if a reordering control should be shown for the given row (in editing mode).
        return False

    def tableview_delete(self, tableview, section, row):
        # Called when the user confirms deletion of the given row.
        pass

    def tableview_move_row(self, tableview, from_section, from_row, to_section, to_row):
        # Called when the user moves a row with the reordering control (in editing mode).
        pass

class GistManager (object):
    gh = None
    auth = None
    token = None
    appid = None
    
    def __init__(self):
        if pythonista: console.clear()
        pass
    
    def token_get(self):
        token = None
        id = None
        
        if pythonista:
            try:
                token = keychain.get_password(APPNAME, 'token')
                id = keychain.get_password(APPNAME, 'id')
            except Exception as e:
                print('Token Get Error')
                print(e)
        else:
            #if os.path.exists()
            # TODO: non ista
            pass
        
        return tuple((token, id))
    
    def token_set(self, token, id):
        
        if pythonista:
            try:
                keychain.set_password(APPNAME, 'token', token)
                keychain.set_password(APPNAME, 'id', str(id))
            except Exception as e:
                print('Token Set error')
                print(e)
        
        return self.token_get()
        
    def get_credentials(self):
        username = None
        password = None
        
        t = {
            'title' : 'Login Required',
            'message' : 'Enter GitHub Credentials',
        }

        if pythonista:
            try:
                username, password = dialogs.login_alert(**t)
            except KeyboardInterrupt:
                #dialogs.alert('Login Required')
                return None
            except Exception as e:
                print('Error: {}'.format(e))
                sys.exit(1)
            
            if len(username) < 1:
                username = None
            
            if len(password) < 1:
                password = None
            
        else:
            print('GitHub Login required.')

            username = getuser('GitHub Username: ')
            password = getpass('GitHub Password: ')
            
        if username is None or password is None:
                print('User Cancelled')
                sys.exit(0)
        
        return tuple((username, password))
    
    def notify(self,title,message):
        if pythonista: 
            dialogs.alert(title, message, "OK", hide_cancel_button=True)
        else:
            print('{}: {}'.format(title, message))
    
    def github_login(self):
        
        token, id = self.token_get()
        
        if token is None or id is None:
        
            scopes = ['repo:status', 'gist']
            note = APPNAME
            note_url = 'https://iguardfile.com'
            
            done = False
            attempts = 0
            
            while not done:
                try:
                    user, password = self.get_credentials()
                    
                    auth = authorize(user, password, scopes, note, note_url)
                    done = True
                except GitHubError as error:
                    print(error.message)
                    print(error.errors)
                
                except KeyboardInterrupt:
                    sys.exit(1)
                    
                except TypeError as e:
                    print(dir(e))
                    sys.exit(1)
                    
                except Exception as e:
                    if e.code != 401:
                        print(e)
                        print(e.code)
                        sys.exit(1)
                        
                    attempts += 1
                    
                if attempts >= 3:
                    done = True
                    self.notify('Error', 'Maximum Login Attempts exceeded.')
                    sys.exit(1)     
            
            try:
                token = auth.token
                id = auth.id
            except Exception as e:
                print(sys.exit(1))
            
            idtoken = self.token_set(token,id)
            if idtoken == tuple((token, id)):
                return True
            
            return False
            
        self.gh = login(token=token)
        #self.auth = self.gh.authorization(id)
        self.token = token
        self.appid = id
        return True
        
    def gists_get(self):
        import pickle
        from datetime import datetime

        # Open file
        try:
            g = open('gistsobj.db','rb+')
            c = pickke.load(g)
        except:
            c = {}
        
        try:          
            gists = c['gists']
            save_time = c['save_time']
            elapsed_hours = math.floor(((datetime.now() - save_time).seconds) / 3600)
        except:
            if self.gh is None:
                print('Error: invalid gh login')
                sys.exit(1)
                
            gists = [g for g in self.gh.gists()]
            if 'gists' not in c or 'save_time' not in c:
                c = {'gists' : None, 'save_time' : None}
                
            c['gists'] = gists
            c['save_time'] = datetime.now()
        finally:
            if hasattr(globals(),'g'):
                if hasattr(g,'close'): g.close()
            
        """
        'as_dict', 'as_json', 'comments', 'comments_count', 'comments_url', 'commits', 'commits_url', 'create_comment', 'created_at', 'delete', 'description', 'edit', 'etag', 'files', 'fork', 'forks', 'forks_url', 'from_dict', 'from_json', 'git_pull_url', 'git_push_url', 'history', 'html_url', 'id', 'is_starred', 'last_modified', 'owner', 'public', 'ratelimit_remaining', 'refresh', 'session', 'star', 'truncated', 'unstar', 'updated_at', 'url']
        """
        
        return gists
    
    @cache_result
    def gists_get_dict(self):
        if self.gh is None:
            self.github_login()
        
        gists = self.gists_get()
        output = []
        for gist in gists:
            output.append(gist.as_dict())
        return output
        
    def gists_get_single(self, num):
        d = self.gists_get_dict()
        if num < len(d):
            return d[num]
        else:
            return False
    
    def gists_count(self):
        return len(self.gists_get_dict())
    
    def gists_create(self, filename, content, summary, public=False):
        gh = self.gh
        if gh is None:
            print('Error: invalid gh login')
            sys.exit(1)
            
        files = {
            filename : {
                'content': content
                }
            }
            
        gist = gh.create_gist(summary, files, public=public)
        
        # gist == <Gist [gist-id]>
        print(gist.html_url)
    
    def gists_create_anonymous(self, filename, content, summary):
        files = {
            filename : {
                'content': content
                }
            }
            
        gist = create_gist(summary, files)
        
        print(gist.html_url)
        
    def get_url_share(self):
        urls = appex.get_urls()
        get_urls = []
        if isinstance(urls,str):
            get_urls.append(urls)
        elif isinstance(urls, list):
            get_urls = urls
            
        for url in urls:
            # get contents
            # check url
            
            pass
    
    
    def main(self):
        if not self.github_login():
            self.notify('Unable to authenticate')
            sys.exit(1)
        
        #x = self.gists_get()
        #for g in x:
        #    print(dir(g))
        GistView().get_listview()


if __name__ == '__main__':
    GistManager().main()

