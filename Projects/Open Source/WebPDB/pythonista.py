# -*- coding: utf-8 -*-

class PythonistaRemoteDebugger (object):
  
  def __init__(self, pdb_port=None):
    import rpdb
    if pdb_port is None:
      pdb_port=self.get_open_port()
    
    self.pdb_port = pdb_port
    self.rpdb = rpdb.Rpdb(port=pdb_port)
  
  @staticmethod
  def get_open_port():
        import socket
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(("",0))
        s.listen(1)
        port = s.getsockname()[1]
        s.close()
        return port
  
  def set_trace(self):
    self.rpdb.set_trace()
    
