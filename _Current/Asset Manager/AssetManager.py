# -*- coding: utf-8 -*-
"""
iPhone4_screen_size = (320, 480)
iPhone4s_screen_size = (320, 480)
iPhone5_screen_size = (320, 568)
iPhone5s_screen_size = (320, 568)
iPhone6_screen_size = (375, 667)
iPhone6P_screen_size = (414, 736)
iPhone6s_screen_size = (375, 667)
iPhone6sP_screen_size = (414, 736)
iPad2_screen_size = (768, 1024)
iPad_Mini_1 = (768, 1024)
iPad_Mini_2 = (768, 1024)
iPad_Mini_3 = (768, 1024)
iPad_Air_screen_size = (768, 1024)
iPad_Air_2_screen_size = (768, 1024)
iPad_Pro_screen_size = (1024, 1366)
"""
import ui
import platform
import re

class Device (object):
    name = ''
    series = ''
    full_name = ''
    size = ''
    model = ''
    device_type = ''
    device_version = ''
    resolution = (0, 0)
    default_orientation = 'Landscape'    
    iPad = False
    iPhone = False
    Watch = False
    iPod = False
    
    # Features
    gps = False
    retina = False
    gsm = False
    cdma = False
    wifi = True
        
    # Reusable Regex's
    find_digit = re.compile('\d')
    def get_device(self):
        
        
        ios_version, _, device = platform.mac_ver()
        self.ios = ios_version
        self.device_raw = device
        
        # Detect Device Type
        try:
            found = self.find_digit.search(device)
            self.device_type = str(device)[0:found.start()]
            self.device_version = str(device)[found.start():]
            print(self.device_version)
            try:
                setattr(self, device_type, True)
            except:
                pass
        except Exception as e:
            raise e # for now
        
        # Setup Device Name & Resolutions
        
        ## Watch
        if self.Watch:
            
            # Defaults - Applies to All Models
            self.name = 'Apple Watch'
            self.gps = True
            
            if self.device_version == '1,1':
                self.series = '1st Generation'
                self.size = '38mm'
            elif self.device_version == '1,2':
                self.series = '1st Generation'
                self.size = '42mm'
            elif self.device_version == '2,6':
                self.series = 'Series 1'
                self.size = '38mm'
            elif self.device_version == '2,7':
                self.series = 'Series 1'
                self.size = '42mm'
            elif self.device_version == '2,3':
                self.series = 'Series 2'
                self.size = '38mm'
            elif self.device_version == '2,4':
                self.series = 'Series 2'
                self.size = '42mm'            
            elif self.device_version == '3,1':
                self.series = 'Series 3'
                self.size = '38mm'            
            elif self.device_version == '3,2':
                self.series = 'Series 3'
                self.size = '42mm'
            elif self.device_version == '3,3':
                self.series = 'Series 3 +GSM'
                self.size = '38mm'
                self.gsm = True                          
            elif self.device_version == '3,4':
                self.series = 'Series 3 +GSM'
                self.size = '44mm'
                        
            # Set full name
            self.full_name = '{} {} ({})'.format(self.name, self.size, self.series)
        
            # Setup iPad Definitions:
                iPads = {
                    'iPhone 1,1' : dict(
                        series='1',
                        model='A1203',
                        pysical=dict(
                            height_in=4.5,
                            width_in=2.4,
                            height_mm=115,
                            width_mm=61,
                            display=3.5,
                            size='3.5 Inch',
                            resolution='320x480',
                            width=320,
                            height=480,
                            default_orientation='portrait',
                            processor='APL0098 (412 MHz)',
                            memory='128MB',
                            camera_rear=True,
                            camera_rear_quality='2MP',
                            camrea_rear_mp=2,
                            camrea_front=False,
                            camera_front_quality=None,
                            camera_front_mp=None,
                            battery_mah=1400,
                            battery_capacity='1400 mAh',
                            weight='0.3lbs',
                            dimensions=(4.5, 2.4, 0.46),
                            connector='30-pin',
                            cellular_support='2.5G networks',
                            starting_price_usd='$499',
                            release_date='June 29th, 2007',
                            misc_info='No third-party apps, brought multitouch touchscreen to market',
                        )
                    ),
                    'iPhone 2,1' : dict(
                        
                    )
                }                
                
            
            
        
    
        

Device().get_device()

class AssetManager (object):
    pass
    
class Main(ui.View):
    pass
v = ui.load_view()
v.present('sheet')
