# Installer Tool

This is a tool to setup installers for Pythonista Components.

## How it works

1. Use Wrench Menu (Will default to file/folder in editor)
2. Can optionally select another folder or files
3. Optionally Compress into zip 

# Options

1. SFX Style (Zip embedded into single file)
2. GUI Installer (like InstallShield)
3. Console/Text based
3. Install into current folder
4. Install into system packages
5. Install into py2 or py3 system packages
6. Detect Pythonista Install or Regular Python Install
7. Install from Gitahub (Using Github Downloader code)
8. Config based GUI installer; options like accept license



## TODO

1. Setup file/folder browser/selector
2. Build GUI for ios installer
3. Build GUI for system installer
	* Uses objc_util or other system osx
	* Must include ability to use pip
4. Build SFX Maker
