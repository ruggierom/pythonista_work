"""
test
"""
# -*- coding: utf-8 -*-

from constants import HookType
from typing import Union

class Hook:
    def __init__(self, **kwargs):
        pass


class CommandLineHook (Hook):
    
    def __init__(self, hook_type=None):
        assert any(
            (
                isinstance(hook_type, HookType),
                hook_type is None,
            )
        )
        if not hook_type:
            pass
            
        super().__init__(hook_type=hook_type)
    
    

class blah (object):
    
    
    
    def __init__(self)
