#!python2
from __future__ import print_function
import ui
"""
Skotes - Sketchpad + Notes

Example of Pythonista, as well as a way to prevent the content from being uploaded.

Design Notes - All:
    * HTML Based
    * Notes + Inline Images Allowed
    
Design Notes - iPhone:
    * Slide menu (Left)
    * Menu contains File & Tools
Design Notes - iPad:
    * Tools appear as slide menu OR floating Window
    * When closed floating menu goes to slide
Menu:
    * File:
        * New
        * Open
        * Save
        * Save As
        * Close File
        * Export:
            text + images
            html + images
            PDF File
    * Tools:
        * Rectangle
        * Rounded Rectangle
        * Oval
        * Circle
        * Line
        * Text (in sketch)
        * Pencil (Freeform)
        * Maybe: Marker (Freeform)
        * Maybe: path trace
        * Maybe: Finish something first dick.
        

"""
import os
html = os.path.join('.','html-cloud','index.html')
v = ui.load_view()
wv = v['noteview']
wv.load_url(html)
print(wv)
v.present('sheet', hide_title_bar=True, title_bar_color='#000')
