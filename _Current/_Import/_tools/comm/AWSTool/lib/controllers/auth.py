# -*- coding: utf-8 -*-
"""
Authentication controller
"""
# https://forum.omz-software.com/topic/2156/share-code-touch-id-authentication-in-pythonista
# https://gist.github.com/omz/66a763a9db15dc847690

from objc_util import *
import threading
import keychain

class TouchID (object):
    """
    Use TouchID to validate the device owner, if available.
    
    Example:
        from lib.controllers import auth
        auth.validate
    """
    NSBundle = ObjCClass('NSBundle')
    LocalAuthentication = NSBundle.bundleWithPath_('/System/Library/Frameworks/LocalAuthentication.framework')
    LocalAuthentication.load()
    LAContext = ObjCClass('LAContext')
    
    # authenticate() will raise one of these exceptions when authentication
    # fails. They all derive from AuthFailedException, so you can catch that
    # if you don't care about the failure reason, but you could also handle
    # cancellation differently, for example.
    
    class AuthFailedException (Exception): 
        pass
    class AuthCancelledException (AuthFailedException): 
        pass
    class AuthTimeoutException (AuthFailedException): 
        pass
    class AuthNotAvailableException (AuthFailedException): 
        pass
    class AuthFallbackMechanismSelectedException (AuthFailedException): 
        pass
    
    def __init__(self, reason=None):
        if reason:
            self.reason = reason
        else:
            self.reason = 'Authentication Required'
    
    def is_available(self):
        '''Return True if TouchID authentication is available, False otherwise'''
        context = self.LAContext.new().autorelease()
        return bool(context.canEvaluatePolicy_error_(1, None))
    
    def authenticate(self, reason='', allow_passcode=True, timeout=None):
        '''Authenticate the user via TouchID or passcode. Returns True on success, raises AuthFailedException (or a subclass) otherwise.'''
        if not self.is_available():
            raise AuthNotAvailableException('Touch ID is not available.')
        policy = 2 if allow_passcode else 1
        context = self.LAContext.new().autorelease()
        event = threading.Event()
        result = {}
        def callback(_cmd, success, _error):
            print(_cmd)
            result['success'] = success
            if _error:
                error = ObjCInstance(_error)
                result['error'] = error
            event.set()
        handler = ObjCBlock(callback, restype=None, argtypes=[c_void_p, c_bool, c_void_p])
        context.evaluatePolicy_localizedReason_reply_(policy, reason, handler)
        if not event.wait(timeout):
            #NOTE: invalidate() is a private method (there's apparently no public API to cancel the TouchID dialog)
            context.invalidate()
            raise AuthTimeoutException('Timeout')
        success = result.get('success', False)
        error = result.get('error')
        if success:
            return True
        elif error:
            error_code = error.code()
            if error_code == -2:
                raise self.AuthCancelledException('Cancelled by user')
            elif error_code == -3:
                raise self.AuthFallbackMechanismSelectedException('Fallback authentication mechanism selected')
            else:
                desc = error.localizedDescription() or 'Unknown error'
                raise AuthFailedException(desc)
        else:
            raise self.AuthFailedException('Unknown error')
            
    def request(self, reason, **kwargs):
        attempts = kwargs.pop('attempts',3)
        passcode = kwargs.pop('allow_passcode', True)
        timeout = kwargs.pop('timeout',30)
        
        assert isinstance(attempts, int)
        assert isinstance(passcode, bool)
        assert isinstance(timeout, int)
        assert attempts > 0
        assert attempts < 10
        assert timeout > 10
        assert timeout < 300
        
        for a in range(1,3):
            try:
                auth = self.authenticate(reason, passcode, timeout)
                return True
            except self.AuthFailedException as e:
                pass
            except self.AuthNotAvailableException:
                print('TouchID not abailable')
                pass # lets plan
            except self.AuthCancelledException:
                print('User Cancelled')
                
        return False
                
    def get_keychain_auth(self, service, user, **kwargs):
        
        if self.request(**kwargs):
            return keychain.get_password(
                service, 
                user)
        
if __name__ == '__main__':
    TouchID().request('dem reazons')

"""
    # Demo:
    def demo():
        try: 
            reason = 'We need you fingerprint to ste...ehm... to log you in. You have 10 seconds.'
            authenticate(reason, allow_passcode=True, timeout=10)
            print('Success!')
        except AuthFailedException as e:
            print(e)
"""
