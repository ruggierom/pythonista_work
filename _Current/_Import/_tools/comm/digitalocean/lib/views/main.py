# -*- coding: utf-8 -*-
import ui
import sys
import os
from objc_util import *

# ObjCInstance(nav).navigationController().navigationBar()._backgroundView().backgroundColor=UIColor.redColor()

## dev only
sys.path.insert(0,os.path.join('..'))
## dev only 

from lib.views.home import HomeView
from lib.views.slidemenu import SideMenuSlideView

class AWSMain (object):
    
    nav_view = None
    slide_view = None 
    slide_tv = None
    
    
    def __init__(self, **kwargs):
        
        L=ui.ListDataSource(['Sign In','About'])
        w, h = ui.get_screen_size()
        
        # Setup side menu
        menu_delegate = kwargs.pop('menu_delegate', L)
        
        mt = ui.Label()
        mt.text='Menu'
        mt.font=('System',25)
        mt.height=44
        mt.flex='WH'
        #mt.y = -20
        mt.frame=(0,0,100,44)
        mt.border_color = '#000'
        mt.border_width = 1
        mt.alignment = ui.ALIGN_CENTER
        mt.bring_to_front
        
        master = ui.TableView()
        master.flex = 'WH'
        master.y = 20
        #master.frame = (0,20,100,100)
        master.data_source=L
        master.delegate=L
        master.background_color = '#fff'
        #mt.add_subview(master)
        
        
        self.slide_tv = master
        
        
        # Main view
        home = HomeView()
        home.name = 'AWS Manager'
        detail = ui.NavigationView(home)
        #detail.frame = (0,60,100,100)
        detail.flex = 'H'
        detail.width = w
        detail.name = 'menu'
        detail.border_width = 0.8
        detail.border_color = '#ddd'
        self.nav_view = detail
        
    @on_main_thread
    def test(self):
        master = self.slide_tv
        detail = self.nav_view    
        a = SideMenuSlideView(master, detail, present=True)
        return self
        
    @on_main_thread
    def chg_navbar_colour(self):
        UIC=ObjCClass('UIColor')
        ObjCInstance(self.nav_view)\
          .navigationController().\
          navigationBar().\
          _backgroundView().\
          backgroundColor=\
          UIColor.colorWithRed(0.9, green=0.9, blue=0.9, alpha=1.0)
    pass

## dev ##
if __name__ == '__main__':
    t = AWSMain()
    t.test()
    t.chg_navbar_colour()
    
    
## dev ##
