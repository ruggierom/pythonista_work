import socket

# from: https://stackoverflow.com/questions/3764291/checking-network-connection

def has_internet(host="8.8.8.8", port=53, timeout=3):
    """
    Host: 8.8.8.8 (google-public-dns-a.google.com)
    OpenPort: 53/tcp
    Service: domain (DNS/TCP)
    """
    try:
        socket.setdefaulttimeout(timeout)
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
        return True
    except Exception as ex:
        print ex.message
    return False

def has_localweb(ipv6=False):
    if ipv6:
        return has_internet('::1', 80, 1)
    else:
        return has_internet('127.0.0.1', 80, 1)
