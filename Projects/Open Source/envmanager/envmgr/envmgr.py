# -*- coding: utf-8 -*-
"""
*env Manager

Idea came from: https://github.com/riywo/anyenv

Features:
  * Install / Update / Uninstall *env tools
  all:
    * Uses OS package manager to install dev tools
    * Adds required lines to .bash_profile or .bashrc
    * Attempts to determine OS Distro
    
  jenv:
    * Auto-install openjdk
    * Auto-Install Oracle JDK
      * Forces user to accept (optionally disabled)
  pyenv:
    * Checks for commonly missed dependencies
  
  --
  
  RHEL Variants:
    * Uses yum module if available
  Debian Variants:
    * https://pypi.python.org/pypi/deb-pkg-tools/
  OSX:
    * Attempts to detect homebrew
"""
