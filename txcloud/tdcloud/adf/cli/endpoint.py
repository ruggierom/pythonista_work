# -*- coding: utf-8 -*-
#!/usr/bin/env python3

#from ..common.meta import *
import click
from abc import ABCMeta

#@add_metaclasd(Access)

class CLIMeta(type):
    pass
    

class CLIBase:
    """
    
    """
    
    
    def __init__(self):
        self.__did_super = True
        pass 
         
    @property
    def endpoint(self):
        return self._endpoint
    
    @endpoint.setter
    def endpoint(self, value):
        return self

class Blah:
    pass

class Wank(CLIBase, Blah):
    def __init__(self):
        super().__init__()
    
w = Wank()
