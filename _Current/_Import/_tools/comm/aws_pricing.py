# https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/index.json

# https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/{offer_code}/current/index.{format}

import requests
import sys
import json
from six import text_type

try:
    import yaml
except ImportError:
    yaml=False

from pprint import pformat
from collections import OrderedDict, deque

list_types = (list, deque, tuple)
dict_types = (dict, OrderedDict)
iter_types = tuple(list_types + dict_types)

#print(iter_types)
#sys.exit()

def checkiter(itervar, iterype):
    """
    Plan to extend this
    """
    return isinstance(itervar, itertype)
        

class iterdumps (object):

    @staticmethod
    def dumps(data, indent=None):
        assert isinstance(data, iter_types)
        
        if indent:
            data = pformat(data, indent=indent)
        return data
    
    @staticmethod
    def dump(data, filep, indent=None):
        assert isinstance(data, iter_types)
        
        if indent:
            data = pformat(data, indent=indent)
        if hasattr(filep, write):
            filep.write(data)
        else:
            raise AttributeError('Error - {0} not a valid file pointer'\
            .format(str(filep)))

def getdata(url):
    r = requests.get(url)
    if not r.status_code == 200:
        raise Exception(str(r.content, encoding='utf-8'))
    return r.json()

def load_offers():
    offers_url = 'https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/index.json'
    return getdata(offers_url)['offers']

def get_pricing(offer_code, url_only = True, ret_format='json'):
    #fmt='json'
    #offer = 'AmazonEC2'
    url = 'https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/{offer_code}/current/index.{format}'.format(offer_code=offer_code, format=ret_format)
    if url_only:
        return url
    else:
        return getdata(url)

def savedata(data, outfile, save_format='json'):
    pass
    """
    if save_format in dir(globals()):
        fmt = getattr(globals(),save_format)
        if not fmt:
            fmt = iterdumps
            
        with open(outfile, 'w') as f:
            f.write(fmt.dumps(data))
    """

if __name__ == '__main__':
    allurls = {}
    offers = load_offers()
    for offer in offers:
        print('Getting pricing for {0}'.format(offer))
        #d = get_pricing(offer)
        #allurls.append(get_pricing(offer))
        x = { offer : get_pricing(offer)}
        allurls.update(x)
        print('Saving...')
        fname = '{0}.json'
        #allurls.append()
        #savedata(d,fname)
        
    #print(allurls)
    import sh
    for offer, url in allurls.items():
        output = sh.wget(url,'-o','{}.json'.format(offer))
        print(output)
    """
    with open('pricing_urls.txt', 'w') as f:
        t = '\n'.join(allurls)
        f.write(str(t))
    """
    

