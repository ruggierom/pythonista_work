# -*- coding: utf-8 -*-
"""
Spot Instance Pricing Calculator
"""
import ui
import json
from objc_util import on_main_thread

def _get_data(file):
    with open('data/json/{0}.json'.format(file), 'r') as f:
        return json.load(f)

def _get_offers():
    with open('data/categories.json') as f:
        return json.load(f)['offers']  
    

class SIPCData (object):
    
    def __init__(self):
        # Load Core Data
        offers = _get_offers()
        self.categories = offers.keys()
        
    
    def tableview_number_of_sections(self, tableview):
        # Return the number of sections (defaults to 1)
        return 1

    def tableview_number_of_rows(self, tableview, section):
        # Return the number of rows in the section
        return 0

    def tableview_cell_for_row(self, tableview, section, row):
        # Create and return a cell for the given section/row
        cell = ui.TableViewCell()
        cell.text_label.text = 'Foo Bar'
        return cell

    def tableview_title_for_header(self, tableview, section):
        # Return a title for the given section.
        # If this is not implemented, no section headers will be shown.
        return 'Some Section'

    def tableview_can_delete(self, tableview, section, row):
        # Return True if the user should be able to delete the given row.
        return True

    def tableview_can_move(self, tableview, section, row):
        # Return True if a reordering control should be shown for the given row (in editing mode).
        return True

    def tableview_delete(self, tableview, section, row):
        # Called when the user confirms deletion of the given row.
        pass

    def tableview_move_row(self, tableview, from_section, from_row, to_section, to_row):
        # Called when the user moves a row with the reordering control (in editing mode).
        pass
    
    def tableview_did_select(self, tableview, section, row):
        # Called when a row was selected.
        pass

    def tableview_did_deselect(self, tableview, section, row):
        # Called when a row was de-selected (in multiple selection mode).
        pass

    def tableview_title_for_delete_button(self, tableview, section, row):
        # Return the title for the 'swipe-to-***' button.
        return 'Delete'

