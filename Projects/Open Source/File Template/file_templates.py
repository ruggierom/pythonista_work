# -*- coding: utf-8 -*-

import ui
import console
import dialogs
from lib import uiHelper, CodeEditorView, Config


"""
# --- DEMO

editor_view = None

def copy_action(sender):
        import clipboard
        clipboard.set(editor_view.text)
        import console
        console.hud_alert('Copied')

def main():
        global editor_view
        editor_view = CodeEditorView('python', ext_kb=True, frame=(0, 0, 500, 500))
        editor_view.name = 'Code Editor Demo'
        copy_btn = ui.ButtonItem('Copy', action=copy_action)
        editor_view.right_button_items = [copy_btn]
        editor_view.text = '#coding: utf-8\nprint "Hello World"'
        editor_view.present('sheet')
"""

class Category (ui.ListDataSource):
  """
  Create Data lists for each category, then have a method for switching
  
  """
  def __init__(self):
    # Load Snippets / Gists / Files / Packages
    
    pass

class Main (ui.View):

  def __init__(self):

    
    #self.templates = ui.TableView()
    self.templates = ui.load_view('views/templates')
    self.templates.background_color = '#eee'
    self.templates.name = 'Templates'
    self.templates.flex = 'WH'
    
    w, h = ui.get_window_size()
    

    self.main = ui.NavigationView(self.templates)
    self.main.tint_color = '#333'
    self.main.background_color = '#eee'
    


    right_buttons = [
            self.make_title_button(self.add_template, 'iob:plus_round_32'),
            self.make_title_button(self.configure, 'iob:gear_a_32'),
    ]

    left_buttons = [
            self.make_title_button(self.dismiss, 'iob:close_round_24'),
            self.make_title_button(self.snippet_type, 'iob:ios7_folder_32')
    ]

    self.templates.left_button_items = left_buttons
    self.templates.right_button_items = right_buttons

    # Add

    self.add_view = ui.load_view('views/add')
    #self.add_view.name='add'
    
    self.add_view.background_color = '#eee'

    self.background_color = '#ccc'
    self.flex = 'WH'

    self.add_subview(self.main)

  # extension

  def touch_began(self, touch):
    uiHelper(self.add_view).dismiss_keyboard()
    #self.end_editing()
    pass

  def draw(self):
    _, _, w, h = self.frame
    
    self.main.frame = (10, 20, (w-20), (h-30))
    #self.main.corner_radius = 16
    
  def snippet_type(self, sender):
    pass

  def dismiss(self, sender):
    self.close()

  def make_title_button(self, action, image):
    return ui.ButtonItem(
            action=action,
            image=ui.Image.named(image),
            tint_color='#333',
    )
  
  def category_view(self, sender):
    pass

  def save_snippet(self, sender):
    pass

  def cancel_add(self, sender):
    try:
      dialogs.alert('Unsaved Changes','Changes will be lost if you leave now.','Ok')
      self.main.pop_view()
    except KeyboardInterrupt:
      pass
      

  def configure(self, sender):
    pass

  def paste_code(self, sender):
    pass

  def add_template(self, sender):
    self.main.push_view(self.add_view)
    pass

  def select_template(self, sender):
    pass

  def edit_template(self, sender):
    pass

  def preview_template(self, sender):
    pass


  def run(self):
    self.present(hide_title_bar=True)
    

if __name__ == '__main__':
  Main().run()

