# -*- coding: utf-8 -*-

import ui
from dialogs import _FormContainerView
from base64 import b64decode
from composite import Composite
from gestures import Gestures
from exceptions import DuplicateKeyError

class Checkbox(ui.View):
    checkboxes = {
        'checked': [
            'iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYA',
            'AACqaXHeAAADoUlEQVR42u2bO4hTQRSG4xvf',
            'iA+00EICoiFx8yYGJDaCBATFKGIjVotbLIKN',
            'hbqoWFhYWPjARrFRtFqsRAvBQhDZtRAFQQQx',
            'rroLEcVH3Gz8f9C9IUySewibufckgcMuyZy5',
            '5//m3Jm5c2cC1Wq1oUUikTXxeDwfi8VOJxKJ',
            'YfxfhFU9bkXGypgZOzU002j8Eo7zUMkQ/pZZ',
            'qc+tTBjU5ApAMpnsQ+FROiuz0Wg0urUpABQ6',
            '6rS6SitToxEAW96F+Aqs5HGrtIJQmwnT93yT',
            'tP8IGwSgbehQFrO8l40xMlbGzNgb3Q7UPA2A',
            'HZ6pIL6/nUqlVrKMH42xU0MDbUMs83+oK5vE',
            's4AGawChTO1M/7wp7U0t7+dMaHA75AMcIw0/',
            'DNJRk1FTvU5qZ3oM1//ATkQbAGqq10ntAcP0',
            'tuKH3h5D2RbEehatuDeTySx0MzoYhshiwJD+',
            'Ja+Lh+j9iPNXTcyv0JqpVn7UVq/XbwCYyusN',
            'QmiTAHMuFArN1wyA8T5oMdMbSafTyzQCYKz9',
            'JtGGzu2aNgBM/Y2I7ZvLh57nqgDgMxut+til',
            'eI7vV1UBgPhjgkfeEjtKLQAofhNi+iFo/cNq',
            'RoFCoTAH8Tx1K54zO03zAMZ2QpD640j9tVoA',
            'MK4w7Lcg9Q/omAk6K1Qjgta/Qz9NAM4IxI/x',
            'eV8NAKRyHJ8/go5vN/1UAAgGgwtw/ZeC1r9B',
            'PzUA0JoXBOLfw5brAOCs2FQEAHbSTwMAxrAI',
            '9kYg/gr9NAG4JBD/FoseS+wCcOboDPwU19fb',
            'EL8DNuVS/BTWArfTzyYAit9XN1R9hoWlAWWz',
            '2aXweyeY7V2kny0AZvGOfUGAEWFA1wWp/5qr',
            'vx0BIBYvh8C6dgnET8LS9LMFgAFHXc7Qxp3X',
            'z2YLh8MrUO6DAMB5+tkEwHI3jcHJIRDmLUFd',
            'L7jU7S8AzvN5n2Guv0eyo8NQh/1bQGATtQKQ',
            'Favx3SeB/0mjGLudoBwC4f1r/XuCp7xnuVxu',
            'rkcAtA+Br60E5X+i/GaBOHsToRmy47yOVQAW',
            'ITzhixDrACxB+I7UD7Ju6wBsQID4AdbpCQCd',
            'hoA6HiL1Z3kGQIchfIVtYD0+BNA+BPgdob8/',
            'AbQP4T79dACQQ5iArVMCQA4BZQ6yrA4Acgh3',
            'WcY2gLY2SgpWfsbqrvFoZjZjyjdKdmKrLCGv',
            'gvXDLmOyc4ivwLyyVba3Wbrrt8t3/YGJrj8y',
            '0zs01Ts21zs42Ts62zs83cXH5/8CllkxCviA',
            'RtIAAAAASUVORK5CYII='],

        'unchecked': [
            'iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMA',
            'AACdt4HsAAAAQlBMVEX///8zMzMzMzMyMjIz',
            'MzMuLi4zMzMuLi4zMzM2NjYzMzMzMzM1NTUz',
            'MzM0NDQzMzM0NDQ0NDQxMTEzMzM1NTUzMzM6',
            '7LBrAAAAFXRSTlMA8Pp/SxbmCwUT0G86/bLn',
            'qIAvajBGafPqAAAAwElEQVR4Xu2XSw6DMAxE',
            'h/whaYBC7n/Vyi5IXZJ4V/ltsponR954cOGd',
            'raE9JFTrPH5Ju2mdGJtwg3NpAyxvXBymDWEO',
            'MOedf00Ped0GniF9519ziXhILHnl0JIA7Jzf',
            'ZnQxbxzbAW84j27YYDwczz+jm5l/4WDpyRgg',
            'U9Ki0lMwQKFkRaD9RQwQaZsBpJkwxETZvxSo',
            'QAUqUIEKVKACFahAfGiKT13xsS0+98WFQ1p5',
            'xKVLWvvExVNefeXlW1z/P5ukTB0BVFA1AAAA',
            'AElFTkSuQmCC']
    }

    def __init__(self, text, value=False, key=None, imgsize=26,  **kwargs):
        
        self.height = kwargs.pop('height', 32)
        self.width = kwargs.pop('width', 100)
        self.flex = kwargs.pop('flex', 'WB')
        self.font = kwargs.pop('font', ('<System>', 14))
        
        if not key:
            key = text

        def b64i(data):
            if isinstance(data, list):
                data = ''.join(data)

            d = b64decode(data)
            return ui.Image.from_data(d)

        # Strictly to make it fit in Pythonista
        def c(which):
            return self.checkboxes[which]

        def getopt(opt):
            pass

        unchecked = b64i(c('unchecked'))
        checked = b64i(c('checked'))
        #self.c = c
        self.unchecked = unchecked
        self.checked = checked

        # Checkbox Options

        self.key = key
        self.value = value
        
        state = {True: checked, False: unchecked}
        self.button_image = state[value]
        
        button = ui.Button()
        button.width = imgsize
        button.height = imgsize
        button.value = value
        button.flex = 'RTB'
        button.name = key
        button.tint_color = '#000'
        button.text = text

        
        button.image = self.button_image

        def chgbtn(sender):
            self.value = not self.value
            sender.image = state[self.value]
            if hasattr(sender, 'recognizer'):
                print(dir(sender))

        button.action = chgbtn

        self.add_subview(button)

        if text:
            t = ui.Label()
            t.name = 'text'
            t.text = text
            t.font = self.font
            tw = ui.measure_string(t.text, font=t.font)
            t.flex = 'WRTB'
            bw = (imgsize + tw[0] + 5)
            t.x = imgsize + 5
            t.height = self.height
            
            g = Gestures()
            g.add_tap(t, self.button_press)
            t.image = button.image
            #t.image = Composite(t.image, button.image)
            
            self.add_subview(t)
            self.frame = (5, 5, bw, imgsize)
            
        
        for key, value in kwargs.items():
            try:
                setattr(self, key, value)
            except Exception as e:
                print(e)
    
    def button_press(self, data):
        """
        Simulated button press - to enable tapping the label
        """
        state = {True: self.checked, False: self.unchecked}
        for sv in self.subviews:
            if isinstance(sv, ui.Button):
                self.value = not self.value
                sv.image = state[self.value]
        
        
class CheckboxGroup (ui.View):
    """
    Helper class to create a group of checkboxes.
    
    Sets up a ui.ScrollView container, and within the container you can add as many checkboxes as you wish.
    
    All arguments are optional
    
    Arguments:
        :param (list, None): items: List of items to add
        :param tuple: frame: ui.View frame
        :param (str, tuple): background_color: Background Color
        :param str: name: Name of the checkbox group
        :param :param (int, float): checkbox_width: Width of each checkbox defaults to width of checkbox group*
        :param (int, float): checkbox_height: Height of each checkbox. Default 32
        :param str: checkbox_flex: ui.View flex for checkboxes. Default 'WB'
        :param int: columns: Number of checkbox_columns
        :param dict: checkbox_attr: Custom attributes to set on the checkbox. Overrides all previous values if set.
    
    Notes:
        If columns > 1, the width will be auto set as group width / #columns, the width attribute will be ignored.
    """
    
    def __init__(self,items=None, frame=(0, 0, 100, 100),
                 background_color=None, columns=1,
                 padding=5,
                 name='Checkboxes', **kwargs):
        
        
        self._column_count = 1
        
        self.frame = frame
        self.background_color = background_color
        self.name = name
        self.columns = columns
        self.padding = padding
        
        
        # Get the kwargs for width in case its set so the checkbox width can be set
        self.width = kwargs.pop(
            'checkbox_width',
            self.width
        )
        
        self.checkbox_width = kwargs.pop(
            'checkbox_width',
            self.width
        )
        
        self.checkbox_height = kwargs.pop(
            'checkbox_height', 
            32
        )
            
        self.content_view = ui.ScrollView(
            frame=self.frame,
            flex='WH',
            background_color=self.background_color,
        )
        
        # Use composite to
        self.checkbox_group = Composite(self, self.content_view)
        
        self.items = []
        
        if items:
            for item in items:
                self.add(**item)
        
        for key, value in kwargs.items():
            setattr(self.checkbox_group, key, value)
            
    
    @property
    def values(self):
        """
        Aggregate values and return a dictionary of keys.
        """
        return { cbox.key:cbox.value for cbox in self.items }
     
    def add(self, text=None, key=None, 
            value=False, checkbox=None):
                
        assert any((
            all((text, key,)),
            checkbox
        )), 'text + key, or checkbox object required'
        
        
        if self.columns == 1:
            checkbox_frame = (
                self.padding,
                (len(self.items) * self.checkbox_height),
                self.checkbox_width,
                self.checkbox_height,
            )
            
        else:
            if self._column_count == 1:
                x = self.padding
            else: 
                x = (self.checkbox_width * (self.columns - self._column_count + 1))
                x += (self.padding + 2)
            
            if self._column_count == self.columns:
                self._column_count = 0
            
            y = int(len(self.items) / self.columns) * self.checkbox_height
                
            self._column_count += 1
                
            checkbox_frame = (
                x,
                y,
                self.checkbox_width,
                self.checkbox_height,
            )
                
        
        if checkbox:
            checkbox.frame = checkbox_frame
        else:
            if self.items:
                for item in self.items:
                    if item.key == key:
                        raise DuplicateKeyError('Duplicate Checkbox key {}'.format(key))
            
            checkbox = Checkbox(
                text=text,
                key=key,
                value=value,
                frame=checkbox_frame,
            )
        
        self.items.append(checkbox)
            
        self.content_view.content_size = (
            self.width,
            (len(self.items) * self.checkbox_height) + self.checkbox_height,
        )
        
        self.content_view.add_subview(checkbox)
    
    def draw(self):
        if self.columns > 1:
            self.checkbox_width = ((self.width / self.columns) - self.padding)
        self.reload(self.items)
        #self.reload()
        pass
    
    def reload(self, items=None):
        """
        Reload after making manual changes to the items list or changing columns
        """
        self._column_count = 1
        item_keys = [ cbox.key for cbox in self.items ]
        
        for sv in self.content_view.subviews:
            if not isinstance(sv, Checkbox):
                continue
                
            self.content_view.remove_subview(sv)
        
        if not items:
            items = self.items.copy()
        
        self.items = []
        # iterate copy of items; purging once
        for item in items:
            self.add(checkbox=item)
    
    def remove(self, key):
        self.items = [ i for i in self.items if i.key != key]
        self.reload()
        

if __name__ == '__main__':
    # Example of using checkbox
    
    v = ui.View(
        frame=(0, 0, 320, 480), # default size
        flex='WH',
        background_color='white',
    )
    
    c = Checkbox('terpy', imgsize=20, height=32)
    v.add_subview(c)
    
    group_items = [
        dict(
            text='I Love Python',
            key='lovepy',
            value=False,
        ),
        dict(
            text='I Love Coding',
            key='lovecode',
            value=False,
        ),
        dict(
            text='I Love Pythonista',
            key='lovepythonista',
            value=False,
        ),
        dict(
            text='I love YouTube',
            key='youtubedev',
            value=False,
        ),
        dict(
            text='I Love Pasta',
            key='lovepasta',
            value=False,
        ),
        dict(
            text='I Love Pizza',
            key='lovepizza',
            value=False,
        ),
        dict(
            text='I Love Pizzay',
            key='lovepizzayo',
            value=False,
        ),
    ]
    
    v2 = ui.View(
        frame=(0, 0, 320, 480),
        flex='WH',
        background_color='#ccc',
    )
    
    
    cbox = CheckboxGroup(
        items=group_items,
        background_color='#ffffff',
        checkbox_height=27,
        columns=1,
        frame=(0, 0, 320, 350),
        font=('<System>', 13)
    )
    
    cbox.flex = 'WB'
    
    def getvals(_):
        print(cbox.values)
    
    def deltest(_):
        cbox.remove('lovepizzayo')
    
    v2.right_button_items = [
        ui.ButtonItem(
            title='Values',
            action=getvals,
        ),
        ui.ButtonItem(
            title='RemTst',
            action=deltest,
        )
    ]
    
    
    v2.add_subview(cbox)
    v2.present()
    #v.present()
    
