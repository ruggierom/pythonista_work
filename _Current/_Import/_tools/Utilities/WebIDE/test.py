import sys, console, os, runpy
#from bottle import debug

#debug(False)
# Run on Pythonista
def ide_exec_handle_exc(exc):
    """
    Parse exceptions from run so they don't kill the IDE
    
    :param exc Exception object
    :example
        try:
            makesure_thiswillbreak()
        except AttributeError as e:
            errors = ide_exec_handle_exc(e)
        except Exception as e:
            errors = ide_exec_handle_exc(e)   
    """
    # This needs revamped
    look_for = ['code', 'message']
    out = {}
    out.update({'traceback': sys.exc_info()[2]})
    for item in look_for:
        if hasattr(exc, item): out.update({item: getattr(exc, item)})
    return out

# Import runpy, execute script

def run_script(filename):
    import runpy, sys, os
    if not str(filename).endswith('.py'):
        return 'Error: You can only run python scripts with this.'

    # Set scruipt to be relational from what WebIDE Sends as
    runscript = os.path.expanduser("~/Documents/{}".format(filename))

    try:
        """
        Grabbing return data so it doesn't show up in the Pythonista Console.
        When executing, runpy seems to return object data from the objects that
        you are instantiating within the script you are running.        

        Additionally, forcing some some lines through stdout to allow the developer
        to easily find what they want.
        """
        
        rtdata = runpy.run_path(runscript,run_name='__main__')
        
        #print(remove)
            
        #print('.....')
        
        #print(rtdata.keys())
        
        for ibs in globals():
            #print(ibs)
            #c = rtdata.pop(str(ibs))
            #rtdata.pop(ibs,None)
            #print(rtdata)
            #del rtdata[i]
            pass
        
        import inspect
        sources = {}
        for key in list(rtdata.keys()):
            if key.startswith('__'):
                rtdata.pop(str(key),None)
              
            
        from pprint import pprint
        pprint(rtdata)
        return True
    except Exception as e:
        return ide_exec_handle_exc(e)
     
script = 'Projects/ssh_sync/sync.py'

#run_script(script)
#print(x)
"""
__package__
__file__
__cached__
"""

#from shutil import copy2

