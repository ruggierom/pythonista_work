# -*- coding: utf-8 -*-
import ui

## dev only
import sys, os
sys.path.insert(0,os.path.join('..'))
## dev only 

class HomeView (object):
    home_view = None
    
    def __init__(self, **kwargs):
        default_url = 'https://duckduckgo.com'
        url = kwargs.pop('url', default_url)
        h = ui.WebView()
        h.load_url(url)
        h.scales_page_to_fit = True
        self.home_view = h
    
    def __new__(cls, **kwargs):
        w, h = ui.get_screen_size()
        #print(w)
        default_url = 'https://duckduckgo.com'
        url = kwargs.pop('url', default_url)
        h = ui.WebView()
        h.load_url(url)
        h.scales_page_to_fit = True
        h.width = w
        bi = ui.Image.named('iob:archive_32')
        #bi.name='worky'
        x = [ui.ButtonItem(
            image=bi,action=cls.testing,
            
            )]
        h.right_button_items = x
        cls.home_view = h
        return cls.home_view
        pass
    def testing(sender):
        print(sender)
        for i in dir(sender):
            print(i)
        pass


class HomeWebViewDelegate (object):
    
    def webview_should_start_load(self, webview, url, nav_type):
        return True
        
    def webview_did_start_load(self, webview):
        pass
        
    def webview_did_finish_load(self, webview):
        pass
        
    def webview_did_fail_load(self, webview, error_code, error_msg):
        pass

#x = HomeView()
#print(x)

"""
        detail = ui.WebView()
        detail.load_url('http://www.google.com/')
        detail.flex = 'WH'
        detail.scales_page_to_fit = False
        a = SideMenuSlideView(master, detail)

"""
