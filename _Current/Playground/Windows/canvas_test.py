# -*- coding: utf-8 -*-

import ui
import console
#import inspect
from colors import ColorsIOS, color_dict, OrderedDict
import colorsys

console.clear()

class Convert (object):
    
    @staticmethod
    def rgb2hex(rgba):
        #print(rgba)
        #print(rgba[0:3])
        r, g, b = rgba[0:3]
        r, g, b = (int(r*255), int(g*255), int(b*255))
        hex = "#{:02x}{:02x}{:02x}".format(r,g,b)
        return hex
    
    @staticmethod
    def hex2rgb(hexcode):
        hexcode = str(hexcode).lstrip('#')
        return tuple(int(hexcode[i:i+2], 16) for i in (0, 2 ,4))

    @staticmethod
    def clamp(val, minimum=0, maximum=255):
        if val < minimum:
            return minimum
        if val > maximum:
            return maximum
        return int(val)

    @staticmethod
    def colorscale(hexstr, scalefactor):
        """
        Scales a hex string by ``scalefactor``. Returns scaled hex string.
    
        To darken the color, use a float value between 0 and 1.
        To brighten the color, use a float value greater than 1.
    
        >>> colorscale("#DF3C3C", .5)
        #6F1E1E
        >>> colorscale("#52D24F", 1.6)
        #83FF7E
        >>> colorscale("#4F75D2", 1)
        #4F75D2
        """
    
        hexstr = hexstr.strip('#')
    
        if scalefactor < 0 or len(hexstr) != 6:
            return hexstr
    
        r, g, b = int(hexstr[:2], 16), int(hexstr[2:4], 16), int(hexstr[4:], 16)
    
        r = Convert.clamp(r * scalefactor)
        g = Convert.clamp(g * scalefactor)
        b = Convert.clamp(b * scalefactor)

    
        return "#%02x%02x%02x" % (r, g, b)
        
    def colorscale_alt(hex_color, brightness_offset=1):
        """ takes a color like #87c95f and produces a lighter or darker variant """
        if len(hex_color) != 7:
            raise Exception("Passed %s into color_variant(), needs to be in #87c95f format." % hex_color)
        rgb_hex = [hex_color[x:x+2] for x in [1, 3, 5]]
        new_rgb_int = [int(hex_value, 16) + brightness_offset for hex_value in rgb_hex]
        new_rgb_int = [min([255, max([0, i])]) for i in new_rgb_int] # make sure new values are between 0 and 255
        # hex() produces "0x88", we want just "88"
        return "#" + "".join([hex(i)[2:] for i in new_rgb_int])


class CloseButton (object):
    
    def __init__(self, width, height, **kwargs):
        self.width = width
        self.height = height
        
        for key, value in kwargs.items():
            try:
                setattr(self, key, value)
            except:
                pass
    
    
    def default(self,
                background_color='#eee', 
                border_color='#ccc',
                border_width=1,
                stroke_color='#000'):
                    
        width = self.width
        height = self.height
                    
        with ui.ImageContext(width, height) as ctx:
        
            close_box = ui.Path.rect(0, 0, width-1, height-1)
            if background_color:
                ui.set_color(background_color)
                close_box.fill()
            ui.set_color(border_color)
            close_box.line_join_style = ui.LINE_JOIN_ROUND
            close_box.line_width=border_width
            if border_color:
                close_box.stroke()
            with ui.GState():
                ui.set_shadow('#666', 2, 2, 3)
            
            # Draw the X
            start_point = (width / 5)
            
            cross = ui.Path(0, 0, width-1, height-1)
            #cross.line_width = start_point / 3
            cross.line_width = (width / 5 / 3)
            cross.line_cap_style = ui.LINE_CAP_ROUND
            cross.move_to(start_point, start_point)
            ui.set_color(stroke_color)
            cross.line_to(width-start_point, height-start_point)
            
            cross.move_to(start_point, height-start_point)
            cross.line_to(width-start_point, start_point)
            with ui.GState():
                ui.set_shadow('#999', 1, 1, 5)
                cross.stroke()
        
            img = ctx.get_image()
            return img
            
    def circled(self,background_color='#eeeeee',
              border_color='#cccccc',
              border_width=1,
              circle_color='#000000',
              stroke_color='#eeeeee'):
        
        width = self.width
        height = self.height
        
        with ui.ImageContext(width, height) as ctx:
            close_box = ui.Path.rounded_rect(0, 0, width-1, height-1, (width/10))
            if background_color:
                ui.set_color(background_color)
                close_box.fill()
            ui.set_color(border_color)
            close_box.line_join_style = ui.LINE_JOIN_ROUND
            close_box.line_width=border_width
            if border_color:
                close_box.stroke()
            with ui.GState():
                ui.set_shadow('#666', 1, 1, 3)
            
            # Draw the circle, filled
            
            ui.set_color(circle_color)
            start_point = ((width / 5))
            circle = ui.Path.oval(start_point / 2.5, 
                                  start_point / 2.5,
                                  width - start_point,
                                  height - start_point)
            with ui.GState():
                if circle_color:
                    ui.set_shadow('#999', 1, 1, .5)
                    circle.fill()
            
            # Draw the X
            start_point = (width / 3.3)
            cross = ui.Path(0, 0, width-1, height-1)
            cross.line_width = start_point / 3
            cross.line_cap_style = ui.LINE_CAP_ROUND
            cross.move_to(start_point, start_point)
            ui.set_color(stroke_color)
            cross.line_to(width-start_point, height-start_point)
            
            cross.move_to(start_point, height-start_point)
            cross.line_to(width-start_point, start_point)
            with ui.GState():
                
                highlight_color = Convert.colorscale(
                                      stroke_color,
                                      1.2,
                                  )
                 
                highlight_color = '#eee'
                ui.set_shadow(highlight_color, 1, 2,5)
                cross.stroke()
        
            img = ctx.get_image()
            return img
            
class Highlighter (ui.View):
    """
    Testing - Attempting to detect the right way to auto-determine a drop shadow colour.
    """    
    
    def __init__(self):
        """
        Class Initializer
        """
        self.frame = (0, 0, 320, 480)
        self.background_color = '#fff'
        self.pallets = []
        
        # Defaults
        self.pallet_height = 32
        self.pallet_width = (self.width / 2 - 1)
        
        # Content
        self.content_view = ui.ScrollView(
            frame=self.frame,
            content_size=(self.width, self.height),
            flex='WH'
        )
        
        self.add_subview(self.content_view)
        
        self.color_log = []
    
    
    def mkpallet(self, color, name):
        x, y = 0, 0 # Setup Default
        flex='RB'
        
        num_pallets = len(self.pallets)
        
        """
        y = max(
            (
                self.pallet_height, 
                self.pallet_height * num_pallets,
            )
        )
        """
        
        y = self.pallet_height * num_pallets
        
        text = '{} ({})'.format(
            name,
            color.hex_format()
        )
        text_color = '#000000'
        
        if self.is_dark(color.hex_format()):
            text_color = '#ffffff'
        
        
        
        v = ui.Label(
            frame=(x, 
                   y, 
                   self.pallet_width, 
                   self.pallet_height,
            ),
    
            background_color=color.hex_format(),
            border_width=0.5,
            border_color='#999',
            #text=color.hex_format(),
            text=text,
            text_color=text_color,
            font=('<System>', 14)
        )
        
        hl = Convert.colorscale(color.hex_format(),1.1).upper()
        
        if hl == color.hex_format():
            hl = Convert.colorscale(color.hex_format(),0.9).upper()
        
        if hl == color.hex_format():
            hl = Convert.colorscale_alt(color.hex_format(),-4).upper()
        
        if hl == color.hex_format():
            hl = Convert.colorscale_alt(color.hex_format(),3).upper()
        
        v2 = ui.Label(
            frame=(
                x + self.pallet_width,
                y,
                self.pallet_width,
                self.pallet_height
            ),
            background_color=hl,
            border_width=0.5,
            border_color='#999',
            text=hl,
            text_color=text_color,
        )
        

        self.color_log.append(color.hex_format())
        self.color_log.append(hl)
        
        
        self.content_view.add_subview(v)
        self.content_view.add_subview(v2)
        self.pallets.append(name)
        
        self.content_view.content_size = (self.width,
        self.pallet_height * num_pallets)
    
    def draw(self):
        self.pallet_width = (self.width / 2)
        self.generate_colours()
        
    def generate_colours(self):
        count = 0
        
        data = OrderedDict(
            sorted(
                color_dict.items(), 
                key=lambda t: t[1]
            )
        )
        
        
        
        
        for name, color in color_dict.items():
            self.mkpallet(color, name)
    
    def is_dark(self, hex):
        #rgb = Convert.hex2rgb(hex)
        try:
            r, g, b = Convert.hex2rgb(hex)
            dv = ((r * 299) + (g * 587) + (b * 114)) / 1000
            #print('{}: {}'.format(hex, dv))
            #return dv
            if dv <= 60:
                return True
            else:
                return False
        except:
            pass
        
        #print(rgb)
        #((Red value X 299) + (Green value X 587) + (Blue value X 114)) / 1000

if __name__ == '__main__':
    test = 1

    if test == 1:
        # Test highlighter
        h = Highlighter()
        #h.generate_colours()
        h.present()
        
        
    if test == 0:
        rgba = (0.0, 0.47843137254901963, 1.0, 1.0)
        hexrgba = Convert.rgb2hex(rgba)
        
        i = CloseButton(160, 160).circled(
            #background_color='lightblue',
            #border_color='darkblue',
            #circle_color='darkblue',
            stroke_color=hexrgba,
        )
        
        ic = CloseButton(24, 24).circled(
            background_color=None,
            border_color=None,
            #circle_color='darkblue',
            stroke_color='#fff',
        )
        
        i.show()
        
        iv = CloseButton(32, 32).default(
            background_color=None,
            border_color=None,
            stroke_color=(0.0, 0.47843137254901963, 1.0, 1.0),
        )
        
        """
        v = ui.View()
        
        v.background_color='#fff'
        
        btn = ui.Button(
            background_image=i,
            frame=(100, 100, 32, 32)
        )
        print(btn.tint_color)
        v.add_subview(btn)
        v.present()
        """
