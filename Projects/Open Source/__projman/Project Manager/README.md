# Pythonista Project Manager

Manage multiple Pythonista projects with ease.

This is *not* another project management/PMP software despite its name. It is a utility which will not only help keep the projects more organized, it also contains a series of helper utilities to make development on Pythonista a little more streamlined.

## Features

None yet

## Planned Features

### Wrench Utilities

The following Utilities can be installed into Pythonista's wrench menu to make development a little eaiser.

**Current Directory**

A tool which will display the current working directory of the file that is open in the Pythonista Editor.

This was developed due to the fact that connecting Pythonista to third party Source Control Manager (SCM) apps such as Working Copy or Clone creates challenges for switching to the working directory in Stash (command line add-on).

The developer can easily switch to the current folder with this utility and Stash will automatically be in that folder.

**ui packer**

A utility by omz, the creator of Pythonista. Included because  it is genuinely essential.

It allows the developer to package up a .pyui file and embed it into a single python file.

**Phone Manager**

An excellent utility by [humberry](https://github.com/humberry) which provides a nice GUI for managing files on your iOS Device.

Github URL: [https://github.com/humberry/PhoneManager](https://github.com/humberry/PhoneManager)

Note: Some modifications to humberry's original source have been made. Once fully tested, I will be issuing a Pull Request in order to give humberry the option to benefit from the changes (should they wish to).


**Snippet Tool** 

Inspired by the [https://github.com/Pythonista-Tools/Pythonista-Tools](Pythonista Tools) and [https://github.com/psidnell/Pythonista](Pythonista Snippets) projects.

This tool is designed to accept small to medium sized snippets of Python code.

The snippets may or may not be directly geared towards Pythonista, but it is designed to give the developer quick access to useful code.

Implemented as a Wrench menu tool, it is designed to allow the developer to directly insert the code into their project without having to leave Pythonista.

**pip ui**

This is a utility based in the excellent work of the Stash pip application.

It is for all intensive purposes a Python pip client, however  it has some notable differences.

*Install into project Folder*

This feature allows for:

1. The developer to use different versions of Python modules than what is installed into the Pythonista system.
2. The developer to make changes (if absolutely required) to the modules source without affecting thenentire system
3. The application to have its own full ecosysten, without requiring other Pythonitsa users to manage dependencies

This type of installation is actually considered to be *non-pythonic*, so care should be taken when using this feature.

*Custom installation Methods*

Many Pythonista users know that although the Stash's pip utility is quite excellent, it isn't able to automatically install **all** modules.

When one Pythonista user sorts the manual installation method, they can submit the installation steps and other users can take advantage of the module with ease.

*Notification of compiled modules*

Not all modules are compatible with Pythonista, as some modules are written in c and need to be compiled.

Due to the iOS ecosystem this is not possible, when the developer attempts to install a module which either requires compiling, or has dependency on this type of app the developer is then notified.

Because some apps use compiled modules as an optional *faster* method, the developer has the option to still install this app.

We will be building a compatibility list of modules which can and cannot be used with Pythonista.


