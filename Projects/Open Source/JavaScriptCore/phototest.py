from objc_util import *
import time
import threading
C = ObjCClass


class SpyPhoto (object):
  def __init__(self):
    self.session = C('AVCaptureSession').new().autorelease()
    self.session.sessionPreset = 'AVCaptureSessionPresetPhoto'
    self.device = C('AVCaptureDevice').defaultDeviceWithMediaType_('vide')
    self.device_input = C('AVCaptureDeviceInput').deviceInputWithDevice_error_(self.device, None)
    self.session.addInput_(self.device_input)
    self.image_output = C('AVCaptureStillImageOutput').new().autorelease()
    self.session.addOutput_(self.image_output)
    self.session.startRunning()
    # NOTE: You may need to adjust this to wait for the camera to be ready (use a higher number if you see black photos):
    time.sleep(0.3)

  def snap(self,filename='photo.jpg'):
    
      def handler_func(_block, _buffer, _err):
          buffer = ObjCInstance(_buffer)
          img_data = C('AVCaptureStillImageOutput').jpegStillImageNSDataRepresentation_(buffer)
          img_data.writeToFile_atomically_(filename, True)
          e.set()
          
      video_connection = None
      for connection in self.image_output.connections():
          for port in connection.inputPorts():
              if str(port.mediaType()) == 'vide':
                  video_connection = connection
                  break
          if video_connection:
              break
      e = threading.Event()
      handler = ObjCBlock(handler_func, restype=None, argtypes=[c_void_p, c_void_p, c_void_p])
      retain_global(handler)
      self.image_output.captureStillImageAsynchronouslyFromConnection_completionHandler_(video_connection, handler)
      e.wait()


SpyPhoto().snap('photo.jpg')
import console
#console.quicklook('photo.jpg')
