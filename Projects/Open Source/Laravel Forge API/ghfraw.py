# -*- coding: utf-8 -*-
"""
Get a file from
githib url (raw link)
"""
import requests
from bs4 import BeautifulSoup
import os.path

url = 'https://github.com/peakwinter/python-nginx/blob/master/nginx.py'

filename = os.path.basename(url)

r = requests.get(url)
html = str(r.text).encode('utf8')

soup = BeautifulSoup(html, 'html5lib')
a = soup.find('a', id='raw-url')
link = a['href']

r = requests.get('https://github.com/{}'.format(link))
with open(filename, 'w') as f:
  f.write(r.text)
