# -*- coding: utf-8 -*-

# https://github.com/kripken/emscripten

# https://github.com/pybee/seasnake/blob/master/README.rst

import  ui

w = ui.WebView()
w.load_url('tess.html')
w.present()
x = w.eval_js('''
Tesseract.recognize(myImage)
.then(function(result){
    console.log(result)
})
''')

print(x)
