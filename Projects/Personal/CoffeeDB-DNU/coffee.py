# -*- coding: utf-8 -*-

import os
import ui
import sys
import photos
import console
from PIL import Image
from picker import DropdownListView

# TODO: Hide keyboard

def _warn(msg):
  sys.stderr.write('{0}\n'.format(msg))  

class OCRSpace (object):
  
  def __init__(self):
    self.source_image = None
    pass      
  
  def _prep_image(self, image=None, image_path=None):    
    from base64 import b64encode
    from six import BytesIO

    
    b64_image = None
    if image is not None:
      b64_image = b64encode(image)
    elif image_path is not None:
      try:
        # with open(image, 'rb') as f:
        #   b64_image = b64encode(f.read())
        image = Image.open(image_path)
        im_buffer = BytesIO()
        image.save(im_buffer, format="JPEG", quality=20, optimize=True)
        b64_image = 'data:image/jpeg;base64,{0}'.format(b64encode(im_buffer.getvalue()).decode('utf8'))
        print('successfully set b64_image')
      except Exception as e:
        raise e
        _warn('Unable to encode image.\nException: {0}'.format(e))
        return False
    else:
      raise AttributeError('image or image_path required, param missing or both provided. Invalid.')
    
    if b64_image is not None:
      self.source_image = b64_image
      return True
      
    return False

  def ocr(self, filename):
    import requests
    from mimetypes import guess_type
    try:
      apikey = 'adf48e413588957'
      endpoint = 'https://api.ocr.space/parse/image'

      self._prep_image(image_path=filename)
      
      if self.source_image is None:
        print('no source image')
        return False
      #self.source_image = bytes(self.source_image, 'utf8')
    
        
      payload = dict(
        apikey=apikey,
        language='eng',
        filetype='image/x-jpeg',
        base64image=self.source_image,
      )
      r = requests.post(endpoint, data=payload)
      if r.status_code == 200:
        j = r.json()
        message = j['ParsedResults'][0]['ParsedText']
        return message
    except Exception as e:
      print(e)
      raise e

"""

## Test
console.clear()
m = OCRSpace().ocr('IMG_7658.PNG')
price = 0.00

for item in m.splitlines():
  if item.strip().startswith('$'):
    check_price = item.lstrip('$')
    try:
      if float(check_price) > float(price):
        price = check_price
    except:
      pass

print(price)
"""

class Coffee (object):
  
  def __init__(self):
  
    
    restaurants = [
      {
        'title': 'Starbucks',
        'image' : ui.Image('assets/starbucks.png'),
        'id' : 1,
        
      },
      {
        'title': 'Tim Hortons',
        'image': ui.Image('assets/tim_hortons.png'),
        'id': 2,
      },
      {
        'title': 'McDonalds',
        'image': ui.Image('assets/mcdonalds.png'),
        'id': 2,
      },
      {
        'title': 'A&W',
        'image': ui.Image('assets/a_w.png'),
        'id': 2,
      },
    ]
    
    
    
    
    users = [
      {
        'title': 'Michael',
        'image': ui.Image.named('iob:code_working_32'),
        'user_id' : 1,
      },
      {
        'title': 'Sham',
        'image': ui.Image.named('iob:person_32'),
        'user_id' : 2,
      },
      {
        'title': 'Alex',
        'image': ui.Image.named('iob:person_32'),
        'user_id' : 3,
      },
      {
        'title': 'Srijit',
        'image': ui.Image.named('iob:person_32'),
        'user_id' : 14,
      },
      
    ]
    
    att_users = [
      {
        'title': 'Michael',
        'image': ui.Image.named('iob:beer_32'),
        'user_id' : 1,
      },
      {
        'title': 'Sham',
        'image': ui.Image.named('iob:coffee_32'),
        'user_id' : 2,
      },
      {
        'title': 'Alex',
        'image': ui.Image.named('iob:female_32'),
        'user_id' : 3,
      },
      {
        'title': 'Srijit',
        'image': ui.Image.named('iob:ios7_cloud_upload_32'),
        'user_id' : 14,
      },
      
    ]
    
    
    rest_dd = DropdownListView(restaurants, flex='W')
    buyer_dd = DropdownListView(users, flex='W')
    att_dd = DropdownListView(
      att_users, flex='W',
    )
    
    v = ui.load_view('views/Coffee')
    v.background_color ='#888'
  
    # Data Tables
    
    self.current_attendees = []
    
    att_ds = ui.ListDataSource(self.current_attendees)
    
    # Subviews
    
    inside = v['inside']
    
    rest_dd.frame = inside['restaurant_dd'].frame
    inside.add_subview(rest_dd)
    
    buyer_dd.frame = inside['buyer_dd'].frame
    inside.add_subview(buyer_dd)
    
    att_dd.frame = inside['attendee_dd'].frame
    att_dd.action=self.add_attendee
    inside.add_subview(att_dd)
    
    
    att_list = inside['attendee_list']
    att_list.data_source = att_ds
    att_list.delegate = att_ds
    
    self.attendee_ds = att_ds
    self.attendee_dd = att_dd
    
    v.present('fullscreen')
    
  def add_attendee(self, sender):
    current = [i['user_id'] for i in self.attendee_ds.items]
    if sender['user_id'] not in current:
      self.attendee_ds.items.append(sender)
      self.attendee_ds.reload()
    pass
  
  def attendee_delete(self, sender):
    pass
    
  def import_receipt(self, sender):
    receipt = photos.pick_asset(title='Select Receipt')
    print(receipt)
    pass
  
  def save_purchase(self, sender):
    pass
  
  def clear_form(self, sender):
    pass
    
Coffee()
