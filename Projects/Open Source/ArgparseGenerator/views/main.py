import ui

"""
"Paper Icons" by Sam Hewitt is licensed under CC-SA-4.0
http://snwh.org/paper/icons
https://samuelhewitt.com
http://creativecommons.org/licenses/by-sa/4.0/
"""


np = ui.load_view('new_project')
v = ui.load_view()
v.right_button_items = [
  ui.ButtonItem(
    image=ui.Image.named('typb:Plus'),
  ),
  ui.ButtonItem(
    image=ui.Image.named('typb:Cog'),
  )
]
v.present('sheet')
