# -*- coding: utf-8 -*-
#!python2
"""
Compression Utility

Features:
  * Zip 
  * Tar/Gzip
  * Unknown what else
  * just trying it now, it seems pretty good.
"""

import ui
import photos

from tabs import uiTabbar

class Tabs (uiTabbar):
  
  def __init__(self, root):
    
    # photos - http://www.free-icons-download.net/a-stack-of-photos-icon-27849/
    
    # Audio - https://www.flaticon.com/free-icon/music-album_26805
    
    # https://www.flaticon.com/authors/freepik
    
    tabs = [
      self.make_tab('iob:filing_32', root),
      self.make_tab('iob:ios7_photos_32')
    ]
    pass
  
  def make_tab(self, button_image, target_view):
    b = ui.Button(
      image=ui.Image.named(button_image),
      action=self.switch_view,
    )
    b.target_view = target_view
    
    
    pass
    
  def photos_import(self):
    photos.pick_asset()
  
  def switch_view(self, sender):
    pass
    
    

v = ui.load_view('views/main')
v.background_color = '#ccc'
v.present()
