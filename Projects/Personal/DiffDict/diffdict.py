# -*- coding: utf-8 -*-
from deepdiff import DeepDiff
import deepdiff.helper
from ast import literal_eval
import json
import re

d1 = dict(
  a='a',
  b='b',
  c='7',
  d='d',
  e='e'
)

d2 = dict(
  a='a',
  b='b',
  c='c',
)

d3 = dict(
  a='a',
  b='b',
  c=7,
  d='a',
)


class Switch (object):
  """
  A Pythonic way of achieving a non-pythonic methodology.
  
  Mildly useful; Please find flaws. 
  """
  
  def __init__(self, var):
    self.var = var
  
  def __enter__(self):
    return self
  
  def __exit__(self, *args):
    return
  
  def case(self, var, val=None, obj=False, dict=False):
    if isinstance(var, (list, tuple)):
      return self.var in var
    elif isinstance(var, str):  
      return var == self.var
    elif isinstance(var, object) and obj:
      if hasattr(var, self.var):
        return getattr(self.var, var) == val
    elif isinstance(var, dict) and dict:
      if var in self.var:
        return self.var[var] == val
    
  def default(self):
    return True

class GendiffResult (object):
  
  def __init__(self, **kwargs):
    for key, value in kwargs.items():
      setattr(self, key, value)

  
  #@property
  def to_dict(self):
    return dict(
      key=self.key,
      change=self.change,
      left=self.left,
      right=self.right
    )
  
  def __repr__(self):
    return "<GendiffResult(key='{key}', change='{change}', left='{left}', right='{right}')>".format(
      key=self.key,
      change=self.change,
      left=self.left,
      right=self.right
    )


class GendiffReport (object):
  
  def __init__(self, results):
    assert isinstance(results, dict), 'Invalid Result Set'
    
    self.colourize = {
      'right_key_missing': dict(
                                 left='#00FF00',
                                 right='#FF0010',
                               ),
      'dictionary_item_removed': dict(
                                  left='#FF0010',
                                  right='#00FF00',
                                 ),
      'values_changed': dict(
                          left='#FFFF00',
                          right='#FFFF00',
                        ),
      'type_changes': dict(
                        left='#FFFF00',
                        right='#FF0010',
                      ),
                                    
    }
    
    self.pretty_types = dict(
      str='String',
      int='Number/Integer',
      float='Decimal/Float',
      bool='Boolean',
      complex='Complex Number',
      
    )
    
    
    self.report = {}
    for key, result in results.items():
      self.report[key] = result.to_dict()
      
      self.report[key]['background_colour'] = self.colourize[result.change]
  
      with Switch(result.change) as s:
        if s.case('type_changes'):
          left_type = self.type_pretty(result.left)
          right_type = self.type_pretty(result.right)
          
          
          self.report[key]['left_type'] = left_type
          self.report[key]['right_type'] = right_type
          continue
        if s.case('left_key_missing'):
          #print('She gone!')
          continue
          
  def type_pretty(self, var):
    pretty = re.findall(r"'(.*?)'", str(type(var)), re.DOTALL)
    
    if len(pretty) > 0:
      return self.pretty_types[pretty[0]]
    else:
      return 'Unknown'
    
  def __getitem__(self, key):
    return getattr(self, key)
  
  @property
  def to_dict(self):
    return self.report
  


class Gendiff (object):
  
  def __init__(self):
    pass
  
  
  def key_diff(self, left, right):
    
    ct = {
      'dictionary_item_added' : '',
      'dictionary_item_removed' : '',
      'values_changed' : '',
      'type_changes' : ''
    }
    
    dd = DeepDiff(left, right, ignore_order=True, 
                  report_repetition=True, verbose_level=2,
                  view='tree')
    
    results = {}
    for key, value in ct.items():
      try:
        item = list(dd[key])[0]
        #print('{}: {}'.format(key,item))
        
        # t1 = left, get value from object
        k = str(item.path()).lstrip("root['").rstrip("']")
        left_value = item.t1
        right_value = item.t2
        
        if isinstance(left_value, deepdiff.helper.NotPresent):
          left_value = str(left_value)
        
        if isinstance(right_value, deepdiff.helper.NotPresent):
          right_value = str(right_value)
          
        if key == 'dictionary_item_removed':
          key = 'right_key_missing'
        elif key == 'dictionary_item_added':
          key = 'left_key_missing'
        
          
        # type_changes
        
        results[k] = GendiffResult(key=k, 
                                   left=left_value,
                                   right=right_value,
                                   change=key)
      except KeyError:
        #print(dd[key])
        pass
    
    #print(results)
    print('---')
    x = GendiffReport(results)
    #from pprint import  pprint
    import json
    print(json.dumps(x.to_dict, indent=2))

if __name__ == '__main__':
  import console
  console.clear()
  Gendiff().key_diff(d1, d3)
  

