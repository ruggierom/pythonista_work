# -*- coding: utf-8 -*-

import os
import platform
import plistlib
import scene
import sys  # noqa


class SystemInfo (object):
  """
  Get information on running system
  
  Portions from:
    https://github.com/cclauss/Ten-lines-or-less/blob/master/pythonista_version.py
  """
  
  def __init__(self):
    try:
      self.ios_ver, _, self.machine_model = platform.mac_ver()
    except:
      self.ios_ver = None
      self.machine_model = None
      pass
      
  
  @staticmethod
  def pythonista_version():
    try:
      plist = plistlib.readPlist(os.path.abspath(os.path.join(sys.executable,
                                                 '..', 'Info.plist')))
      return '{CFBundleShortVersionString} ({CFBundleVersion})'.format(**plist)
    except Exception:
      return None
    
    def tribute_to_cclaus(self):
      """
      The remainder (exceptnfor adding self.) of the script from github by cclaus
      """
      ios_ver, _, machine_model = platform.mac_ver()
      bit = platform.architecture()[0].rstrip('bit') + '-bit'
      rez = '({:.0f} x {:.0f})'.format(*scene.get_screen_size())
      fmt = ('Pythonista version {} running Python {} on iOS {} on a {} {} with a '
             'screen size of {} * {:.0f}')
      print(fmt.format(self.pythonista_version(), platform.python_version(), ios_ver,
                       bit, machine_model, rez, scene.get_screen_scale()))

    @property
    def iPad(self):
      pass
    

"""
Decorators
"""

class RequiresPython(object):

    def __init__(self, required):
        from distutils.version import StrictVersion
        current = '.'.join(map(str, sys.version_info[0:3]))
        self.passing = StrictVersion(current) >= StrictVersion(required)
 

    def __call__(self, f):
        def null():
          return None
    
        def wrapper(*args):
          if self.passing:
            f(*args)
          else:
            return null()
        return wrapper
        
