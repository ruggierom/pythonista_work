# -*- coding: utf-8 -*-

from six import add_metaclass as metaclass
from collections import namedtuple as new_cfg
from os.path import join, dirname
import inspect
from abc import (
    ABCMeta, 
    abstractmethod, 
    abstractproperty
)
from yaml import (
    safe_load_all as yaml_load, 
    safe_dump_all as yaml_dump
)

from inspect import (
    getabsfile as _getfile,
    ismodule,
)

from munch import Munch

class Conf(Munch):
    pass

m = Conf({'o': 't'})
print(m)

@metaclass(ABCMeta)
class Base (object):
    """ Base Class
    
    Methods taken/modified from:
        https://github.com/alexprengere/reprmixin
        (c) Alex Prengere, Apache License 2.0
    """
    
    
    @staticmethod
    def _find_attrs(obj):
        """Iterate over all attributes of objects."""
        visited = set()
    
        if hasattr(obj, '__dict__'):
            for attr in sorted(obj.__dict__):
                if attr not in visited:
                    yield attr
                    visited.add(attr)
    
        for cls in reversed(inspect.getmro(obj.__class__)):
            if hasattr(cls, '__slots__'):
                for attr in cls.__slots__:
                    if hasattr(obj, attr):
                        if attr not in visited:
                            yield attr
                            visited.add(attr)

    def __repr__(self):
        return '{0}({1})'.format(
            self.__class__.__name__,
            ', '.join('{0}:{1}'.format(attr, repr(getattr(self, attr)))
                      for attr in self._find_attrs(self)
                      if not attr.startswith('_')))
                      


class ConfigBase (Base):
    
    def __init__(self):
        self._did_super = True
    
    @property
    def config_file(self):
        pass
    
    @config_file.setter
    def config_file(self, value):
        pass
        
    @abstractmethod
    def load_config_file(self):
        pass


## 

class ConfigItem (object):
    def __new__(self, key, value):
        return new_cfg('ConfigItem', key)(value)

        
c = ConfigItem('peck', 'er')
print(c)
cb = ConfigItem('trew','dork')
#c.peck = 'terppp'
print(cb)

class ConfigItems (Base):
    pass

class SchemaConfig(ConfigBase):
    
    def __init__(self):
        super(SchemaConfig, self).__init__()
        
        """
        When using a CLI, __file__ represents the executable, not the containing file.
        
        The command below will allow it to work ad-hoc (running file directly), as well as via module or CLI.
        """
        cfgfile = _getfile(self) if ismodule(self) else __file__
        
        self.config = ConfigItems()
        
        self.load_config_file(
            join(
                dirname(cfgfile),
                'data',
                'schema.yml'
            )
        )

    def load_config_file(self, filename):
        try:
            with open(filename, 'r') as config:
                
                for item in yaml_load(config):
                    
                    for key, value in item.items():
                        setattr(self.config, key, value)
                
        except  IOError as e:
            # TODO: proper logging
            raise e
    

# Setup "from td_azure.config import *"" Exports

def all_subclasses(cls):
    """ List Subclasses of Object (By Name)
    
    This method allows us to use:
        from td_azure.config import * 
    
    Note:
        This is a convenience function only.
    """
    return tuple([obj.__name__ for obj in set(cls.__subclasses__()).union(
        [s for c in cls.__subclasses__() for s in all_subclasses(c)])])

__all__ = all_subclasses(ConfigBase)

s = SchemaConfig()
print(s)
#print(s)
