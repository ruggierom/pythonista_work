from collections import namedtuple as _nt

Keypair = _nt('Keypair', 
    ['name',' key', 'keysecret'])

del _nt # Clean up renamed import just in case

__all__ = ['Keypair']
