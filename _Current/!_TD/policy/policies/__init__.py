# -*- coding: utf-8 -*-

if __name__ == '__main__':
  """
  Execute if run directly
  """
  raise SystemError('Do not run this file directly.')

from . import *
