#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Change the current working directory.

A replacement for stash's built in cd, allows for:
    * "cd -" Goto previous folder
    * "cd --" Goto the 2nd last folder you were in.
    * "cd ! '<name>'" - Switch to project <name>'s root (if using project manager)

Options - set via Environment Variable (.stashrc):
    * CD_SAVE_HISTORY - False or filename/location
    * CD_HISTORY_LIMIT
    * CD_MAX_BACKPEDAL (6) How many -'s to allow
    
"""
from __future__ import print_function

import argparse
import os
import sys

sys.path.insert(0, os.path.dirname(__file__))
from anabolic import config

try:
    _ = _stash(r'echo 0 > /dev/null')
    STASHENV=True
except:
    STASHENV=False


## Options

CD_SAVE_HISTORY=config.get('CD_SAVE_HISTORY', True)
CD_HISTORY=config.get('CD_HISTORY','~/.anabolic/.cd_history')

def get_option(item):
    """
    Get an option; if folder/file does not exist then create
    """
    

def save_history(cwd):
    if config.get('CD_SAVE_HISTORY', True):
        open(config.get('CD_HISTORY','~/.anabolic/.cd_history'), 'w') as hist:
            
    pass


def main(args):
    p = argparse.ArgumentParser(description=__doc__)
    p.add_argument("dir", action="store", nargs="+",
                   default=os.environ["HOME2"], type=str,
                   help="the new working directory")
    ns = p.parse_args(args)

    status = 0
    target_dir = ''
    try:
        # Save current dir
        current_dir = os.getcwd()
        
        # handle spsaces since this doesn't have opts
        target_dir = ' '.join(ns.dir)
        
        if os.path.exists(target_dir):
            if os.path.isdir(target_dir):
                # chdir does not raise exception until listdir is called, so check for access here
                if os.access(target_dir, os.R_OK):
                    os.chdir(target_dir)
                    # Successfully changed - save history
                    save_history(current_dir)
                else:
                    print('cd: {} access denied'.format(target_dir))
            else:
                print('cd: %s: Not a directory' % target_dir)
        else:
            print ('cd: %s: No such file or directory' % target_dir)
    except Exception as err:
        print("cd: {}: {!s}".format(type(err).__name__, err), file=sys.stderr)
        status = 1

    sys.exit(status)


if __name__ == "__main__":
    main(sys.argv[1:])
