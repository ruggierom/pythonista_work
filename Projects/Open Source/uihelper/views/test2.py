@davidenrique9

```
def go(sender):
    search=sender.superview['Title']#from timelineUI.pyui
    page=browser.superview['webview1']#from browser.pyui
    page.load_url('https://en.m.wikipedia.org/wiki/'+search.text)
    sender.superview.add_subview(ui.load_view('browser'))

ui.load_view('TimelineUI').present('sheet')
```

Although I do suggest looking through some of code posted here on the forums - use a forum search.

Or - better yet [Read ui.View Documentation](http://omz-software.com/pythonista/docs/ios/ui.html#view)

[edit] realized I sounded like  jerk there. more info below


Here is a larger example below:

```
subview = None

def load_subview(superview, view_to_load):
    subview = view_to_load
    superview.add_subview(view_to_load)

    # optional if your titlebar is showing
    superview.right_button_items = [
      ui.ButtonItem(title='Done', action=unload_subview)
    ]

def unload_subview(sender):
    if subview:
        subview.superview.remove_subview(subview)
        # optional - remove the "Done" button
        superview.right_button_items = None

def go(sender):
    search=sender.superview['Title']#from timelineUI.pyui
    page=browser.superview['webview1']#from browser.pyui
    page.load_url('https://en.m.wikipedia.org/wiki/'+search.text)

    load_view(ui.load_subview('browser'))

ui.load_view('TimelineUI').present('sheet')
```

I've not tested this as I mocked it up quickly.

When you wish to present alternate views you do so by using add_subview()

If the secondary view takes up the full screen, it will hide the previous view, if not - say if it is smaller or the "flex" attribute is not being set it will take up as much of the screen as it is instructed to.

i setup a title bar button item which should appear with your secondary view and disappear when closed, as well as showed the use case for remove_subview.

If you do not track the subviews, you will have a more difficult time managing it.

