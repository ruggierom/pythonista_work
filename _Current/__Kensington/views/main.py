import ui




class KensingtonMidwives (object):
    """ Main Class """
    
    def __init__(self):
        """ Class Initializer """
        
        v = ui.load_view()
        v.present('sheet')
        self.home = ui.load_view('home')
        self.home.flex = 'WH'
        self.content_view = v['content_view']
        self.content_view.add_subview(self.home)
        

KensingtonMidwives()
