# -*- coding: utf-8 -*-
#!python2
import ui
import speech
import sound
import console
import requests

st = """
One|M
Two|A
"""

#import sys, json; print(dict([i.strip('\n').split('|') for i in str(sys.stdin).splitlines() if len(i) > 2 ]))
"""
z = [ 
    { k:v for (k, v) in i.strip('\n').split('|') }
    
    for i in str(st).splitlines() if len(i) > 2
] 
"""
#import sys, json

#print(dict([i.strip('\n').split('|') for i in str(st).splitlines() if len(i) > 2 ]))

ht = """
<html>
  <head>
    <script src="https://aframe.io/releases/0.8.0/aframe.min.js"></script>
  </head>
  <body>
    <a-scene>
      <a-box position="-1 0.5 -3" rotation="0 45 0" color="#4CC3D9"></a-box>
      <a-sphere position="0 1.25 -5" radius="1.25" color="#EF2D5E"></a-sphere>
      <a-cylinder position="1 0.75 -3" radius="0.5" height="1.5" color="#FFC65D"></a-cylinder>
      <a-plane position="0 0 -4" rotation="-90 0 0" width="4" height="4" color="#7BC8A4"></a-plane>
      <a-sky color="#ECECEC"></a-sky>
    </a-scene>
  </body>
"""
"""
from objc_util import *
import sys

load_framework('ARKit')

SCNScene = ObjCClass('SCNScene')
ARSCNView = ObjCClass('ARSCNView')
ARWorldTrackingConfiguration = ObjCClass('ARWorldTrackingConfiguration')
ARSession = ObjCClass('ARSession')
ARSKView = ObjCClass('ARSKView')
#v = ui.WebView()
#v.load_html(ht)
#v.present()

sys.exit(0)
"""

class Modal (ui.View):
    
    def __init__(self, shrink=20, pts=False):
                     
        width, height = ui.get_window_size()
        
        w = self._shrink(width, shrink, pts)
        h = self._shrink(height, shrink, pts)
        
        
        self.frame = (
            (round(width - w) / 2),
            (round(height -h) / 2) - 35,
            w,
            h
        )
        
        self.corner_radius = 12
        self.border_color = '#ccc'
        self.border_width = 2
        
        
    def _shrink(self, value, shrink, pts):
        value = float(value)
        shrink = float(shrink)
        if pts:
            return int(value - shrink)
        else:
            return float(round(value - ((value * shrink) / 100)))
            
    def custom(self, x, y, width, height):
        #self.size = (width, height)
        #self.position = (x, y)
        self.flex = 'W'
        self.frame = (x, y, width, height)
        return self

class TextDelegate (object):
    
    def textfield_should_begin_editing(self, textfield):
        main = textfield.superview
        #print(main)
        return True
    def textfield_did_begin_editing(self, textfield):
        pass
    def textfield_did_end_editing(self, textfield):
        pass
    def textfield_should_return(self, textfield):
        textfield.end_editing()
        return True
    def textfield_should_change(self, textfield, range, replacement):
        return True
    def textfield_did_change(self, textfield):
        pass
    
    

class TTS (ui.View):
    
    last_speech = None
    
    def __init__(self):
        w, h = ui.get_window_size()
        
        self.sv = ui.ScrollView()
        self.sv.flex = 'WH'
        
        self.background_color = '#eee'
        self.flex = 'WH'
        self.frame = (0, 0, w, h)
        self.name = 'Text to Speech'
        self.sv.frame = self.frame
        
        # instruction bubble
        
        m = Modal().custom(15, 15, (w-30), 80)
        m.background_color = '#fff'
        
        # instructions
        
        tx = ui.TextView(
            frame=(10, 5, w, 80),
            text="""Tell me what you would like me to say... But if I say it wrong, write it phenetically.

For example: fuckity fuckity fuck fuck fuck fark
            """,
            editable=False,
            scroll_enabled=False
        )
        
        m.add_subview(tx)
        self.sv.add_subview(m)
        self.sv.delegate = self
        self.delegate = self
        
        # Input
        
        i = Modal().custom(15, 110, (w-30), 150)
        i.background_color = '#fff'
        
        lbl = ui.Label(
            text ='What shall I say?',
            frame = (10, 5, 150, 30),
            flex = 'W'
        )
        
        self.t = ui.TextField(
            placeholder='just type anything at all',
            frame = (10, 40, 360, 40),
            flex='W',
            clear_button_mode='while_editing',
            delegate=TextDelegate(),
        )
        
        b = ui.Button(
            name='speak',
            image=ui.Image.named(
                'iob:speakerphone_32'
            ),
            title='Speak',
            action=self.speak, 
            x=280,
            y=95
        )
        
        i.add_subview(lbl)
        i.add_subview(self.t)
        i.add_subview(b)
        
        i.delegate = self
        
        self.sv.add_subview(i)
        
        # joke panel
        
        jokes_view = ui.load_view('inner')
        
        nh = (h - 365)
        s = Modal().custom(15, 275, (w-30), nh)
        s.background_color = '#fff'
        
        for v in jokes_view.subviews:
            #print(v)
            s.add_subview(v)
        
        
        self.sv.add_subview(s)
        self.add_subview(self.sv)
        self.kb_y = 0.00
        
    def touch_began(self, touch):
        print('touch')
        #self.t.end_editing()
        pass

    def keyboard_frame_did_change(self, frame):
        self.kb_y = frame[1]
        if self.kb_y == 0:
            # move TextView back to orig pos
            pass
        else:
            dy = 0
            view = self
            while view:
                dy += view.y
                view = view.superview
            self.kb_y -= dy
            # Move TextView to be all above kb_y
    
    def speak(self, sender):
        text = self.t.text
        print(text)
        
        if len(text) < 1:
            console.show_activity()
            speech.say('dumb ass, you need to write something first')
            console.hide_activity()
            console.alert(
                'Dumbass',
                'You need to write something first',
                'I am an idiot',
                hide_cancel_button=True)
        console.show_activity()
        print("saying it..")
        x = speech.say(text)
        print(x)
        console.hide_activity()
        pass
    
    def apijoke(self, sender):
        punchline = ''
        def punch():
            speech.say(punchline,'en_US')
        
        try:
            url = 'https://08ad1pao69.execute-api.us-east-1.amazonaws.com/dev/random_joke'
            
            # This particular API needs you to specify it wants JSON.
            
            h = {'Accept' : 'application/json'}
            r = requests.get(url, headers=h)
            if r.status_code == 200:
                joke = r.json()
                punchline = joke['punchline']
                setup = joke['setup']
                speech.say(setup, 'en_US')
                ui.delay(punch, 2)
        except Exception as e:
            print(e)
            speech.say('Something went wrong calling the API.', 'en_US')
        pass
    
    
    def spreak(self, sender):
        if hasattr(sender, 'lang'):
            #print(sender.lang)
            lang = sender.lang
        else:
            lang = 'en_US'
        
        say = sender.say
        speech.say(say, lang)
        
        
if __name__ == '__main__':
    """
    {"id":25,"type":"programming","setup":"What do you call a programmer from Finland?","punchline":"nerdic"}
    
    {"id":21,"type":"programming","setup":"What's the best thing about a Boolean?","punchline":"Even if you're wrong, you're only off by a bit."}
    """
    TTS().present()
