# -*- coding: utf-8 -*-
"""
An tool created to finalize the system preparation for Windows users at TD Bank - Hosting Solutions.

Actions Performed:
    * Download / Install Ubuntu on Windows
        * Setup sshd, including start on load
        * Setup Firefox + Enable roots
        * Setup nfs/smb for file sharing
        * Auto create shared work folder
        * Setup sudo no passwd
        * Setup Banners + MOTD
    * Download/Install Docker
    * Add user to docker_users group
    * Install docker.py
    * Setup PyPi Configs
    * Download/Install Choco - Non-Admin
    * Downlod/Install Visual Studio Code
    * Download/Install Sublime
    * Download/Install Atom
    * Download/Install GIT
    * Download/Install SourceTree
    * Download/Install WinSCP
    * Add User to Hyper-V Group
    * Setup/Install VirtualBox
    * Add tools to VBox
    
Possibly:
    * Install PyWinAuto
    * Script to show tickets in MOTD

Later:
    * Download/Install Visual Studio
"""
