# -*- coding: utf-8 -*-
import console
import editor
import dialogs
import os
import ui

gitignore = '.gitignore'
def find_gitignore(cwd):
    file = os.path.join(cwd, gitignore)
    if os.path.exists(file):
        return file
    else:
        pdir = os.path.abspath(os.path.join(cwd, os.pardir))
        if pdir != cwd:
            return find_gitignore(pdir)


# Options
OPEN_IN_EDITOR=False
EDITOR_NEW_TAB=True

cwd = os.path.dirname(editor.get_path())
gitigfile = find_gitignore(cwd)

if gitigfile:
    os.chdir(cwd)    # Make sure python is back
    answer = console.alert(
        'Edit gitignore',
        'Found {}.\nDo you wish to edit this file?'.format(
            os.path.relpath(gitigfile)
        ), 
        'Yes',
        'No',
        hide_cancel_button=True
    )
    if answer == 1:
        if OPEN_IN_EDITOR:
            editor.open_file(gitigfile, new_tab=EDITOR_NEW_TAB)
        else:
            gdata = None
            with open(gitigfile, 'r') as f:
                gdata = f.read()
                gdata = dialogs.text_dialog(
                    title='.gitignore',
                    text=gdata,
                    autocorrection=None,
                    autocapitalization=ui.AUTOCAPITALIZE_NONE,
                    spellchecking=None
                )
            if gdata:
                with open(gitigfile, 'w') as f:
                    f.write(gdata)
                    console.hud_alert('.gitignore saved')
    else:
        print('User cancelled.')
