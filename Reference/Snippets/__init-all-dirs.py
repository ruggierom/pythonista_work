# -*- coding: utf-8 -*-

import os

__all__ = [d for d in os.listdir('.') if os.path.isdir(d)]
