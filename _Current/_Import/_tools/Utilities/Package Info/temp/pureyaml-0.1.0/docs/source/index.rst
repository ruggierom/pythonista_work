Welcome to pureyaml's documentation!
====================================

Contents:
=========

.. toctree::
   :maxdepth: 4

   Readme <readme>
   Installation <installation>
   Usage <usage>
   Contributing <contributing>
   Authors <authors>
   History <history>
   pureyaml API Docs <pureyaml>
   pureyaml.grammar API Docs <pureyaml.grammar>

Feedback
========
If you have any suggestions or questions about **pureyaml** feel free to email me at bionikspoon@gmail.com.

If you encounter any errors or problems with **pureyaml**, please let me know!
Open an Issue at the GitHub https://github.com/bionikspoon/pureyaml main repository.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
