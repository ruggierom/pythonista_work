# -*- coding: utf-8 -*-
"""
Metronome Manager

Offers the following:
  * Configutation
  * View/Delete autotabs
  * view/delete recordings
  * smokes a bowl
"""

import os
import zipfile

class MetronomeManager (object):
  
  def __init__(self):
    pass

  @staticmethod
  def zipme():
    import shutil
    shutil.make_archive('metronome', 'zip', '.')
    
MetronomeManager.zipme()
