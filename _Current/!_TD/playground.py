# -*- coding: utf-8 -*-
import os
from policy import *
import console
from six import add_metaclass

console.clear()

"""

Use metaclass to force super call. 
Use inspect to ensure that it is defined? 
    * Use Env and Config to fail if not super?
    * have import to stop failure
    * Setup run() function abstract
    *  
Logging profiles? 


Med App

CLI:
    * Define cli group
    * Setup properties and methods for CLI
    * Setup command info?
    * Setup a test for context management


"""
import sys
from os import environ as env
from virtualenv import create_environment
#import sh

class Environ (object):
    def __init__(self):
        pass

try:
    from virtualenv import __version__ as venv_version
except ImportError as e:
    raise SystemError(e)



if 'venv_version' in env.keys():
    if venv_version != env['venv_version']:
        print('Setting Up New Virtual Environment to build Custom Venv')
        pass # Setup venv
else:
    pass



sys.exit(0)



import click

@click.group
def cli():
    pass


class CLIMeta (type):
    pass



from abc import ABCMeta, abstractmethod
import math

class PolyMeta (ABCMeta):
    
    def __init__(self):
        super(PolyMeta, self).__init__()
        

@add_metaclass(ABCMeta)
class Polygon(object):

    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def perimeter(self):
        pass

    @abstractmethod
    def area(self):
        pass

class Circle(Polygon):
    def __init__(self, radius):
        self.radius = radius

    def perimeter(self):
        return 2 * math.pi * self.radius

#    def area(self):
#        return 2 * math.pi * self.radius


print(globals().keys())

# c = Circle(9.0)
# TypeError: Can't instantiate abstract class Circle with abstract methods area

'''
"""
TD Azure Work - Playground
"""

# Custom Exceptions

class MissingEnvironmentVariableError (Exception):
    pass


class AzureTDBank (object):
    """
    Generic Interface and reusable snippets.
    
    This is an experiment
    """
    
    _REQUIRED_ENV_VARS = [
        'AZURE_TENANT_ID',
        'AZURE_CLIENT_ID',
        'AZURE_CLIENT_SECRET',
        'AZURE_SUBSCRIPTION_ID',
    ]
    
    def __init__(self, **kwargs):
        
        for key in self._REQUIRED_ENV_VARS:
            """
            Initialize Blank Vars to allow use of env var or coded details.
            """
            setattr(self, key, None)
         
        for k, v in kwargs.items():
            try:
                setattr(self, k, v)
            except:
                pass
        
        empty_keys = [ i for i in self._REQUIRED_ENV_VARS ]
        if empty_keys:
            raise KeyError('Missing Azure Authentication Keys: {keylist}'.format(keylist=','.join(empty_keys)))
        
        
        
    def _check_env_vars(self):
        """
        Check if required Env Vars are set.
        
        Note:
            In future it may be helpful to add additional
            validation such as data types, min/max/exact 
            length or full data validation.
        """
        try:
            for envvar in self._REQUIRED_ENV_VARS:
                if os.environ.get(envvar, None):
                    try:
                        setattr(self, var, os.environ.get(var))
                    except AttributeError as e:
                        """
                        Placeholder for furure improvements
                        """
                        raise e
                """
                else:
                    raise MissingEnvironmentVariableError(
                        'Missing Evironment Variable {}'.format(envvar)
                    )
                """
        except AttributeError:
            raise AttributeError('Fatal Error: Unable to see the required environment variables list. ')

if __name__ == '__main__':
    # force fake env vars
    """
    for var in AzureTDBank._REQUIRED_ENV_VARS:
        os.environ.putenv(var, '1234')
        print('setting {}'.format(var))
        print(os.environ.get(var))
    """
    #print(os.get_exec_path())
    AzureTDBank()

'''


# -*- coding: utf-8 -*-

y = '''---
reporting:
  stored_procs:
    reportingsp:
      uses:
        usedin: breadcrumb
    secondProc:
      uses:
        usedin: something_else
'''

z = '''---
reporting:
  stored_procs:
    reportingsp:
      uses:
        usedin: breadcrumb
    secondProc:
      uses:
        hertlocker: something_else
'''

import yaml
from six import StringIO
from deepdiff import DeepDiff

yy = StringIO()
yy.write(y)
yy.seek(0)
zz = StringIO()
zz.write(z)
zz.seek(0)

t = yaml.safe_load(yy)
r = yaml.safe_load(zz)

s = DeepDiff(r, t)
print(s)

##
"""

import argparse
import getpass
import sys

import json
import keyring
import requests

#-----------------------------------------------------------------------------
# Globals

BASE_URL = "http://confluence.XXXXX.com/rest/api/content"

VIEW_URL = "http://confluence.XXXXX.com/pages/viewpage.action?pageId="


def pprint(data):
    '''
    Pretty prints json data.
    '''
    print json.dumps(
        data,
        sort_keys = True,
        indent = 4,
        separators = (', ', ' : '))


def get_page_ancestors(auth, pageid):

    # Get basic page information plus the ancestors property

    url = '{base}/{pageid}?expand=ancestors'.format(
        base = BASE_URL,
        pageid = pageid)

    r = requests.get(url, auth = auth)

    r.raise_for_status()

    return r.json()['ancestors']


def get_page_info(auth, pageid):

    url = '{base}/{pageid}'.format(
        base = BASE_URL,
        pageid = pageid)

    r = requests.get(url, auth = auth)

    r.raise_for_status()

    return r.json()


def write_data(auth, html, pageid, title = None):

    info = get_page_info(auth, pageid)

    ver = int(info['version']['number']) + 1

    ancestors = get_page_ancestors(auth, pageid)

    anc = ancestors[-1]
    del anc['_links']
    del anc['_expandable']
    del anc['extensions']

    if title is not None:
        info['title'] = title

    data = {
        'id' : str(pageid),
        'type' : 'page',
        'title' : info['title'],
        'version' : {'number' : ver},
        'ancestors' : [anc],
        'body'  : {
            'storage' :
            {
                'representation' : 'storage',
                'value' : str(html),
            }
        }
    }

    data = json.dumps(data)

    url = '{base}/{pageid}'.format(base = BASE_URL, pageid = pageid)

    r = requests.put(
        url,
        data = data,
        auth = auth,
        headers = { 'Content-Type' : 'application/json' }
    )

    r.raise_for_status()

    print "Wrote '%s' version %d" % (info['title'], ver)
    print "URL: %s%d" % (VIEW_URL, pageid)


def get_login(username = None):
    '''
    Get the password for username out of the keyring.
    '''

    if username is None:
        username = getpass.getuser()

    passwd = keyring.get_password('confluence_script', username)

    if passwd is None:
        passwd = getpass.getpass()
        keyring.set_password('confluence_script', username, passwd)

    return (username, passwd)


def main():

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-u",
        "--user",
        default = getpass.getuser(),
        help = "Specify the username to log into Confluence")

    parser.add_argument(
        "-t",
        "--title",
        default = None,
        type = str,
        help = "Specify a new title")

    parser.add_argument(
        "-f",
        "--file",
        default = None,
        type = str,
        help = "Write the contents of FILE to the confluence page")

    parser.add_argument(
        "pageid",
        type = int,
        help = "Specify the Conflunce page id to overwrite")

    parser.add_argument(
        "html",
        type = str,
        default = None,
        nargs = '?',
        help = "Write the immediate html string to confluence page")

    options = parser.parse_args()

    auth = get_login(options.user)

    if options.html is not None and options.file is not None:
        raise RuntimeError(
            "Can't specify both a file and immediate html to write to page!")

    if options.html:
        html = options.html

    else:

        with open(options.file, 'r') as fd:
            html = fd.read()

    write_data(auth, html, options.pageid, options.title)


if __name__ == "__main__" : main()
"""
