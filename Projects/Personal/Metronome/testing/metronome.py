# -*- coding: utf-8 -*-

"""
https://greensock.com/draggable

https://www.jqueryscript.net/demo/Knob-Style-Rotary-Switch-Plugin-with-jQuery-rotarySwitch/
"""

import ui
from gestures import Gestures
from objc_util import *
import console
from PIL import Image, ImageDraw, ImageFont
from six import BytesIO
import  math
  
current_rotation = 0

def leave_notice(data):
  console.hud_alert('Hold to Quit','error',.75)
  global  current_rotation
  print(current_rotation)

def p2pi(ip):
    with BytesIO() as bIO:
        ip.save(bIO, 'PNG')
        return ui.Image.from_data(bIO.getvalue())

v = ui.load_view()

power = v['power']
knob1 = v['knob1']
knob1['knob01'].image = ui.Image('../assets/knobs/lightBigFront.png')
knob1['knob01'].transform = ui.Transform.rotation(-2.5)

knobback = v['knobback']
knob2 = v['knob2']
knobback.image = ui.Image('../assets/knobs/darkBigBackground@2x.png')
knob2['knob02pin'].image = ui.Image('../assets/knobs/darkBigFront@2x.png')
knob2['knob02pin'].transform = ui.Transform.rotation(-2.5)

viewport = v['viewport']
header_img = viewport['headertext']

tempo_label = viewport['tempo_label']
tempo_value = viewport['tempo_value']

def draw_text(text, view, font_size):
  size = (int(view.width), int(view.height), )
  txt = Image.new('RGBA', size, (255, 0, 0, 0)) 

  # get a font
  # elektra.ttf
  fnt = ImageFont.truetype('../assets/fonts/digital-7-mono.ttf', font_size)
  # get a drawing context
  d = ImageDraw.Draw(txt)
  
  opacity = 255
    
  # draw text, half opacity
  d.text((5, 5), text, font=fnt, fill=(0, 0, 0,opacity))
  view.image = p2pi(txt)
  return
  

def leave(sender):
  v.close()

@on_main_thread
def rotation_handler(data):
  global current_rotation
  #global knob1
  
  def dt():
    draw_text(str(int(data.rotation * 1.5 * 25)),tempo_value, 25)
  
  if data.state == Gestures.CHANGED:
    knob1.transform = ui.Transform.rotation(data.rotation * 1.5)
    """
    if current_rotation + (data.rotation * 1.5) < 0:
      current_rotation = 0
    #elif current_rotation + (data.rotation * 1.5) > 4.79:
    #  current_rotation = 4.79
    else:
      knob1.transform = ui.Transform.rotation(data.rotation * 1.5)
      current_rotation += data.rotation * 1.5
      #ui.delay(dt, .2)
    """
    ui.delay(dt, .2)
  
  if data.state == Gestures.ENDED:
    current_rotation += data.rotation * 1.5
    



  
def draw_header_text(text, size, opacity = 1.0):                
  txt = Image.new('RGBA', size, (255, 0, 0, 0)) 

  # get a font
  # elektra.ttf
  fnt = ImageFont.truetype('../assets/fonts/digital-7-mono.ttf', 60)
  # get a drawing context
  d = ImageDraw.Draw(txt)
  
  opacity = int(opacity * 255)
  if opacity > 255:
    opacity = 255
    
  # draw text, half opacity
  d.text((5, 5), text, font=fnt, fill=(0, 0, 0,opacity))
  #out = Image.alpha_composite(txt)
  return p2pi(txt)

def tempo_knob(gest_data):
  global header_img
  global current_rotation
  
  w = int(header_img.width)
  h = int(header_img.height)
  loc = gest_data.location
  # i = location of finger
  x, y = loc
  
  # calculate delta angle
  #angle = math.atan2(y, x)
  angle = (math.atan2(y-64, x-64) / 2)
  current_rotation += angle
  
  knob2.transform = ui.Transform.rotation(current_rotation)
  
  header_img.image = draw_header_text('Moose:{0:.7f}'.format(angle), (w,h))
  pass

draw_text('Tempo: ', tempo_label, 25)
draw_text('0', tempo_value, 25)

g = Gestures()
g.add_rotation(knob1, rotation_handler)
g.add_long_press(power, 
                 leave, 
                 minimum_press_duration=1.5)

g.add_pan(knob2, tempo_knob)


v.present('fullscreen', hide_title_bar=True, orientations = ['landscape'])

w = int(header_img.width)
h = int(header_img.height)
header_img.image = draw_header_text('Booo: ', (w,h))
