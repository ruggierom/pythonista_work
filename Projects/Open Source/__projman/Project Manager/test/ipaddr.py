# -*- coding: utf-8 -*-

import socket
from objc_util import *

def network_state():
    # return values:
    # 2 = WiFi is available
    # 1 = No WiFi, but cellular is available
    # 0 = No network availability at all
    net_state = 0
    try:
        wifi_ip = socket.gethostbyname(socket.gethostname())
        # We have an IP, without a connection - likely WiFi
        net_state = 2
        return net_state
    except (OSError, Exception) as e:
        net_state = 0
    try:
        test_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        test_socket.connect(('google.com', 80))
        cellular_ip = test_socket.getsockname()[0]
        # We have an IP, only after a connection - likely cellular
        net_state = 1
        return net_state
    except (OSError, Exception) as e:
        net_state = 0
        # We don't appear to have any connection
        net_state = 0
        
    return net_state


def objipaddr():
    pass

"""
#include <ifaddrs.h>
#include <arpa/inet.h>

- (NSString *)getIPAddress {

    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];

                }

            }

            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;

} 
"""
