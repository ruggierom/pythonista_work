import ui

"""
git clone git://github.com/Azure/azure-sdk-for-python.git

# download latest virtio iso image
>$ wget https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso

https://downloads.vivaldi.com/stable/Vivaldi.1.15.1147.42.x64.exe

"""
images_to_use = [
    'emj:Boy',
    'emj:Girl',
    'emj:Kissing_2'
    'emj:Man',
    'emj:Woman',
    'emj:Man_And_Woman',
    'emj:Womans_Boots',
    'emj:Womans_Hat',
    'emj:Womans_Sandal',
    'emj:Bikini',
    'emj:Kiss_Mark',
    'emj:Bactrian_Camel',
    'emj:Briefcase',
    'emj:Checkmark_3',
    'emj:Christmas_Tree',
    'emj:Closed_Umbrella',
    'emj:Dash_Symbol',
    'emj:Dizzy_Symbol',
    'emj:Handbag',
    'emj:House',
    'emj:Key',
    'emj:Lock_1',
    'emj:Lock_2',
    'emj:Lock_3',
    'emj:Meat_On_Bone',
    'emj:Medical_Mask',
    'emj:Money_Bag',
    'emj:Money_With_Wings',
    'emj:Monkey',
    'emj:Monkey_Hear-No-Evil',
    'emj:Monkey_See-No-Evil',
    'emj:Monkey_Speak-No-Evil',
    'emj:Mouth',
    'emj:Movie_Camera',
    'emj:No_Entry_1',
    'emj:No_Entry_2',
    'emj:Office_Building',
    'emj:Package',
    'emj:Page_Facing_Up',
    'emj:Page_With_Curl',
    'emj:Pedestrian',
    'emj:Person_Blond',
    'emj:Person_Frowning',
    'emj:Person_Pouting',
    'emj:Person_Raising_Hand',
    'emj:Personal_Computer',
    'emj:Police_Officer',
    'emj:Pistol',
    'emj:Princess',
    'emj:Rose',
    'emj:Silhouette',
    'emj:Star_1',
    'emj:Star_2',
    'emj:Womans_Clothes',
    'plc:Star',
    'plc:Heart',
    'typb:Info',
    'typb:Key',
    'typb:Star',
    'typb:Search',
    'typb:Relocate',
    'typb:Refresh',
    'typb:Sync',
    'typb:Warning',
    'typb:User',
    'typb:Locked',
    'typb:Unlocked',
    'typb:Anchor',
    'typb:Archive',
    'typb:Views',
    'iob:drag_32',
    'iob:ios7_eye_32'
]
# { custom_icos = ['⚠️','❗️','❓', '‼️', '⁉️','✅', '💭'] }

present_options = dict(
    style='Full Screen',
    hide_title_bar=True, 
    title_bar_color='#000'
)
# Add

class PhotoshootManager (ui.View):
    
    def __init__(self):
        self.present_options = dict(
            style='full_screen',
            hide_title_bar=True, 
            title_bar_color='#000'
        )
        self.frame=(0, 0, 320, 480)
        self.flex = 'WH'
        v = ui.load_view('Guide')
        v.frame = self.frame
        v.flex = 'WH'
        self.content_view = v
        self.content_view.background_color = '#333'
        
        """
        # Fix my fuckup
        for s in self.content_view.subviews:
            self.content_view.remove_subview(s)
        
        
        v = ui.load_view('Guide-iPad')
        vv = ui.load_view('Guide')
        #v = vv
        # Add Menubar
        self.content_view.add_subview(v['padding_device_menubar'])
        _, _, w, h = self.content_view.frame
        vh = self.content_view['padding_device_menubar'].height
        h -= vh
        main = ui.ScrollView(
            name='main',
            flex='WH',
            frame = (0, vh, w, h),
            background_color = 'white',
        )
                
        #self.add_subview(self.content_view)
        vv.frame = main.frame
        main.add_subview(vv)
        self.main = main
        self.add_subview(self.main)
        
        
        # Setup Delegates
        
        #self.content_view['area_main_toolbar']['command'].delegate = self
        #self.command_field = self.content_view['area_main_toolbar']['command']
        
        """
    
    def drop_in(self, view):
        for s in self.content_view.subviews:
            self.content_view.remove_subview(s)
        
        for s in view.subviews:
            # print('Adding {}, {} '.format(s, s.frame))
            self.content_view.add_subview(s)

    # TextField Delegate Commands
    def textfield_should_begin_editing(self, textfield):
        return True
        
    def textfield_did_begin_editing(self, textfield):
        print('did begin')
        pass
        
    def textfield_did_end_editing(self, textfield):
        pass
        
    def textfield_should_return(self, textfield):
        textfield.end_editing()
        return True
        
    def textfield_should_change(self, textfield, range, replacement):
        return True
        
    def textfield_did_change(self, textfield):
        pass                
    
    def enter_command(self, sender):
        #print(sender)
        print('hmmahtime')
                
        
    def layout(self):        
        #self.content_view.flex = 'WH'
        w, h = ui.get_window_size()
        #self.content_view.frame = (0, 0, w, h)
        
    
    def draw(self):
        pass

x = PhotoshootManager()
x.present(**present_options)
#x.drop_in(x.content_view)

# v = ui.load_view()
# v2 = ui.load_view('Guide-iPad')
# v2.background_color = '#333'
# v.background_color = '#eee'
#v.present('sheet', hide_title_bar=True, title_bar_color='#000')
# v2.present(**present_options)
