# -*- coding: utf-8 -*-

from six import text_type
from objc_util import *

#NSBundle.bundleWithPath_('/System/Library/Frameworks/JavaScriptCore.framework').load()

load_framework('JavaScriptCore')

class JSContext (object):
  """
  JSContext Class
  
  TODO:
    * Implement class methods
    * 
  """
  
  @on_main_thread
  def __init__(self, JSVirtualMachine=None):
    self._JSContext = ObjCClass('JSContext')
    self.context = self._JSContext.alloc().init()
  
  @on_main_thread
  def evaluate_script(self, script):
    """
    Executes the specified JavaScript code.
    
    :param str script: JavaScript code you want to execute
    :return Return from executed JavaScript code
    """
    assert isinstance(script, text_type)
    return self.context.evaluateScript_(script)
  
  def evaluate_script_with_source_url(script, url):
    """
    Executes the specified JavaScript code, treating the specified URL as its source location.
    
    :param str script: JavaScript code you want to execute
    :param NSURL url: Source location for script
    :return Return from executed JavaScript code
    """
    if isinstance(url, text_type):
      url = nsurl(url)
    
    assert isinstance(url, ObjCInstance)
    assert isinstance(script, text_type)
    return self.context.evaluateScript_withSourceURL(script, url)
    
  def current(self):
    """
    Returns the context currently executing JavaScript code.
    """
    pass
    
  
  def drf(self):
    print(dir(self._JSContext))
  
  def _dir(self):
    return dir(self.context)

context = JSContext()
#context.drf()

#print(context._dir())
#print(dir(context))

