# -*- coding: utf-8 -*-

class Certificate (object):
  
  def __init__(self)p
    self.id = 0
    self.server_id = 0
    self.site_id = 0
    self.domain = str()
    self.type = str()
    self.existing = False
    self.request_status = ''
    self.status = ''
    self.active = False
    self.activation_status = ''
    self.created_at = '' # Maybe datetime
    
  def delete(self):
    """
    Delete the given certifcate
    
    :return void
    """
    pass
  
  @property
  def signing_request(self):
    """
    Get the SSL certificate signing request for the site.
    """
    pass
    
    
  pass

class Credential (object):
  pass

class Daemon (object):
  pass

class FirewallRule (object):
  pass
  
class Job (object):
  pass

class MySQLDatabase (object):
  pass

class MySQLUser (object):
  pass

class Recipe (object):
  pass

class Resource (object):
  pass

class SSHKey (object):
  pass

class Server (object):
  pass

class Site (object):
  pass

class Worker (object):
  pass


