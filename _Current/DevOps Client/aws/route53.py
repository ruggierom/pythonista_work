# -*- coding: utf-8 -*-
"""
Route 53 AWS Implementation

Sets up a Data Source for 
"""
import ui
import json

try:
    from .client import Client
except ImportError:
    from client import Client
        
        
class Route53 (object):
    
    def __init__(self):
        self.client = Client().route53()
        self.zones = []
        self._get_records()
    
    def _get_records(self, marker=None):
        request_params=dict(
            MaxItems='100',
        )

        if marker:
            request_params['Marker'] = marker                
        
        response = self.client.list_hosted_zones(
            **request_params
        )
        
        try:
            metadata = response['ResponseMetadata']
            if metadata['HTTPStatusCode'] == 200:
                try:
                    for zone in response['HostedZones']:
                        self.zones.append(dict(
                            id=zone['Id'],
                            name=zone['Name'],
                            private_zone=zone['Config']['PrivateZone'],
                            num_records = zone['ResourceRecordSetCount'],
                        ))
                    
                    if response['IsTruncated']:
                        self._get_records(response['NextMarker'])
                except Exception as e:
                    raise e
        except Exception as e:
            raise e

        print(self.zones)        
        pass


class HostedZone (Route53):
    def __init__(self):
        super(HostedZone, self).__init__()
        
class HostedZonesDataSource (ui.ListDataSource):
    
    def __init__(self):
        hz = HostedZone()
        items = []
        
        for zone in hz.zones:
            title = '{domain}\Record Count: {recird_count}\nZone Id: {zone_id}'
            items.append(
                dict(
                    title=title,
                    image=None,
                    accessory_type='detail_button'
                )
            )
        
        super(HostedZoneDataSource,self).__init__(items)

__all__ = ['Route53']
