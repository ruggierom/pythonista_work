# -*- coding: utf-8 -*-
import ui
import bz2
from base64 import b64decode

__uiconfig__ = {
  'main_ui' : 'views/main',
  'installed_ui' : 'views/installed',
  'configure' : 'views/configure',
}

main_ui = '''\
QlpoOTFBWSZTWfYqku4ABMJfgHVVUGd/9T/nXY6/79/+UAS+6ndMLIm4oAg0
QCmnptUnqfpPSg0ADIGgAAAABoJiE0noakg0BoAAGjIAAAAkRCkepP1T1H6p
pppoZDIGjT8qaaGh5QA2RA5pkZDJghowmCNNGjEDTJkYAAgikIZTTTJMIU00
yYEZNGAAEMTTMiy6IQMCMAAf0Gb0R985JOkvUQMyC7ZeSFGDYizsmglNWsWU
iKNsAbtaFLrzSGcBddVVrAIg4CAH6IgXkEEQA0zW0MYEJjYrhIF8NNADM0qo
UV3vxr82/05/oDeocwTqBREQIlEmtgjOqQmpaViAMizGuF4iX7yHb8qXcNTj
VpoezSKzDx7fnUeFs+ZlraFeWQQ7r7V7ym8TYuwkQIaWvtDcLGzJjyAzWYOD
lGb2Phmm7krATCBQamBDw0jgKrokymjmDaRO4nNVgO4hmEQqw5zhCBnNJEC4
QCYgO6Ww+OacRlDCSCEcbMWotCoMVVCrIsohFmkq1jB7DW+zdLk2lsDKM8Mw
TMRdHERNbkK8OQTQ2280C8Ucnl2p5oc8tOauHBV1vNypuMQUQxsbExAvat28
gU0ZcPCszt7NXymI2NjuGQiCyIAUmZoAQEILAGYyBAmUztJePm7bbZPzo1w7
3qaSYZ7+7tudG5ZFaLQADLBQOaqZmajEF2MpYokmJyZfqM3xwmwVihPEmKYJ
0VfMmBrjq0WLiZKMoUzvKonxNANxZ7l4TFqtbftqIJUg72sC4ApwZi42UNds
t+M9ZQN4EapW98jxLJ74z0pFKsbhdMyrY6a4xVqPhOCSE2ld60rmEjLQrtcy
3XxSh40qboJKJigIgC+dXykQCtjFglG2WBPMdj51iyAr6wCzEajWBApEupeo
6rKJSWdTUberOb4PU6cLjRaPwJLkHgAVC/rNAwZgi1ho0NKDzkIol6M/bxxM
Vll79rnwHEQcASvU/5WZLgm+0PpunRhPkTJeL4+AL3wfWOfDYlhL0cMk1I5h
kWAw+R7ph5MtqTH6rGQTgMYkmuiOAOuTagFRgSpBiFwSwpYcFs1ZdH9xAX61
6hgH+VSgf3bZg+nZbe9ZLr5koTToVLWvJS7WVTk25pcHDeyYhb1yzgxQhec3
dlNYmuEeONTiRMI5w3ZMBW6eVOC8R6j+50L9Atu0zlsyaQ6Zyd7zOg73mkjq
1lMOUgmsimPJSnVXPKkrAwkuNa0VM+IX34PSQzLWiIgbByuBfW5Dorc3BJhi
uopnJemmp6vIDe4M9A9BdfuB3DMMqBwD+WX3J8lwO0OMLo5I8Aolw22rJhrw
CJzDUHXlJDGzABomHTCGSxrR38dI4bekmumyoWllvW9njmvUcqtism9v2iWf
1CJuC6HAJjjdsAs8q19RLWM9MZGDMCImwhkdru6nM+YzUmuN/kG02NNpNO9C
7trWrqQsq5FjJZfH56IjS40RB93pY+dES/+LuSKcKEh7FUl3AA==
'''

installed_ui = '''\
QlpoOTFBWSZTWZigJZIAAdhfgFVQUGd/8D/jVY6/79/aQAJ8aNrABASmyQMI
aA0AaekAAAAA1JmSamaRoeoNAAAAAAABzTEwEaYEYRgAAACYRgJFNCExE9E1
Mj1NNGnqZMgANGjTajylULFIaBevBb3wwkEOfzkodKSCGUpJQ8aSFDClLEix
pAgsSBbECqhMY2FqSDtgCjU0Sn9PDfugvkbhNThBKjVENWb8eSjIjywREpKX
k5K7TYABQpSOphUhEbwBh5JkZLC0qUS1kcpzrQo6o2imvItZ9b9cCiJASlLO
5s8+2RrLQbBWIuFJnRXq29Lr1dIKSbaYbBpHStqYqxkE5dfT6W3yEppFWCz3
JEdeHFmjRrtvTLXmhLAwBKVGFK7CkQBAZziFABIBeXA7MyOGKQqfRdljkT89
6mxb62kiFcqEMjfQhAEAQuJkKYlZQiDBVMRS3E18jnxuaw3Vq1AO1q6RBVIB
8gQkF0ERQ3sfGRrWzXMpnIBjR+n1UqKVvB9zDtNJ4TJhmX3oFqOetiMAZ3mt
Wfg3Qd8tARuWAQoRQWSoNjQWjmE6/1dSuQcOBnD/a2pBJMGPfYtPdTHHhTXl
JVXFftNSUjT0LAwaKKfgT5blmNRvFzrlxtwLFESOPxLY0ZjjKFytgalzFxsD
IU5Ba0EuBMkUxCpqkmb2Vpyp6tAqGEYiz2wZA5wLRnG7EZwnx5LhlA4PNgi5
J3qi4kYIpoyCzmkY1CYV0qCzQpmaYyZivGTWksLS+8WpZizOzAVGsLz+Pc/0
DQ7kTnPcjs7Oz3iP+LuSKcKEhMUBLJA=
'''



pyui = bz2.decompress(b64decode(main_ui))
main = ui.load_view_str(pyui.decode('utf-8'))

pyui = bz2.decompress(b64decode(installed_ui))
installed = ui.load_view_str(pyui.decode('utf-8'))

config = ui.View()
add = ui.View()

class Tab (object):
  
  def __init__(self, sv, view=None):
    self.sv = sv

  def set(self, view):
    self.view = view
    return self
  
  def view(self, sender):
    sv = sender.superview
    pass
    
main.right_button_items = [
  ui.ButtonItem(
    image=ui.Image.named('typb:Cog'),
    action=Tab(main,config).view,
  ),
  ui.ButtonItem(
    image=ui.Image.named('typb:Plus'),
    action=Tab(main,add).view,
  ),
][::-1]

main.present('panel')
