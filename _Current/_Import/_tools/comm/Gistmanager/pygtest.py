import ui

from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.formatters import HtmlFormatter

code = 'print "Hello World"'
webcode = HtmlFormatter().get_style_defs('.highlight')
webcode += highlight(code, PythonLexer(), HtmlFormatter())

rcode = str(dir(ui.TableViewCell.text_label))
print(rcode)

from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.formatters import HtmlFormatter

webcode = highlight(code, PythonLexer(),
                         HtmlFormatter(full=True))
                     


view = ui.WebView()
view.load_html(webcode)
view.present('sheet')
