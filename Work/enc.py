# -*- coding: utf-8 -*-
import os
import pyaes

#print(os.getcwd())
"""
BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS) 
unpad = lambda s : s[0:-ord(s[-1])]
"""

key = 'We_Care_for_Our_Clients_Success!'.encode('utf8')
plaintext = 'And this is yet another example of an epic failure'

aes = pyaes.AESModeOfOperationCTR(key)


ciphertext = aes.encrypt(plaintext)

print(repr(ciphertext))

#iv = os.urandom(16)

aes = pyaes.AESModeOfOperationCTR(key)
decrypted = aes.decrypt(ciphertext)

# True
print(decrypted.decode('utf8') == plaintext)
