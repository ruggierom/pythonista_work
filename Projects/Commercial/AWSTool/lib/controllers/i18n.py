# -*- coding: utf-8 -*-
"""
Internationalization
"""
import os
import sys
from six import add_metaclass

## dev only
sys.path.insert(0,os.path.join('..'))
## dev only 

# internal
from utils import snake_case

# Lets try to set locale as a minimum
try:
    # On iOS We need to pull it from ObjC
    from objc_util import *  
    PYTHONISTA=True     
except ImportError:
    system_locale = 'en_CA'
    PYTHONISTA=False
    pass

#Locale Definitions



# Define metaclass
class Locale (type):
    pass

@add_metaclass(Locale)
class Global (object):
    
    def __init__(self):
        _global_defs = [
            'ISOCountryCodes',
            'ISOCurrencyCodes',
            'ISOLanguageCodes',
            'baseSystemLanguages',
            'commonISOCurrencyCodes',
            'deviceLanguage',
            'internetServicesRegion',
            'supportedLanguages',
            'preferredLanguages',
            'renderableLocaleLanguages',
            'renderableUILanguages',
            'pu_currentCharacterDirection',
            'px_currentCharacterDirection',
            'supportedLanguages',
            'supportedRegions',
            'supportsSecureCoding',
            'systemLanguages',
            'currentDeviceLanguageForFacebook',
        ]
        

@add_metaclass(Locale)
class BaseLocale (object):
    
    system_locale = None
    
    def __init__(self):
        # Try to get as much detail from the system as possible
        """
        # iterate NSLocale.currentLocale().*
        #currencySeparator
        #groupingsep
        """
        if PYTHONISTA:
            NSLocale = ObjCClass('NSLocale')
            
            system_locale = str(NSLocale.autoupdatingCurrentLocale().identifier())
            
            NSCurrentLocale = NSLocale.currentLocale()
            
            _get_locale = [
                'countryCode',
                'currencyCode', 
                'currencySymbol',
                'currencySeparator',
                'decimalSeparator', 
                'groupingSeparator',
                'usesMetricSystem',
                'alternateQuotationBeginDelimiter',
                'alternateQuotationEndDelimiter',
                'calendarIdentifier',
                'languageCode',
                'languageIdentifier',
                'localeIdentifier',
                'quotationBeginDelimiter',
                'quotationEndDelimiter',
            ]
            for attr in _get_locale:
                x = getattr(NSCurrentLocale,attr)
                setattr(self,snake_case(attr),x())
        else:
            pass
    pass
    
class Translation (type):
    pass

# Base classes
@add_metaclass(Translation)
class Base (Locale):
    
    def __init__(self):
        pass
    
    def compile(self):
        pass
        
    @classmethod
    def get(msg):
        pass
        
@add_metaclass(Translation)
class Translate (Base): 
    def __init__(self):
        pass

"""
Heirarchy:
    from lib.controller.i18n import Translate as _
"""

"""
locale list:
    ISOCountryCodes', 'ISOCurrencyCodes', 'ISOLanguageCodes', '_ICUdisplayNameForLanguage_capitalization_', '_addLikelySubtagsForLocaleIdentifier_', '_displayNameForNormalizedLanguage_context_displayLanguage_', '_filterLanguageList_forRegion_fromLanguages_', '_languageNameOverrides', '_languagesForRegionWithoutFiltering_', '_languagesToExemplarStrings', '_localeOverridesForLocaleWithCountryCode_', '_navigation_overrideDistanceUnitProvider', '_navigation_overrideDistanceUnit_', '_navigation_setNeedsUpdateOverrideDistanceUnit', '_normalizedLanguageIdentifierFromString_', '_parentLocaleIdentifierForIdentifier_', '_preferredMeasurementSystem', '_preferredTemperatureUnit', '_setPreferredMeasurementSystem_', '_setPreferredTemperatureUnit_', '_sf_isRTL', '_vk_indexForLocale_', 'addLikelySubtagsForLocaleIdentifier_', 'alloc', 'allocWithZone_', 'archivedPreferences', 'autoupdatingCurrentLocale', 'availableLocaleIdentifiers', 'baseLanguageFromLanguage_', 'baseSystemLanguages', 'cancelPreviousPerformRequestsWithTarget_', 'cancelPreviousPerformRequestsWithTarget_selector_object_', 'canonicalLanguageAndScriptCodeIdentifierForIdentifier_', 'canonicalLanguageIdentifierFromString_', 'canonicalLocaleIdentifierFromComponents_', 'canonicalLocaleIdentifierFromString_', 'canonicalLocaleIdentifierWithValidCalendarForComponents_', 'canonicalLocaleIdentifier_withNewLanguageIdentifier_', 'characterDirectionForLanguage_', 'commonISOCurrencyCodes', 'componentsFromLocaleIdentifier_', 'currentDeviceLanguageForFacebook', 'currentLocale', 'description', 'deviceLanguage', 'deviceLanguageIdentifier', 'deviceLanguageLocale', 'displayNameForLanguage_displayLanguage_context_', 'displayNameForRegion_displayLanguage_context_short_', 'enableDefaultKeyboardForPreferredLanguages', 'exemplarForLanguage_', 'instancesRespondToSelector_', 'internetServicesRegion', 'isSubclassOfClass_', 'languageArrayAfterSettingLanguage_fallback_toLanguageArray_', 'languageFromLanguage_byReplacingRegion_', 'lineDirectionForLanguage_', 'localeForBundleLanguage_', 'localeIdentifierFromComponents_', 'localeIdentifierFromWindowsLocaleCode_', 'localeLanguageMatchesPrimaryLanguage', 'localeWithLocaleIdentifier_', 'matchedLanguagesFromAvailableLanguages_forPreferredLanguages_', 'mostPreferredLanguageOf_forUsage_options_', 'mostPreferredLanguageOf_withPreferredLanguages_forUsage_options_', 'new', 'preferredLanguages', 'pu_currentCharacterDirection', 'px_currentCharacterDirection', 'registerPreferredLanguageForAddedKeyboardLanguage_', 'registerPreferredLanguage_usage_confidence_', 'renderableLanguagesFromList_', 'renderableLocaleLanguages', 'renderableUILanguages', 'resetTimeFormat', 'safari_localeIdentifiersForMostWidelyUsedLanguages', 'setArchivedPreferences_', 'setLanguageToPreferredLanguages_fallback_', 'setLocaleAfterLanguageChange_', 'setLocaleAfterRegionChange_', 'setLocaleAndResetTimeFormat_', 'setLocaleOnly_', 'setPreferredLanguages_', 'set_navigation_overrideDistanceUnitProvider_', 'shouldShowPreferredLanguages', 'string_withCapitalizedDisplayNamesForFirstLanguageIdentifier_secondLanguageIdentifier_thirdLanguageIdentifier_', 'superclass', 'supportedCJLanguageIdentifiers', 'supportedLanguages', 'supportedRegions', 'supportsSecureCoding', 'systemLanguages', 'systemLanguagesForRegion_', 'systemLocale', 'updateShouldShowPreferredLanguages_', 'validateLocale_', 'vs_systemLanaguage', 'windowsLocaleCodeFromLocaleIdentifier_']

Then with currentLocale()

calendarDirection
_cfTypeID
_copyDisplayNameForKey_value_
_isDeallocating
_navigation_distanceUsesMetricSystem
_navigation_distanceUsesMetricSystemIgnoringUserPreference_
_navigation_objectForKey_
_navigation_useYardsForShortDistances
_nullLocale
_prefs
_setNullLocale
_tryRetain
alternateQuotationBeginDelimiter
alternateQuotationEndDelimiter
calendarIdentifier
classForCoder
collationIdentifier
collatorIdentifier
copy
copyWithZone_
countryCode
currencyCode
currencySymbol
dealloc
debugDescription
decimalSeparator
displayNameForKey_value_
displayNameForLanguage_displayLanguage_context_
displayNameForRegion_displayLanguage_context_short_
encodeWithCoder_
exemplarCharacterSet
groupingSeparator
hash
identifier
init
initWithCoder_
initWithLocaleIdentifier_
isEqual_
languageCode
languageIdentifier
localeByChangingLanguageTo_
localeIdentifier
localizedStringForAlternateQuotationBeginDelimiter_
localizedStringForAlternateQuotationEndDelimiter_
localizedStringForCalendarIdentifier_
localizedStringForCollationIdentifier_
localizedStringForCollatorIdentifier_
localizedStringForCountryCode_
localizedStringForCurrencyCode_
localizedStringForCurrencySymbol_
localizedStringForDecimalSeparator_
localizedStringForGroupingSeparator_
localizedStringForLanguageCode_
localizedStringForLocaleIdentifier_
localizedStringForQuotationBeginDelimiter_
localizedStringForQuotationEndDelimiter_
localizedStringForScriptCode_
localizedStringForVariantCode_
mutableCopy
objectForKey_
performSelectorInBackground_withObject_
performSelectorOnMainThread_withObject_waitUntilDone_
performSelector_withObject_afterDelay_
quotationBeginDelimiter
quotationEndDelimiter
release
retain
retainCount
scriptCode
usesMetricSystem
variantCode

"""
