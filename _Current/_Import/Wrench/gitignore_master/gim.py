# -*- coding: utf-8 -*-
import os
import sys
from dulwich.repo import Repo
from dulwich.client import get_transport_and_path
from six import BytesIO
from shutil import rmtree
import ui


v = ui.TextView()
v.flex = 'WH'
v.present()

class Splewty (BytesIO):
    
    def __init__(self):
        super().__init__()
        self.cline = 0
        self.output = ''
        self.current_line = ''
        
        if os.path.exists(os.path.join(os.getcwd(),'twerp')):
            rmtree(os.path.join(os.getcwd(),'twerp'))
    
    def write(self, stream):
        data = stream.decode('utf-8')
        self.current_line = data
        if not b'\r' in stream:
            self.output += self.current_line
            v.text = self.output
        else:
            v.text = self.output
            v.text += self.current_line

plewt = Splewty()
plewt.write(b'Cloning GIT Repository...\n')

repo = 'https://github.com/github/gitignore'
path = '{}/twerp'.format(os.getcwd())

from dulwich import porcelain
porcelain.clone(repo, path, errstream=plewt)

# print(plewt.getvalue())
