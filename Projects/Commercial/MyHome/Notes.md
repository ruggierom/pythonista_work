# MyHome

MyHome is a Software-As-A-Service (SAAS) based web portal which allows property management companies to better manage the client communication and work order requests.

Further to that, it provides detailed insights for corporate management to assess the tenant management of the various teams at Briarlane.

## How does it work?

By keeping digital records of all work order requests, accompanied by custom mobile applications to be used by office and labour staff, Briarlane is able to see a snapshot of work requests, their time to completion, as well as allowing tenants to provide feedback on the services rendered.

MyHome also features integrated tenant communications which allows property managers to send communications and updates during emergency work proceedings, as well as tracking when the tenants have acknowledged the communication.

The software will provide KPI metrics for all staff associated with maintenance of the properties, and track problematic items 
