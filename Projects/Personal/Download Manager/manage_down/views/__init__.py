# -*- coding: utf-8 -*-

import ui
import io
import math

from six import text_type
from PIL import Image, ImageFilter

from .home import Home
from .util import uiHelper

class ColorTool (object):
  """
  Convert colors to/from PIL/HTML/Tuple

  Taken From: http://code.activestate.com/recipes/266466-html-colors-tofrom-rgb-tuples/
  """
  @staticmethod
  def RGBToHTMLColor(rgb_tuple):
    """ convert an (R, G, B) tuple to #RRGGBB """
    hexcolor = '#%02x%02x%02x' % rgb_tuple
    # that's it! '%02x' means zero-padded, 2-digit hex values
    return hexcolor

  @staticmethod
  def HTMLColorToRGB(colorstring):
    """ convert #RRGGBB to an (R, G, B) tuple """
    colorstring = colorstring.strip()
    if colorstring[0] == '#': colorstring = colorstring[1:]
    if len(colorstring) != 6:
      raise ValueError("input #%s is not in #RRGGBB format" % colorstring)
    r, g, b = colorstring[:2], colorstring[2:4], colorstring[4:]
    r, g, b = [int(n, 16) for n in (r, g, b)]
    return (r, g, b)

  @staticmethod
  def HTMLColorToPILColor(colorstring):
    """ converts #RRGGBB to PIL-compatible integers"""
    colorstring = colorstring.strip()
    while colorstring[0] == '#': colorstring = colorstring[1:]
    # get bytes in reverse order to deal with PIL quirk
    colorstring = colorstring[-2:] + colorstring[2:4] + colorstring[:2]
    # finally, make it numeric
    color = int(colorstring, 16)
    return color

  @staticmethod
  def PILColorToRGB(pil_color):
    """ convert a PIL-compatible integer into an (r, g, b) tuple """
    hexstr = '%06x' % pil_color
    # reverse byte order
    r, g, b = hexstr[4:], hexstr[2:4], hexstr[:2]
    r, g, b = [int(n, 16) for n in (r, g, b)]
    return (r, g, b)

  @staticmethod
  def PILColorToHTMLColor(pil_integer):
    return RGBToHTMLColor(PILColorToRGB(pil_integer))

  @staticmethod
  def RGBToPILColor(rgb_tuple):
    return HTMLColorToPILColor(RGBToHTMLColor(rgb_tuple))

  @staticmethod
  def getRGBTupleFromImg(file_obj, coords=(0,0)):
    """
    Extract an #RRGGBB color string from given pixel coordinates
    in the given file-like object.
    """
    pil_img = Image.open(file_obj)
    pil_img = pil_img.convert('RGB')
    rgb = pil_img.getpixel(coords)
    return rgb

class uiTools (object):
  
  @staticmethod
  def img2ui(ip):
    with io.BytesIO() as bIO:
      ip.save(bIO, ip.format)
      return ui.Image.from_data(bIO.getvalue())
      
class ProgressBarStyle (object):
  
  def __init__(self):
    use_gradient = True
    bar_color = (0,)
    
  def gradient(self, enable=True):
    self.use_gradient = enable
    return self
    
  def color(self, color=None, **kwargs):
    if color is not None:
      if isinstance(color, text_type):
        if color.startswith('#') and len(color) in [4,7]:
          # Valid HTML or Short HTML
          pass
        elif len(color) in [3,6]:
          # Valid without hash, add hash          
          pass
        
          
        
    return self
    
  def bar_html(self, color):
    self.bar_color = ColorTool.HTMLColorToPILColor(color)
  
  def bar_rbg(self, *args):
    """
    Set colour using RGB.
    
    Examples:
      * ProgressBarStyle().rgb(255,2555,255)
      * 
      * or
      * 
      * color=(255,255,255)
      * ProgressBarStyle().rgb(color)
    """
    assert (len(args) in (1,3,)), 'Invalid use of method see docs.'
    if len(args) == 1:
      color = args[0]
      assert isinstance(color, (tuple, list)), 'Single argument rgb must be tuple or list only'
      self.bar_color = ColorTool.RGBToPILColor(color)
    elif len(args) == 3:
      self.bar_color = ColorTool.RGBToPILColor(args)
    else:
      raise AttributeError('Invalid args. Accepts single arg tuple, or triple arg integer')

  def bar_pil(*args):
    assert (len(args) in (1,3,)), 'Invalid use of method see docs.'
    if len(args) == 1:
      color = args[0]
      assert isinstance(color, (tuple, list)), 'Single argument rgb must be tuple or list only'
      self.bar_color = color
    elif len(args) == 3:
      self.bar_color = ColorTool.RGBToPILColor(args)
    else:
      raise AttributeError('Invalid args. Accepts single arg tuple, or triple arg integer')      


class ProgressBar (ui.View):
  
  def __init__(self):
    bar = ui.Label(title='')
    bar.background_color = '#00F'
    bar.frame = (0,0,0, 40)
    bar.flex = 'WH'
    self.add_subview(bar)
  
  

class ProgressBarView (ui.View):
  """
  Create a Progress Bar 
  
  Gradient taken from: https://stackoverflow.com/questions/30608035/plot-gradients-using-pil-in-python
  """

  def __init__(self, width=None, height=None, **kwargs):

    if width is None:
      if self.superview is None:
        width = 100
        height = 42
      else:
        width = self.superview.width
        height = self.superview.height
        
    self.flex = 'W'
    self.width = width
    self.height = height
    self.frame = (0, 0, width, height)
    self.percent = 0
    
    # Border size
    self.border_width = kwargs.pop('border_width', 0.5)
    
    # Corner Radius
    self.corner_radius = kwargs.pop('corner_radius', 4)

    imgsize = (int(width), int(height))
    image = Image.new('RGB', imgsize) #Create the image
    image.format = 'JPEG'

    # Use a gradient?
    gradient = kwargs.pop('gradient', None)
    
    # color (solid)
    
    
    
    if isinstance(gradient, (list, tuple,)):
      colors = gradient
    elif gradient is None:
      inner_color = [255, 149, 0] #Color at the center
      outer_color = [255, 79, 0] #Color at the corners
      colors = [inner_color, outer_color]
      
    if gradient is False:
      image.paste( (200,200,200), [0,0,image.size[0],image.size[1]])
    else:
      image = self._make_gradient(image, imgsize, colors)
      

    img = uiTools.img2ui(image)
    i = ui.ImageView()
    i.image = img
    i.frame = self.frame
    i.flex = 'W'
    self.i = i
    self.i = ProgressBar()
    self.add_subview(self.i)
    self.i.width = 0
    
    for key, value in kwargs.items():
        try:
            setattr(self, key, value)
        except:
            pass
    
    
  def draw(self):
    self.flex = ''
    pass
    
  def layout(self):
    #self.set_progress(self.percent)
    self.flex = 'W'
    pass


  def _make_gradient(self, image, imgsize, colors):
    """
    Create a gradient image
    """
    
    # TODO: Finish converting pascal case to snake case
    
    inner_color, outer_color = colors
    
    for y in range(imgsize[1]):
      for x in range(imgsize[0]):
        #Find the distance to the center
        distanceToCenter = math.sqrt(
          (x - imgsize[0]/2) ** 2 + (y - imgsize[1]/2) ** 2
        )

        #Make it on a scale from 0 to 1
        distanceToCenter = float(distanceToCenter) / (math.sqrt(2) * imgsize[0]/2)

        #Calculate r, g, and b values
        r = outer_color[0] * distanceToCenter + inner_color[0] * (1 - distanceToCenter)
        g = outer_color[1] * distanceToCenter + inner_color[1] * (1 - distanceToCenter)
        b = outer_color[2] * distanceToCenter + inner_color[2] * (1 - distanceToCenter)

        #Place the pixel
        image.putpixel((x, y), (int(r), int(g), int(b)))
        
    return image

  def do_progress(self, part, total):
    return self.set_progress(int(100 * float(part)/float(total)))

  def set_progress(self, percent):
    self.percent = percent
    self.i.width = (percent * self.width) / 100



"""
if __name__ == '__main__':
  root = ui.View()
  root.background_color = '#fff'
  root.flex = 'WH'
  root.name = 'ProgressBar Example'
  p = uiProgressBarView(350,30)
  p.y = 40
  p.x=15
  p.do_progress(34, 100)
  #p.i.width = 0
  root.add_subview(p)
  root.present()


    def mkp():
        p.i.width = 350
    #ui.animate(mkp, 4)
    #p.progress(70)
    pass
"""

