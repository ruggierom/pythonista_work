# -*- coding: utf-8 -*-

from bottle import Bottle, run
from bottle_jwt import JWTProviderPlugin
from bottle_jwt import jwt_auth_required as authorize

import os

from coffeedb import CoffeeTrip

app = Bottle()

server_secret = 'SX[gx!-NJ3wyfW"+ji!oCiTS7.?u5NL_E_;F}vB'

class AuthBackend(object):
    """Implementing an auth backend class with at least two methods.
    """
    
    def authenticate_user(self, username, password):
        """Authenticate User by username and password.
        Returns:
            A dict representing User Record or None.
        """
        if username == self.user['username'] and password == self.user['password']:
            return self.user
        return None

    def get_user(self, user_id):
        """Retrieve User By ID.
        Returns:
            A dict representing User Record or None.
        """
        return CoffeeTrip.user(user_id)        

provider_plugin = JWTProviderPlugin(
    keyword='jwt',
    auth_endpoint='/sprinkles',
    backend=AuthBackend(),
    fields=('username', 'password'),
    secret=server_secret,
    ttl=30
)

app.install(provider_plugin)

@app.get('/')
def home():
  pass

# users

@app.get('/user')
def user():
  """
  User List
  """
  
