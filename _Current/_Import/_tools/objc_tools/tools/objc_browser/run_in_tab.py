from objc_util import on_main_thread

@on_main_thread
def run():
    from objc_browser import run
    run()
    
run()
