import paramiko
from scp import SCPClient
import keychain
import sys
import os
import editor
from six import text_type

service = 'remotefile'

def createSSHClient(server, port, user, password):
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(server, port, user, password)
    return client
    
def makedirs(*args):
    try:
        _dir = os.path.join('./temp',*args)
        os.makedirs(_dir)
        return _dir
    except Exception as e:
        print('Failure: {0}'.format(e))
        return _dir

def xferfile(op, path, filename, **kwargs):
    user = kwargs.pop('user','root')
    server = kwargs.pop('server','iguardfile.com')
    port = kwargs.pop('port',22)
    local_path = makedirs(service)
    password = keychain.get_password(service, user)
    ssh = createSSHClient(server, port, user, password)
    scp = SCPClient(ssh.get_transport())
    
    remote_file = os.path.normpath(os.path.join(path,filename))
    local_file = os.path.normpath(os.path.join(local_path,filename))
    
    if op == 'get':
        try:
            scp.get(remote_file, 
                    local_file, 
                    preserve_times=True)
            
            return local_file
        except Exception as e:
            print(e)
            sys.exit(1)
    elif op == 'put':
        try:
            scp.get(local_file, 
                    remote_file, 
                    preserve_times=True)
            
            return True
        except Exception as e:
            print(e)
            sys.exit(1)
    """
    def get(self, remote_path, local_path='',
            recursive=False, preserve_times=False):
    
    def put(self, files, remote_path=b'.',
            recursive=False, preserve_times=False):
        
        
    """

def editfile(remote_file, **kwargs):
    assert isinstance(remote_file, text_type)
    filename = os.path.basename(remote_file)
    path = remote_file.rstrip(filename)
    z = xferfile('get',path, filename)
    print(z)
    if z:
        editor.open_file(z)
    pass
    
rfile = 'awsprice/getawspricelist.py'

editfile(rfile)
