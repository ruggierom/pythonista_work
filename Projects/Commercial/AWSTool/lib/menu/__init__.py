# -*- coding: utf-8 -*-
"""
All menus will be defined here.

Each menu is subclasses from the 'Base' Class / Base Menu.
"""
from collections import OrderedDict, deque
import six
from six import with_metaclass, add_metaclass


## define a namedtuple to

class Menu (type):
    pass

@add_metaclass(Menu)
class Base (object):
    """
    Base Menu definition.

    In order to keep an "iOS" look and feel, powerful UITableView will be used.
    """
    def __init__(self):
        """
        Class Initializer
        """
        self.tableview = None
        self.reload_disabled = True
        self.delete_enabled = False
        self.move_enabled = False # For now

        self.action = None
        self.edit_action = None
        self.accessory_action = None

        self.tapped_accessory_row = -1
        self.selected_row = -1

    @property
    def items(self):
        return self._items

    @items.setter
    def items(self, value):
        self._items = ListDataSourceList(value, self)
        self.reload()


    # Data source methods
    def tableview_number_of_sections(self, tableview):
        # Return the number of sections (defaults to 1)
        return 1

    def tableview_number_of_rows(self, tableview, section):
        # Return the number of rows in the section
        return 0

    def tableview_cell_for_row(self, tableview, section, row):
        # Create and return a cell for the given section/row
        cell = ui.TableViewCell()
        cell.text_label.text = 'Foo Bar'
        return cell

    def tableview_title_for_header(self, tableview, section):
        # Return a title for the given section.
        # If this is not implemented, no section headers will be shown.
        return 'Some Section'

    def tableview_can_delete(self, tableview, section, row):
        # Return True if the user should be able to delete the given row.
        return True

    def tableview_can_move(self, tableview, section, row):
        # Return True if a reordering control should be shown for the given row (in editing mode).
        return True

    def tableview_delete(self, tableview, section, row):
        # Called when the user confirms deletion of the given row.
        pass

    def tableview_move_row(self, tableview, from_section, from_row, to_section, to_row):
        # Called when the user moves a row with the reordering control (in editing mode).
        pass

    # TableView Delegate functionality
    def tableview_did_select(self, tableview, section, row):
        # Called when a row was selected.
        pass

    def tableview_did_deselect(self, tableview, section, row):
        # Called when a row was de-selected (in multiple selection mode).
        pass

    def tableview_title_for_delete_button(self, tableview, section, row):
        # Return the title for the 'swipe-to-***' button.
        return 'Hide'

@add_metaclass(Menu)
class SideMenu (Base):
    pass
    
# ugh may not use
class MenuDataSourceList (list):
    def __init__(self, seq, datasource):
        list.__init__(self, seq)
        self.datasource = datasource

    def append(self, item):
        list.append(self, item)
        self.datasource.reload()

    def __setitem__(self, key, value):
        list.__setitem__(self, key, value)
        self.datasource.reload()

    def __delitem__(self, key):
        list.__delitem__(self, key)
        self.datasource.reload()

    def __setslice__(self, i, j, seq):
        list.__setslice__(self, i, j, seq)
        self.datasource.reload()

    def __delslice__(self, i, j):
        list.__delslice__(self, i, j)
        self.datasource.reload()

# for reference

class ListDataSource (object):
    def __init__(self, items=None):
        self.tableview = None
        self.reload_disabled = False
        self.delete_enabled = True
        self.move_enabled = False

        self.action = None
        self.edit_action = None
        self.accessory_action = None

        self.tapped_accessory_row = -1
        self.selected_row = -1

        if items is not None:
            self.items = items
        else:
            self.items = ListDataSourceList([])
        self.text_color = None
        self.highlight_color = None
        self.font = None
        self.number_of_lines = 1

    def reload(self):
        if self.tableview and not self.reload_disabled:
            self.tableview.reload()

    @property
    def items(self):
        return self._items

    @items.setter
    def items(self, value):
        self._items = ListDataSourceList(value, self)
        self.reload()

    def tableview_number_of_sections(self, tv):
        self.tableview = tv
        return 1

    def tableview_number_of_rows(self, tv, section):
        return len(self.items)

    def tableview_can_delete(self, tv, section, row):
        return self.delete_enabled

    def tableview_can_move(self, tv, section, row):
        return self.move_enabled

    def tableview_accessory_button_tapped(self, tv, section, row):
        self.tapped_accessory_row = row
        if self.accessory_action:
            self.accessory_action(self)

    def tableview_did_select(self, tv, section, row):
        self.selected_row = row
        if self.action:
            self.action(self)

    def tableview_move_row(self, tv, from_section, from_row, to_section, to_row):
        if from_row == to_row:
            return
        moved_item = self.items[from_row]
        self.reload_disabled = True
        del self.items[from_row]
        self.items[to_row:to_row] = [moved_item]
        self.reload_disabled = False
        if self.edit_action:
            self.edit_action(self)

    def tableview_delete(self, tv, section, row):
        self.reload_disabled = True
        del self.items[row]
        self.reload_disabled = False
        tv.delete_rows([row])
        if self.edit_action:
            self.edit_action(self)

    def tableview_cell_for_row(self, tv, section, row):
        item = self.items[row]
        cell = TableViewCell()
        cell.text_label.number_of_lines = self.number_of_lines
        if isinstance(item, dict):
            cell.text_label.text = item.get('title', '')
            img = item.get('image', None)
            if img:
                if isinstance(img, basestring):
                    cell.image_view.image = Image.named(img)
                elif isinstance(img, Image):
                    cell.image_view.image = img
            accessory = item.get('accessory_type', 'none')
            cell.accessory_type = accessory
        else:
            cell.text_label.text = str(item)
        if self.text_color:
            cell.text_label.text_color = self.text_color
        if self.highlight_color:
            bg_view = View(background_color=self.highlight_color)
            cell.selected_background_view = bg_view
        if self.font:
            cell.text_label.font = self.font
        return cell

