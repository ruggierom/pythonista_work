# -*- coding: utf-8 -*-
"""
Snippet Manager - Manage local and remote snippets

Features:
    * GitHub Snippets (CRUD your own snippets, browse starred repos, browse starred snippets)
"""

import ui

class SnippetManagerTheme (object):
    """
    Theme Base Class - Base the themes off of this class 
    """
        
    def __init__(self, **kwargs):
        
        self.present_attributes = dict(
            style='default',
            animated=True,
            #popover_location=None,
            hide_title_bar=False,
            title_bar_color=None,
            title_color=None,
            orientations=('portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right',),
            hide_close_button=False,        
        )
        
        self.alpha = 1.0
        self.background_color = None
        self.border_color = None
        self.border_width = 0        
        
        for k, v in kwargs.items():
            try:
                setattr(self, k, v)
            except AttributeError:
                pass
    
    def set_theme(self, view):
        assert isinstance(view, ui.View)
        for k, v in vars(self).items():
            try:
                setattr(view, k, v)
            except AttributeError:
                pass

class DefaultTheme (SnippetManagerTheme):
    
    def __init__(self):
        super(DefaultTheme, self).__init__()
        self.background_color = '#eee'
        self.present_attributes['style'] = 'sheet'
        self.present_attributes['title_bar_color'] = '#fff'


# I really should finish that tab bar thing that I started

class SnippetManager (ui.View):
    """
    Main Class Implementation for the Snippet Manager
    """
    
    def __init__(self):
        """
        Class Initializer
        """
        
        # Setup base view
        
        w, h = ui.get_window_size()    # Use window_size versus screen_size for split apps
        self.frame = (0, 0, w, h)
        self.flex = 'WH'
        theme = DefaultTheme()
        theme.set_theme(self)
        
        self.name = 'Snippet Manager'
        
        

main = SnippetManager()
main.present(**main.present_attributes)
