import ui
from dialogs import _FormContainerView
from base64 import b64decode

class Checkbox (ui.View):
    
    checkboxes = {
        'checked': [
        'iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYA',
        'AACqaXHeAAADoUlEQVR42u2bO4hTQRSG4xvf', 
        'iA+00EICoiFx8yYGJDaCBATFKGIjVotbLIKN', 
        'hbqoWFhYWPjARrFRtFqsRAvBQhDZtRAFQQQx', 
        'rroLEcVH3Gz8f9C9IUySewibufckgcMuyZy5', 
        '5//m3Jm5c2cC1Wq1oUUikTXxeDwfi8VOJxKJ', 
        'YfxfhFU9bkXGypgZOzU002j8Eo7zUMkQ/pZZ', 
        'qc+tTBjU5ApAMpnsQ+FROiuz0Wg0urUpABQ6', 
        '6rS6SitToxEAW96F+Aqs5HGrtIJQmwnT93yT', 
        'tP8IGwSgbehQFrO8l40xMlbGzNgb3Q7UPA2A', 
        'HZ6pIL6/nUqlVrKMH42xU0MDbUMs83+oK5vE', 
        's4AGawChTO1M/7wp7U0t7+dMaHA75AMcIw0/', 
        'DNJRk1FTvU5qZ3oM1//ATkQbAGqq10ntAcP0', 
        'tuKH3h5D2RbEehatuDeTySx0MzoYhshiwJD+', 
        'Ja+Lh+j9iPNXTcyv0JqpVn7UVq/XbwCYyusN', 
        'QmiTAHMuFArN1wyA8T5oMdMbSafTyzQCYKz9', 
        'JtGGzu2aNgBM/Y2I7ZvLh57nqgDgMxut+til', 
        'eI7vV1UBgPhjgkfeEjtKLQAofhNi+iFo/cNq', 
        'RoFCoTAH8Tx1K54zO03zAMZ2QpD640j9tVoA', 
        'MK4w7Lcg9Q/omAk6K1Qjgta/Qz9NAM4IxI/x', 
        'eV8NAKRyHJ8/go5vN/1UAAgGgwtw/ZeC1r9B', 
        'PzUA0JoXBOLfw5brAOCs2FQEAHbSTwMAxrAI', 
        '9kYg/gr9NAG4JBD/FoseS+wCcOboDPwU19fb', 
        'EL8DNuVS/BTWArfTzyYAit9XN1R9hoWlAWWz', 
        '2aXweyeY7V2kny0AZvGOfUGAEWFA1wWp/5qr', 
        'vx0BIBYvh8C6dgnET8LS9LMFgAFHXc7Qxp3X', 
        'z2YLh8MrUO6DAMB5+tkEwHI3jcHJIRDmLUFd', 
        'L7jU7S8AzvN5n2Guv0eyo8NQh/1bQGATtQKQ', 
        'Favx3SeB/0mjGLudoBwC4f1r/XuCp7xnuVxu', 
        'rkcAtA+Br60E5X+i/GaBOHsToRmy47yOVQAW', 
        'ITzhixDrACxB+I7UD7Ju6wBsQID4AdbpCQCd', 
        'hoA6HiL1Z3kGQIchfIVtYD0+BNA+BPgdob8/', 
        'AbQP4T79dACQQ5iArVMCQA4BZQ6yrA4Acgh3', 
        'WcY2gLY2SgpWfsbqrvFoZjZjyjdKdmKrLCGv', 
        'gvXDLmOyc4ivwLyyVba3Wbrrt8t3/YGJrj8y', 
        '0zs01Ts21zs42Ts62zs83cXH5/8CllkxCviA', 
        'RtIAAAAASUVORK5CYII='],
        
        'unchecked': [
        'iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMA', 
        'AACdt4HsAAAAQlBMVEX///8zMzMzMzMyMjIz', 
        'MzMuLi4zMzMuLi4zMzM2NjYzMzMzMzM1NTUz',
        'MzM0NDQzMzM0NDQ0NDQxMTEzMzM1NTUzMzM6',
        '7LBrAAAAFXRSTlMA8Pp/SxbmCwUT0G86/bLn',
        'qIAvajBGafPqAAAAwElEQVR4Xu2XSw6DMAxE',
        'h/whaYBC7n/Vyi5IXZJ4V/ltsponR954cOGd',
        'raE9JFTrPH5Ju2mdGJtwg3NpAyxvXBymDWEO',
        'MOedf00Ped0GniF9519ziXhILHnl0JIA7Jzf',
        'ZnQxbxzbAW84j27YYDwczz+jm5l/4WDpyRgg',
        'U9Ki0lMwQKFkRaD9RQwQaZsBpJkwxETZvxSo',
        'QAUqUIEKVKACFahAfGiKT13xsS0+98WFQ1p5',
        'xKVLWvvExVNefeXlW1z/P5ukTB0BVFA1AAAA',
        'AElFTkSuQmCC']
    }

    def __init__(self, key, value=False, text=None, imgsize=26, **kwargs):
        
        def b64i(data):
            if isinstance(data, list):
                data = ''.join(data)
                
            d = b64decode(data)
            return ui.Image.from_data(d)
            
        # Strictly to make it fit in Pythonista
        def c(which):
            return self.checkboxes[which]
        
        def getopt(opt):
            pass
        
        unchecked = b64i(c('unchecked'))
        checked = b64i(c('checked'))
        
        # Checkbox Options
        
        
        self.value = value
        button = ui.Button()
        button.width = imgsize
        button.height = imgsize
        button.value = value
        button.flex = 'TBR'
        button.name = key
        button.tint_color = '#000'
        button.text = text
        
        state = {True: checked, False: unchecked}
        button.image = state[value]
        
        def chgbtn(sender):
            self.value = not self.value
            sender.image = state[self.value]
        
        button.action = chgbtn

        
        self.add_subview(button)
        
        if text:
            t = ui.Label()
            t.text = text
            tw = ui.measure_string(t.text, font=t.font)
            t.flex = 'W'
            bw = (imgsize + tw[0] + 5) 
            t.x = imgsize + 5
            t.height = imgsize - round(imgsize/10)
            self.add_subview(t)
            self.frame = (5,5,bw,imgsize)
            self.height = 26

if __name__ == '__main__':
  t = ui.View()
  t.background_color = '#fff'
  t.flex = 'WH'
  
  c = Checkbox('test', text='good times')
  t.add_subview(c)
  t.present()
