# -*- coding: utf-8 -*-
import sys
import _ui
import _appex
from io import StringIO
import importlib

# modules to copy
import editor
import canvas
import scene
import cb
import clipboard
import console
import contacts
import keychain
import linguistictagger
import location
import motion
import notification
import objc_util
import photos
import reminders
import sound
import speech
import twitter


def printhelp(module):
  _old = sys.stdout
  sys.stdout = _reout = StringIO()
  help(module)

  with open('{}.txt'.format(module.__name__),'w') as f:
    f.write(_reout.getvalue())
  
  sys.stdout = _old

def copymod(modules):
  if isinstance(modules, str):
    modules = [modules]
  
  for module in modules:
    data = None
    try:
      file = module.__file__
      name = module.__name__
      with open(file, 'r') as f:
        data = f.read()
      
      with open('{}_.py'.format(name),'w') as f:
        f.write(data)
      
    except Exception as e:
      print('Module Open issue: {}'.format(e))
      pass
    
    # try the help thing
    
    try:
      name = module.__name__
      _name = '_{}'.format(name)
      _mod = importlib.import_module(_name)
      printhelp(_mod)
    except Exception as e:
      print('Underscore export issue: {}'.format(e))
    


mod_list = [
  editor,
  scene,
  canvas,
  cb,
  clipboard,
  console,
  contacts,
  keychain,
  linguistictagger,
  location,
  motion,
  notification,
  objc_util,
  photos,
  reminders,
  sound,
  speech,
  twitter,
]


copymod(mod_list)
#printhelp(_appex)
