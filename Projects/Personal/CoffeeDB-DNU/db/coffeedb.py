# -*- coding: utf-8 -*-

import datetime
from peewee import *
import os

# Coffee Users

database = SqliteDatabase('coffee.db', threadlocals=True)


class Coffee (Model):

    class Meta:
        database = database

class User (Coffee):
    id = PrimaryKeyField()
    name = CharField()
    active = BooleanField(default=True)


class Restaurant (Coffee):
    id = PrimaryKeyField()
    name = CharField()
    active = BooleanField(default=True)


class Purchase (Coffee):
    id = PrimaryKeyField()
    buyer_id = IntegerField()
    restaurant_id = IntegerField()
    total_price = FloatField(null=True)
    purchase_date = DateTimeField(default=datetime.datetime.now)


class Attendance (Coffee):
    id = PrimaryKeyField()
    purchase_id = IntegerField()
    user_id = IntegerField()


class CoffeeTrip:

    def __init__(self):
        self.model = None
        self.columns = None
        self.join = False
        self.join_model = None

        if not os.path.exists('coffee.db'):
            self.create_tables()

    @staticmethod
    def create_tables():
        try:
            database.connect()
            database.create_tables([User, Restaurant, Purchase, Attendance])
        except OperationalError:
            pass
        finally:
            try:
                database.close()
            except:
                pass

    @property
    def users(self):
        self.model = User
        self.columns = ('id','name',)
        return self

    @property
    def restaurants(self):
        self.model = Restaurant
        self.columns = ('id','name',)
        return self

    #  Users

    @staticmethod
    def new_user(user_name):
        with database.transaction():
            user = User.create(name=user_name)
            return user.id

    @staticmethod
    def all_users():
        with database.transaction():
            user_list = []
            for user in User.select():
                user_list.append({
                    user.id : user.name
                })

        return user_list

    @staticmethod
    def deactivate_user(user_id):
        with database.transaction():
            try:
                update = User.update(active=False).where(id==user_id)
                update.execute()
                return True
            except Exception as e:
                print(e)
                return False

    @staticmethod
    def activate_user(user_id):
        with database.transaction():
            try:
                update = User.update(active=True).where(id==user_id)
                update.execute()
                return True
            except Exception as e:
                print(e)
                return False

    @staticmethod
    def delete_user(user_id):
        with database.transaction():
            try:
                update = User.get(id=user_id)
                update.delete_instance()
                return True
            except Exception as e:
                print(e)
                return False

    @staticmethod
    def new_restaurant(restaurant_name):
        with database.transaction():
            restaurant = Restaurant.create(name=restaurant_name)
            return restaurant.id


    @staticmethod
    def activate_restaurant(restaurant_id):
        with database.transaction():
            try:
                update = Restaurant.update(active=True).where(id==restaurant_id)
                update.execute()
                return True
            except Exception as e:
                print(e)
                return False
    @staticmethod
    def deactivate_restaurant(restaurant_id):
        with database.transaction():
            try:
                update = Restaurant.update(active=False).where(id==restaurant_id)
                update.execute()
                return True
            except Exception as e:
                print(e)
                return False

    @staticmethod
    def new_purchase(buyer_id, restaurant_id, attendees=None, price=None):
        with database.transaction():
            purchase = Purchase.create(buyer_id=buyer_id, restaurant_id=restaurant_id, total_price=price)
            pid = purchase.id
            if isinstance(attendees, list):
                for person in attendees:
                    _ = Attendance.create(purchase_id=pid, user_id=int(person))

            return pid

    @staticmethod
    def delete_purchase(purchase_id):
        with database.transaction():
            purchase = Purchase.get(id=purchase_id)
            return purchase.delete_instance()

    @staticmethod
    def delete_attendee(purchase_id, user_id):
        with database.transaction():
            purchase = Attendance.get(purchase_id=purchase_id, user_id=user_id)
            return purchase.delete_instance()
