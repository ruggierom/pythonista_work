# -*- coding: utf-8 -*-
import base64
import bz2
import ui
import textwrap
import clipboard
from PIL import Image

def img2b64(image, output='image'):
    with open(image, 'rb') as fimg:
        encoded = base64.b64encode(bz2.compress(fimg.read())).decode('utf-8')
        wrapped = b'\n'.join(textwrap.wrap(encoded, 60))
        code = """\
data = '''data:image/png;base64,%s
'''
import ui
from base64 import b64decode
image = ui.Image.from_data(data) """ % (encoded,)
        dink = base64.b64decode(bz2.decompress(wrapped))
        print(len(wrapped))
        #derp = "data:image/png;base64,{}".format(dink)
        #print(derp)
        #clipboard.set(code)
        #print(code)
        zz = ui.Image.from_data(dink)
        zz.show()
        

#x = img2b64('logo.png')

def resize_image(image):
    basewidth = 308
    img = Image.open(image)
    wpercent = (basewidth/float(img.size[0]))
    hsize = int((float(img.size[1])*float(wpercent)))
    img = img.resize((basewidth,hsize), Image.ANTIALIAS)
    img.save('logo_sized.png','PNG') 

## no bz2: 24054
resize_image('logo.png')
