# -*- coding: utf-8 -*-

from tabview import *
import ui

if __name__ == '__main__':
  """
  Execute if run directly
  """
    home = ui.load_view('views/home')
    tabs = [home]
    main = TabView(tabs=tabs, height=65)
    main.present()
