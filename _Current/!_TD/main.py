# -*- coding: utf-8 -*-

from abc import (
    ABCMeta, 
    abstractclassmethod, 
    abstractmethod,
    abstractproperty,
)

from six import add_metaclass
"""
abstractclassmethod
abstractmethod
abstractproperty
abstractstaticmethod

https://github.com/bcdev/jpy
"""


class TDAzure (object):
    
    def __init__(self):
        self.client = Azure.Client('innov')
        print(self.client)
    pass


class AzureClientMeta (type):
    pass
    
    
class Azure (object):
    
    @add_metaclass(AzureClientMeta)
    class Client (object):
        
        def __init__(self, environment=None):
            #print(dir(self))
            self.onboarding = self._load_onboarding()
            #print('init')
            #print(self.onboarding)
        
        def __enter__(self):
            return self
            
        def __exit__(self, type, value, tb):
            pass
             
        def _load_onboarding(self, *args, **kwargs):
            return None
            

z = TDAzure()
print(z)
#print('Context')
with Azure.Client('innov') as c:
    print(c)
