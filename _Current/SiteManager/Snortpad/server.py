#!python2

try:
    # Python 2.x
    from SocketServer import ThreadingMixIn
    from SimpleHTTPServer import SimpleHTTPRequestHandler
    from BaseHTTPServer import HTTPServer
except ImportError:
    # Python 3.x
    from socketserver import ThreadingMixIn
    from http.server import SimpleHTTPRequestHandler, HTTPServer

class ThreadingSimpleServer(ThreadingMixIn, HTTPServer):
    pass

import sys
import os

class PrivateHTTPserver (object):
    
    def __init__(self, **kwargs):
        self.interface = kwargs.pop('interface', '0.0.0.0')
        self.port = kwargs.pop('port', 1981)
        self.start = kwargs.pop('start','html')
        
        try:
            os.chdir(
                os.path.join('.', self.start)
            )
        except:
            pass
    
    def run(self):
        print 'Started HTTP server on ' +  self.interface + ':' + str(self.port)

        server = ThreadingSimpleServer((self.interface, self.port), SimpleHTTPRequestHandler)
        try:
            while 1:
                sys.stdout.flush()
                server.handle_request()
        except KeyboardInterrupt:
            print 'Finished.'

h = PrivateHTTPserver().run()
