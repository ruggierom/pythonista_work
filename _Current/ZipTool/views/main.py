import ui
from tabview import TabView



v = ui.load_view()
v.background_color = '#333'

def quit(sender=None):
    sender.superview.close()


v.present('fullscreen', title_bar_color='black', hide_title_bar=True)
